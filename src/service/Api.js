import AsyncStorage from '@react-native-community/async-storage';
import {request, requestMultipart} from './ApiSauce';
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoder';
import {Dimensions} from 'react-native';
import {GOOGLE_MAPS_APIKEY, latitudeDelta} from '../service/Config';
// const request = (path, json) => {
//   return new Promise((resolve, reject) => {
//     ApiSauce.post(path, json).then((response) => {
//       if (response.ok) {
//         resolve(response.data);
//       } else {
//         console.log(response.err);
//         reject(response.err);
//       }
//     });
//   });
// };

export const SignInApi = json => request('signin', json);
export const SignUpApi = json => request('signup', json);
export const SignUpOtpApi = json => request('checkandotp', json);
export const ForgotOtpApi = json => request('forgot_otp', json);
export const ResetPasswordApi = json => request('change_password_forget', json);
export const PopularPackageListApi = json => request('popular_pakcage', json);
export const PackageListApi = json => request('new_package_list_sd', json);
export const PackageDetailApi = json => request('package_details_sd', json);
export const PackageBookingApi = json =>
  request('package_booking_add_sd', json);
export const CabBookingApi = json => request('paytm_payment_gateway_url', json);
export const GetProfileApi = json => request('getprofile', json);
export const GetFlightFareApi = json =>
  request('get_fare_quote_resultindex', json);

export const GetFareCalenderApi = json => request('get_calendar_fare', json);
export const SearchFlightApi = json => request('search_flights', json);
export const GetSsrDetailApi = json => request('get_ssr_details', json);
export const GetAddressApi = json => request('lat_long_address', json);
export const GetAirportsApi = () => request('fetch_airports', {});
export const GetAirportListApi = () => request('airport_list', {});
export const GetCabApi = json => request('airport_prices', json);
export const GetHomePageApi = json => request('gethomepage', json);
export const CouponListApi = json => request('coupan_list', json);
export const CouponApi = json => request('coupan_verify', json);
export const GetMemberApi = json => request('get_members', json);
export const AddMemberApi = json => request('add_member', json);
export const EditMemberApi = json => request('edit_member', json);
export const ToggleMemberApi = json => request('toggle_select_member', json);
export const DeleteMemberApi = json => request('delete_member', json);
export const HistoryApi = json => request('booking_history_sd', json);
export const CancelBookingApi = json => request('cancel_booking_sd', json);
export const UpdateProfileApi = form => requestMultipart('edit_profile', form);

export const AsyncStorageSetUserId = id => AsyncStorage.setItem('user_id', id);
export const AsyncStorageGetUserId = () => AsyncStorage.getItem('user_id');

export const AsyncStorageClear = () => AsyncStorage.clear();

export const AspectRatio = () =>
  Dimensions.get('window').width / Dimensions.get('window').height;
export const Height = Dimensions.get('window').height;
export const Width = Dimensions.get('window').width;
export const LongitudeDelta = () =>
  (latitudeDelta * Dimensions.get('window').width) /
  Dimensions.get('window').height;
export const LatitudeDelta = latitudeDelta;

export const formatAmount = amount =>
  `\u20B9 ${parseInt(amount)
    .toFixed(0)
    .replace(/(\d)(?=(\d\d)+\d$)/g, '$1,')}`;

export const formatNumber = str => str.replace(/,/g, '').replace('\u20B9 ', '');

export const textInPrice = price => `\u20B9 ${price}`;

export const timeFormate_mmss = time => {
  let mm = Math.floor(time / 60);
  let ss = time % 60;
  mm = mm < 10 ? `0${mm}` : mm;
  ss = ss < 10 ? `0${ss}` : ss;
  return `${mm}:${ss}`;
};

export const GeolocationInfo = () => {
  return new Promise((resolve, reject) => {
    Geolocation.getCurrentPosition(info => {
      // console.log('info');
      // console.log(info);
      resolve(info);
    });
  });
};
export const GeocoderLocation = ({latitude, longitude}) => {
  return new Promise((resovle, reject) => {
    Geocoder.geocodePosition({lat: latitude, lng: longitude}).then(res =>
      resovle(res),
    );
  });
};
export const GoogleDirectionApi = (origin, destination) => {
  // console.log(origin);
  // console.log(destination);
  return new Promise((resolve, reject) => {
    const url = `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&mode=driving&units=metric&key=${GOOGLE_MAPS_APIKEY}`;
    fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then(response => {
        const {status, routes = []} = response;
        if (status === 'OK' && routes.length) {
          const {legs = []} = routes[0];
          resolve({
            status: true,
            code: 200,
            data: {
              distance: legs[0].distance,
              duration: legs[0].duration,
            },
          });
        } else {
          resolve({status: false, code: 204, data: response});
        }
      })
      .catch(error => {
        console.log('googledirectionapierror', error);
        resolve({status: false, code: 500, data: error});
      });
  });
};

export const getCouponModule = (condition = '', condition_outstation = '') => {
  // console.log(condition, condition_outstation);
  switch (condition) {
    case 'from_airport':
      return 3;
    case 'to_airport':
      return 4;
    case 'local':
      return 5;
    case 'outstation':
      return 6;
    default:
      return 9;
  }
};

export const modeOfArray = myArray =>
  myArray.reduce(
    (a, b, i, arr) =>
      arr.filter(v => v === a).length >= arr.filter(v => v === b).length
        ? a
        : b,
    null,
  );

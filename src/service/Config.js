const webClientId =
  '207538176069-cgih256ia35b2jlbcm1ta837l79ka22s.apps.googleusercontent.com';

// const BASE_URL = 'http://139.59.76.223/cab/webservices/';
const BASE_URL = 'http://139.59.76.223/cab_and_taxi_final/webservices/';
export const XApiKey =
  '$2y$12$MOOt6dmiClUmITafZDyR2edjeJzx.UiXzG/ArWY8fl.zhNSi6FUfy';
export const GOOGLE_MAPS_APIKEY = 'AIzaSyBWX-QNm_gVzt6U2K6xeU4cmF5dkX8XUQ0';

export const GoogleSigninJson = {
  webClientId: webClientId,
  offlineAccess: true,
  hostedDomain: '',
  forceConsentPrompt: true,
};
export const ApiSauceJson = {
  baseURL: BASE_URL,
  headers: {
    'X-API-KEY': XApiKey,
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
};
export const ApiSauceJsonMulitpart = {
  baseURL: BASE_URL,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'multipart/form-data',
  },
};
export const latitudeDelta = 0.0922;

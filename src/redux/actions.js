export const Login = user => ({type: 'login', payload: user ? user : {}});
export const Logout = () => ({type: 'logout'});
export const SetCoords = coords => ({type: 'setCoords', payload: coords});
export const SearchPackage = (
  popularPackage,
  domesticCity,
  internationalCity,
) => ({
  type: 'searchPackage',
  payload: {
    popularPackage,
    domesticCity,
    internationalCity,
  },
});
export const SetAirportCode = list => ({
  type: 'setAirportCode',
  payload: list,
});
export const SetAirportCab = list => ({
  type: 'setAirportCab',
  payload: list,
});
export const SetCouponList = list => ({
  type: 'setCouponList',
  payload: list,
});

export const SetStates = states => ({type: 'setStates', payload: states});
export const SetCities = cities => ({
  type: 'setCities',
  payload: cities,
});
export const SetDeviceInfo = deviceInfo => ({
  type: 'setDeviceInfo',
  payload: deviceInfo,
});

export const SetNetInfo = netInfo => ({type: 'setNetInfo', payload: netInfo});
export const SkillList = list => ({type: 'skillList', payload: list});
export const SetSettings = object => ({type: 'setSettings', payload: object});

import {AsyncStorageClear} from '../service/Api';
const data = {
  isLogin: false,
  user: {},
  coords: {},
  approved: false,
  app_status: false,
  netInfo: {
    details: {},
    isConnected: false,
    isInternetReachable: false,
    isWifiEnabled: false,
    type: '',
  },
  deviceInfo: {
    id: '',
    token: '',
    model: '',
    os: '',
  },
  skillList: [],
  states: [],
  cities: [],
  airportCodes: [],
  airportCab: [],
  couponList: [],
  settings: {
    booking_hour: '',
    cancelation_hour: '',
    website_link: '',
    contact: '',
    helpline_number: '',
    night_charge_start: '',
    night_charge_end: '',
  },
  popularPackage: [],
  domesticCity: [],
  internationalCity: [],
};
const reducer = (state = data, action) => {
  console.log('actionType', action.type);
  switch (action.type) {
    case 'login':
      return {
        ...state,
        user: action.payload,
        isLogin: true,
      };
    case 'logout':
      AsyncStorageClear();
      return {
        ...state,
        user: {},
        isLogin: false,
      };
    case 'setStates':
      return {
        ...state,
        states: action.payload,
      };
    case 'setCities':
      return {
        ...state,
        cities: action.payload,
      };
    case 'setNetInfo':
      return {
        ...state,
        netInfo: action.payload,
      };
    case 'setDeviceInfo':
      return {
        ...state,
        deviceInfo: action.payload,
      };
    case 'setCoords':
      return {
        ...state,
        coords: action.payload,
      };
    case 'setAirportCode':
      return {
        ...state,
        airportCodes: action.payload,
      };
    case 'setAirportCab':
      return {
        ...state,
        airportCab: action.payload,
      };
    case 'setCouponList':
      return {
        ...state,
        couponList: action.payload,
      };
    case 'setSettings':
      return {
        ...state,
        settings: action.payload,
      };
    case 'searchPackage':
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};

export default reducer;

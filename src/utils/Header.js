import React from 'react';
import {
  Image,
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';

export const SimpleHeader = (title, callback) => (
  <View style={headerStyle.container}>
    <TouchableOpacity style={headerStyle.touch} onPress={callback}>
      <Image
        source={require('../assets/backicon.png')}
        style={headerStyle.backImage}
      />
    </TouchableOpacity>
    <Text style={headerStyle.title}>{title}</Text>
  </View>
);

const headerStyle = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight + 15 : 10,
    paddingBottom: 15,
    backgroundColor: '#FBD303',
  },
  title: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 17,
    color: '#09304B',
    paddingHorizontal: 20,
  },
  backImage: {
    width: 12,
    height: 22,
    resizeMode: 'contain',
  },
  touch: {
    padding: 10,
    position: 'absolute',
    left: 10,
    bottom: 5,
  },
});

import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Modal,
  Image,
  TouchableOpacity,
} from 'react-native';
const getSource = type => {
  switch (type) {
    case 'success':
      return require('../assets/check.png');
    case 'fail':
      return require('../assets/cross.png');
    default:
      return require('../assets/check.png');
  }
};
export const CustomAlert = ({modalState, onCloseModel}) => {
  const {title, type = 'success', visible} = modalState;
  consolejson('modal');
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={visible}
      statusBarTranslucent={true}
      onRequestClose={() => {
        console.log('Modal has been closed.');
      }}>
      <TouchableOpacity style={modalStyle.container} onPress={onCloseModel}>
        <View style={modalStyle.modalView}>
          <Image source={getSource(type)} style={modalStyle.imageIcon} />
          <Text
            style={[
              modalStyle.textTitle,
              type === 'fail' && modalStyle.textTitleFail,
            ]}>
            {title}
          </Text>
        </View>
      </TouchableOpacity>
    </Modal>
  );
};

const modalStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#00000099',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    borderRadius: 25,
    backgroundColor: 'white',
    alignItems: 'center',
    padding: 30,
  },
  imageIcon: {
    width: 100,
    height: 100,
    resizeMode: 'contain',
    marginBottom: 20,
  },
  textTitle: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 20,
    color: '#242E42',
    textAlign: 'center',
    lineHeight: 25,
    width: 220,
  },
  textTitleFail: {
    color: '#F33E51',
  },
});

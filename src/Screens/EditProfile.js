import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
  Dimensions,
  ImageBackground,
  StatusBar,
  SafeAreaView,
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
const window = Dimensions.get('window');
const {width, height} = Dimensions.get('window');
const options = {
  title: 'Select Avatar',
  maxWidth: 500,
  maxHeight: 500,
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};
import {Colors} from 'react-native/Libraries/NewAppScreen';
const GLOBAL = require('./Global');
import {TextField} from 'react-native-material-textfield';
import {useStore} from 'react-redux';
import {UpdateProfileApi} from '../service/Api';
const EditProfile = ({navigation, route}) => {
  const {user} = useStore().getState();

  const [state, setState] = useState({
    user_id: user.id,
    name: user.name,
    mobile: user.mobile,
    email: user.email,
    address: user.address,
    city: user.city,
    state: user.state,
    avatarSource: '',
    image: user.profile_pic,
    profileImage: user.profile_pic,
  });
  const buttonClickListener = async () => {
    console.log('asdf');
    // const url = 'http://139.59.76.223/cab/webservices/edit_profile';
    const data = new FormData();
    data.append('user_id', GLOBAL.user_id);
    data.append('name', state.name);
    data.append('email', state.email);
    data.append('address', state.address);
    data.append('city', state.city);
    data.append('state', state.state);

    // you can append anyone.
    console.log(state.profileImage);
    data.append('profile_pic', {
      uri: state.profileImage,
      type: 'image/jpeg', // or photo.type
      name: 'image.png',
    });
    const {status = false} = await UpdateProfileApi(data);
    alert(status ? 'Profile Updated Successfully!' : 'Something went wrong');
  };

  const changeImage = () => {
    const options = {
      title: 'Select and Take Profile Picture',
      cameraType: 'front',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {uri: response.uri};

        // GLOBAL.profileImage = response.uri;
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        setState({...state, avatarSource: source, profileImage: response.uri});
      }
    });
  };
  useEffect(() => {
    // console.log(state);
  });
  return (
    <View style={{backgroundColor: 'white', flex: 1}}>
      <StatusBar
        barStyle="light-content"
        backgroundColor={'transparent'}
        translucent={true}
      />
      <SafeAreaView style={{flex: 0, backgroundColor: Colors.blue}} />

      <View
        style={{
          backgroundColor: '#09304B',
          padding: 20,
          paddingTop: 35,
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            paddingVertical: 5,
          }}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{
              position: 'absolute',
              left: 0,
              paddingVertical: 5,
            }}>
            <Image
              source={require('../assets/white-back.png')}
              style={{
                width: 20,
                height: 20,
                resizeMode: 'contain',
              }}
            />
          </TouchableOpacity>

          <Text
            style={{
              fontFamily: GLOBAL.heavy,
              fontSize: 20,
              fontWeight: '900',
              color: 'white',
            }}>
            Profile
          </Text>
          <TouchableOpacity
            onPress={() => buttonClickListener()}
            style={{
              position: 'absolute',
              right: 0,
              paddingVertical: 5,
            }}>
            <Text
              style={{
                fontFamily: GLOBAL.heavy,
                fontSize: 20,
                color: 'white',
                fontWeight: '900',
              }}>
              Save
            </Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            flexDirection: 'row',
            alignSelf: 'center',
            padding: 20,
          }}>
          <TouchableOpacity onPress={() => changeImage()}>
            {state.avatarSource != '' && (
              <Image
                style={{
                  width: 120,
                  height: 120,
                  borderRadius: 60,
                  alignSelf: 'center',
                }}
                source={state.avatarSource}
              />
            )}
            {state.avatarSource == '' && (
              <Image
                style={{
                  width: 120,
                  height: 120,
                  borderRadius: 60,
                  alignSelf: 'center',
                }}
                source={{uri: state.image}}
              />
            )}
            <Image
              source={require('../assets/camera.png')}
              style={{
                height: 35,
                width: 35,
                position: 'absolute',
                right: 0,
              }}
            />
          </TouchableOpacity>
        </View>
      </View>

      {/* <View style={{}}>
          <Text
            style={{
              fontFamily: GLOBAL.roman,
              fontSize: 12,
              color: '#8a8a8f',
              marginTop: 12,
              marginLeft: 16,
            }}>
            {GLOBAL.histoty.traval_date} , {GLOBAL.histoty.traval_time}
          </Text>
        </View> */}

      <View
        style={{
          margin: 20,
        }}>
        <TextField
          label={'Name'}
          defaultValue={state.name}
          tintColor="grey"
          onChangeText={(name) => setState({...state, name})}
          baseColor="black"
        />

        <TextField
          label={'Email'}
          defaultValue={state.email}
          tintColor="grey"
          // onChangeText={(email) => setState({...state, email})}
          baseColor="black"
          editable={false}
        />

        <TextField
          label={'Mobile'}
          defaultValue={state.mobile}
          tintColor="grey"
          editable={false}
          baseColor="black"
        />

        <TextField
          label={'Address'}
          defaultValue={state.address}
          tintColor="grey"
          onChangeText={(address) => setState({...state, address})}
          baseColor="black"
        />

        <View style={{flexDirection: 'row'}}>
          <View style={{width: width / 2 - 30}}>
            <TextField
              label={'City'}
              defaultValue={state.city}
              tintColor="grey"
              onChangeText={(city) => setState({...state, city})}
              baseColor="black"
            />
          </View>

          <View style={{width: width / 2 - 30, marginLeft: 20}}>
            <TextField
              label={'State'}
              defaultValue={state.state}
              tintColor="grey"
              onChangeText={(state) => setState({...state, state})}
              baseColor="black"
            />
          </View>
        </View>
      </View>
    </View>
  );
};
export default EditProfile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  button: {
    borderWidth: 1,
    borderColor: '#000000',
    margin: 5,
    padding: 5,
    width: '70%',
    backgroundColor: '#DDDDDD',
    borderRadius: 5,
  },
  textField: {
    borderWidth: 1,
    borderColor: '#AAAAAA',
    margin: 5,
    padding: 5,
    width: '70%',
  },
  spacer: {
    height: 10,
  },
  scene: {
    flex: 1,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
  },
});

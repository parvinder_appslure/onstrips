import React, {useState, useEffect, useRef} from 'react';
import {
  Image,
  View,
  SafeAreaView,
  Text,
  TouchableOpacity,
  StyleSheet,
  StatusBar,
} from 'react-native';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import moment from 'moment';
import RBSheet from 'react-native-raw-bottom-sheet';
import GLOBAL from './Global';
import CheckBox from '@react-native-community/checkbox';
import {FlatList} from 'react-native';
import airlineicon from './airlineicon';
import {ScrollView} from 'react-native';
const FlightDetails = ({navigation, route}) => {
  const [state, setState] = useState({
    flight_from: 'from',
    flight_to: 'to',
    chk_direct: [false, false],
    chk_lcc: [false, false],
    chk_gds: [false, false],
    timeSort: ['', ''],
    sort: [0, 0],
    airlineList: [[], []],
  });
  const [sort, setSort] = useState({go: 0, back: 0});
  const [filter, setFilter] = useState(false);
  const [tabState, setTabState] = useState({
    index: 0,
    routes: [
      {key: 'go', title: route.params.goTitle},
      {key: 'back', title: route.params.backTitle},
    ],
  });

  const filterRef = useRef();
  const [flightState, setFlightState] = useState({
    go: [],
    back: [],
    goFlightId: 'id_0',
    backFlightId: 'id_0',
    fare: 0,
    data: [],
  });
  const [onLoadState, setOnLoadState] = useState('search');
  const convertMinsToTime = (mins) => {
    let hours = Math.floor(mins / 60);
    let minutes = mins % 60;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    return `${hours}hrs:${minutes}mins`;
  };

  const flightTitle = (flight, fare, flightCode, flightNumber) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          paddingVertical: 10,
          paddingHorizontal: 5,
        }}>
        <Image
          source={airlineicon[flightCode]}
          style={{
            width: 30,
            height: 30,
            resizeMode: 'contain',
          }}
        />
        <View
          style={{
            paddingLeft: 10,
          }}>
          <Text
            style={{
              color: '#09304B',
              fontFamily: GLOBAL.roman,
              fontSize: 18,
              fontWeight: '900',
            }}>
            {flight}
          </Text>
          <Text
            style={{
              color: '#09304B80',
              fontFamily: GLOBAL.roman,
              fontSize: 12,
              fontWeight: '400',
            }}>
            {`${flightCode} - ${flightNumber}`}
          </Text>
        </View>
        <Text
          style={{
            color: '#09304B',
            fontFamily: GLOBAL.roman,
            fontSize: 18,
            fontWeight: '900',
            marginLeft: 'auto',
          }}>
          ₹{fare}
        </Text>
      </View>
    );
  };
  const renderText = (value) => (
    <Text
      style={{
        color: '#09304B80',
        fontFamily: GLOBAL.roman,
        fontSize: 14,
        textAlign: 'center',
      }}>
      {value}
    </Text>
  );
  const timeSortHandler = (value) => {
    let timeSort = state.timeSort;
    const {index} = tabState;
    timeSort[index] =
      timeSort[index] === '' || timeSort[index] !== value ? value : '';
    setState({...state, timeSort});
  };
  const timeSortTouch = (key, title) => (
    <TouchableOpacity
      onPress={() => timeSortHandler(key)}
      style={[
        styles.time_touch,
        state.timeSort[tabState.index] === key && styles.time_touch_active,
      ]}>
      <Text
        style={[
          styles.time_text,
          state.timeSort[tabState.index] === key && styles.time_text_active,
        ]}>
        {title}
      </Text>
    </TouchableOpacity>
  );
  const handleAirlineListCheck = (AirlineCode, value) => {
    let {airlineList} = state;
    const {index} = tabState;
    airlineList[index].find(
      (item) => item.AirlineCode === AirlineCode,
    ).check = value;
    setState({...state, airlineList});
  };
  const filterScreen = () => (
    <RBSheet
      ref={filterRef}
      height={650}
      openDuration={250}
      customStyles={{
        container: {
          borderTopRightRadius: 15,
          borderTopLeftRadius: 15,
          paddingTop: 15,
        },
      }}>
      <View>
        <View style={styles.chk_mainView}>
          {[
            {title: 'Non-Stop Flight', key: 'chk_direct'},
            {title: 'LCC', key: 'chk_lcc'},
            {title: 'GDS', key: 'chk_gds'},
          ].map(({title, key}) => (
            <>
              <View style={styles.chk_view}>
                <CheckBox
                  tintColors={{true: '#F8A301', false: '#242E42'}}
                  value={state[key][tabState.index]}
                  onValueChange={(newValue) => {
                    // this.setState({[key]: newValue});
                    let value = state[key];
                    value[tabState.index] = newValue;
                    console.log(key);
                    setState({...state, [key]: value});
                  }}
                />
              </View>
              <Text style={styles.chk_mainText}>{title}</Text>
            </>
          ))}
        </View>
        <Text style={styles.al_title}>Timing</Text>
        <View style={styles.time_container}>
          <View style={styles.time_view}>
            {timeSortTouch('1', 'Morning 6 AM-12')}
            {timeSortTouch('2', 'Afternoon 12-6 PM')}
          </View>
          <View style={styles.time_view}>
            {timeSortTouch('3', 'Evening 6 PM-12')}
            {timeSortTouch('4', 'Night 12-6 AM')}
          </View>

          <Text style={styles.al_title}>Sort By</Text>
          <View style={styles.sort_container}>
            {['Cheapest', 'Best', 'Earliest', 'Shortest'].map(
              (title, index) => (
                <TouchableOpacity
                  key={`sort_${title}`}
                  onPress={() => {
                    let sort = state.sort;
                    sort[tabState.index] = +index;
                    setState({...state, sort});
                  }}
                  style={[
                    {
                      backgroundColor:
                        state.sort[tabState.index] === index
                          ? '#09304B'
                          : '#FBD303',
                    },
                    styles.sort_touch,
                  ]}>
                  <Text
                    style={[
                      styles.textRoman,
                      state.sort[tabState.index] === index && {color: 'white'},
                    ]}>
                    {title}
                  </Text>
                </TouchableOpacity>
              ),
            )}
          </View>

          <Text style={styles.al_title}>Airlines</Text>
          <ScrollView>
            <View style={styles.al_mainView}>
              {state.airlineList[tabState.index].map(
                ({AirlineCode, AirlineName, check}) => (
                  <View style={styles.al_view}>
                    <CheckBox
                      tintColors={{true: '#F8A301', false: '#242E42'}}
                      value={check}
                      onValueChange={(value) =>
                        handleAirlineListCheck(AirlineCode, value)
                      }
                    />
                    <Text style={styles.al_text}>{AirlineName}</Text>
                  </View>
                ),
              )}
            </View>
          </ScrollView>
        </View>
      </View>
    </RBSheet>
  );

  const renderRowItem1 = (itemData, index) => {
    const segment = itemData.item.Segments[0];
    const segmentLength = segment.length;
    const timeFormat = (time) => moment(time).format('h:mm:a');
    const flightDetails = {
      originCode: segment[0].Origin.Airport.AirportCode,
      originTerminal: segment[0].Origin.Airport.Terminal,
      originTime: timeFormat(segment[0].Origin.DepTime),
      destinationCode:
        segment[segmentLength - 1].Destination.Airport.AirportCode,
      destinationTime: timeFormat(
        segment[segmentLength - 1].Destination.ArrTime,
      ),
      duration: convertMinsToTime(
        moment(segment[segmentLength - 1].Destination.ArrTime).diff(
          moment(segment[0].Origin.DepTime),
          'minute',
        ),
      ),
      destinationTerminal:
        segment[segmentLength - 1].Destination.Airport.Terminal,
      stop: 'Non-Stop',
    };
    if (segmentLength > 1) {
      flightDetails.stop = `${segmentLength - 1} Stop`;
      for (let i = 0; i < segmentLength - 1; i++) {
        flightDetails.stop = `${flightDetails.stop}${i === 0 ? '\n' : ','}${
          segment[i].Destination.Airport.AirportCode
        }`;
      }
    }
    let bgColorBol =
      itemData.item.flightId ===
      flightState[
        itemData.item.flightKey === 0 ? 'goFlightId' : 'backFlightId'
      ];
    return (
      <TouchableOpacity
        style={[
          {
            backgroundColor: bgColorBol ? '#FFF9C4' : 'white',
            margin: 15,
            borderRadius: 8,
          },
          !bgColorBol && {
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,

            elevation: 5,
          },
        ]}
        onPress={() => onSelectHandler(itemData.item.flightId)}>
        <View
          style={{
            padding: 10,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingHorizontal: 5,
            }}>
            <View
              style={{
                paddingTop: 10,
              }}>
              <Text
                style={{
                  color: '#09304B80',
                  fontFamily: GLOBAL.roman,
                  fontWeight: '500',
                  fontSize: 17,
                }}>
                {flightDetails.originCode}
              </Text>
              <Text
                style={{
                  color: '#F8A301',
                  fontFamily: GLOBAL.roman,
                  fontWeight: '500',
                  fontSize: 15,
                }}>
                {flightDetails.originTime}
              </Text>

              <Text
                style={{
                  color: '#09304B80',
                  fontFamily: GLOBAL.roman,
                  fontWeight: '500',
                  fontSize: 12,
                }}>
                {`Terminal ${flightDetails.originTerminal || '-'}`}
              </Text>
            </View>
            <View
              style={{
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  color: '#09304B80',
                  fontFamily: GLOBAL.roman,
                  fontSize: 14,
                  textAlign: 'center',
                }}>
                {flightDetails.duration}
              </Text>
              <Image
                source={require('../assets/cad.png')}
                style={{
                  width: 140,
                  height: 20,
                  marginVertical: 2,
                  resizeMode: 'contain',
                }}
              />
              <Text
                style={{
                  color: '#09304B80',
                  fontFamily: GLOBAL.roman,
                  fontSize: 14,
                  textAlign: 'center',
                }}>
                {flightDetails.stop}
              </Text>
            </View>
            <View
              style={{
                paddingTop: 10,
                alignItems: 'flex-end',
              }}>
              <Text
                style={{
                  color: '#09304B80',
                  fontFamily: GLOBAL.roman,
                  fontWeight: '500',
                  fontSize: 17,
                }}>
                {flightDetails.destinationCode}
              </Text>
              <Text
                style={{
                  color: '#F8A301',
                  fontFamily: GLOBAL.roman,
                  fontWeight: '500',
                  fontSize: 15,
                }}>
                {flightDetails.destinationTime}
              </Text>
              <Text
                style={{
                  color: '#09304B80',
                  fontFamily: GLOBAL.roman,
                  fontWeight: '500',
                  fontSize: 12,
                }}>
                {`Terminal ${flightDetails.destinationTerminal || '-'}`}
              </Text>
            </View>
          </View>

          <View
            style={{
              marginTop: 8,
              height: 1,
              backgroundColor: '#f1f1f1',
              width: '100%',
            }}></View>

          {/* <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingVertical: 10,
              alignItems: 'center',
            }}> */}
          {/* <View style={{flexDirection: 'row'}}> */}
          {flightTitle(
            segment[0].Airline.AirlineName,
            itemData.item.Fare.PublishedFare,
            segment[0].Airline.AirlineCode,
            segment[0].Airline.FlightNumber,
          )}
          {/* </View> */}
        </View>
      </TouchableOpacity>
    );
  };
  const renderScene = ({route}) => {
    return (
      <View>
        <FlatList
          contentContainerStyle={{
            paddingTop: 10,
            paddingBottom: 50,
          }}
          data={flightState[route.key]}
          horizontal={false}
          renderItem={renderRowItem1}
          numColumns={1}
          extraData={
            flightState[route.key === 0 ? 'goFlightId' : 'backFlightId']
          }
        />
      </View>
    );
  };
  const renderTabBar = (props) => {
    return (
      <TabBar
        style={{
          backgroundColor: '#FBD303',
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,
          elevation: 5,
        }}
        labelStyle={{
          fontSize: 14,
          fontFamily: GLOBAL.roman,
          fontWeight: '700',
        }}
        tabStyle={{}}
        // scrollEnabled={true}
        activeColor={'#09304B'}
        inactiveColor={'#4B4B4B99'}
        inactiveOpacity={0.5}
        {...props}
        indicatorStyle={{backgroundColor: '#09304B', height: 3}}
      />
    );
  };

  const travel =
    parseInt(GLOBAL.flide.adultcount) +
    parseInt(GLOBAL.flide.childcount) +
    parseInt(GLOBAL.flide.infantcount);

  const headerView = () => (
    <View style={styles.header}>
      <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={styles.headerback}>
        <Image
          source={require('../assets/back2.png')}
          style={styles.backImage}
        />
      </TouchableOpacity>
      <View style={styles.headerTitle}>
        <Text style={styles.flightDateText}>
          {`${moment(GLOBAL.flide.departure_date).format('DD-MMM')} ${
            GLOBAL.flide.totalCount
          } Travellers | ${GLOBAL.flide.classflight}`}
        </Text>
      </View>
      {onLoadState === 'ready' && (
        <TouchableOpacity
          style={styles.filter}
          // onPress={() => setFilter(!filter)}
          onPress={() => filterRef.current.open()}>
          <Image
            source={require('../assets/filter-filled-tool-symbol.png')}
            style={styles.filterImage}
          />
        </TouchableOpacity>
      )}
    </View>
  );
  const tabBarView = () => (
    <TabView
      navigationState={tabState}
      renderTabBar={renderTabBar}
      renderScene={renderScene}
      onIndexChange={(index) => setTabState({...tabState, index: index})}
      renderLabel={({route, focused, color}) => (
        <Text style={{color: 'yellow', margin: 8}}>{route.title + '55'}</Text>
      )}

      //   initialLayout={}
    />
  );

  const sortView = () => (
    <View style={styles.sortView}>
      {['Best', 'Cheapest', 'Earliest', 'Shortest'].map((title, index) => (
        <TouchableOpacity
          key={`sort_${title}`}
          onPress={() => {
            setFilter(false);
            setSort({...sort, [tabState.index ? 'back' : 'go']: index});
          }}
          style={[
            styles.sortTouch,
            sort[tabState.index ? 'back' : 'go'] === index &&
              styles.sortTouchSelect,
          ]}>
          <Text
            style={[
              styles.sortText,
              sort[tabState.index ? 'back' : 'go'] === index &&
                styles.sortTextSelect,
            ]}>
            {title}
          </Text>
        </TouchableOpacity>
      ))}
    </View>
  );
  const bookButtonView = () => (
    <View
      style={{
        width: '100%',
        flexDirection: 'row',
        position: 'absolute',
        bottom: 0,
        paddingHorizontal: 15,
        paddingVertical: 5,
        backgroundColor: 'white',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        alignItems: 'center',
      }}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          padding: 10,
        }}>
        <Text
          style={{
            color: '#09304B',
            fontFamily: GLOBAL.roman,
            fontSize: 16,
            fontWeight: '900',
          }}>
          {`\u20B9 ${parseInt(flightState.fare)
            .toFixed(0)
            .replace(/(\d)(?=(\d\d)+\d$)/g, '$1,')}`}
        </Text>
      </View>
      <TouchableOpacity
        // onPress={() => navigate('FlightBooking', this.state.flightSelect)}
        onPress={continueHandler}
        style={{
          backgroundColor: '#09304B',
          borderRadius: 8,
          marginLeft: 'auto',
        }}>
        <Text
          style={{
            color: 'white',
            padding: 10,
          }}>
          CONTINUE
        </Text>
      </TouchableOpacity>
    </View>
  );

  const onSelectHandler = (flightId) => {
    console.log('flightid', flightId);
    console.log('id', tabState.index);
    let key, fare;
    if (tabState.index === 0) {
      key = 'goFlightId';
      fare =
        flightState.go.find((item) => item.flightId === flightId).Fare
          .PublishedFare +
        flightState.back.find(
          (item) => item.flightId === flightState.backFlightId,
        ).Fare.PublishedFare;
    } else {
      key = 'backFlightId';
      fare =
        flightState.back.find((item) => item.flightId === flightId).Fare
          .PublishedFare +
        flightState.go.find((item) => item.flightId === flightState.goFlightId)
          .Fare.PublishedFare;
    }

    setFlightState({
      ...flightState,
      [key]: flightId,
      fare: fare,
    });
  };

  const continueHandler = () => {
    const goFlightId = flightState.goFlightId;
    const backFlightId = flightState.backFlightId;
    const flightDetail = {};
    flightDetail.goFlight = flightState.go.find(
      (item) => item.flightId === goFlightId,
    );
    flightDetail.backFlight = flightState.back.find(
      (item) => item.flightId === backFlightId,
    );
    flightDetail.fare = flightState.fare;
    const {
      adultcount,
      childcount,
      infantcount,
      TokenId,
      TraceId,
    } = route.params.data;
    const ResultIndex = [
      flightDetail.goFlight.ResultIndex,
      flightDetail.backFlight.ResultIndex,
    ];
    navigation.navigate('FlightBookings', {
      TokenId,
      ResultIndex,
      TraceId,
      adultcount,
      childcount,
      infantcount,
    });
  };
  useEffect(() => {
    fetch(GLOBAL.BASE_URL + 'search_flights', {
      method: 'POST',
      headers: {
        'x-api-key':
          '$2y$12$MOOt6dmiClUmITafZDyR2edjeJzx.UiXzG/ArWY8fl.zhNSi6FUfy',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(route.params.data),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('api');
        if (responseJson.status == true) {
          route.params.data.TraceId = responseJson.TraceId;
          let Results = responseJson.Results;
          let fare = 0;
          let airlineList = [[], []];
          Results.forEach((res, i) => {
            // Results[i] = Results[i].filter((item) => item.IsLCC); //islcc true

            Results[i].map((item, index) => {
              item['flightId'] = 'id_' + index;
              item['flightKey'] = i;
              if (index === 0) {
                fare = fare + item.Fare.PublishedFare;
              }
              item.Segments[0].forEach(({Airline}) => {
                const {AirlineCode, AirlineName} = Airline;
                if (
                  airlineList[i].find(
                    (obj) => obj.AirlineCode === AirlineCode,
                  ) === undefined
                ) {
                  airlineList[i].push({AirlineCode, AirlineName, check: false});
                }
              });
            });
          });
          if (Results[0].length === 0 || Results[1].length === 0) {
            setOnLoadState('error');
            return;
          }
          setFlightState({
            go: Results[0],
            back: Results[1],
            goFlightId: 'id_0',
            backFlightId: 'id_0',
            fare: fare,
            data: Results,
          });
          setState({...state, airlineList});
          setOnLoadState('ready');
        } else {
          console.log(responseJson);
          setOnLoadState('error');
        }
      })
      .catch((error) => {
        console.error(error);
        setOnLoadState('error');
      });
  }, []);
  // useEffect(() => {
  //   filterResults(sort.go, 0, 'go');
  // }, [sort.go]);

  // useEffect(() => {
  //   filterResults(sort.back, 1, 'back');
  // }, [sort.back]);
  // useEffect(() => {
  //   console.log(tabState.routes);
  // }, [tabState.routes]);
  const noFlightView = (source, h1, h2) => {
    return (
      <>
        <Image source={source} style={styles.noFlightImage} />
        <Text style={styles.error_text_1}>{h1}</Text>
        <Text style={styles.error_text_2}>{h2}</Text>
      </>
    );
  };
  useEffect(() => {
    // return;
    const {index} = tabState;
    const {sort, airlineList, chk_direct, chk_lcc, chk_gds, timeSort} = state;
    const {data} = flightState;

    const key = index === 0 ? 'go' : 'back';
    const keyId = index === 0 ? 'goFlightId' : 'backFlightId';
    let flight = data[index] || [];
    if (flight.length === 0) return;
    let arr = [];
    airlineList[index].map((item) => {
      if (item.check) arr.push(item.AirlineCode);
    });
    console.log(flight.length);
    // direct flight
    if (chk_direct[index])
      flight = flight.filter((item) => item.Segments[0].length === 1);
    // lcc and gds flight
    if (chk_lcc[index] ^ chk_gds[index])
      flight = flight.filter((item) => item.IsLCC === chk_lcc[index]);
    // specific airline

    console.log(flight.length);
    if (arr.length)
      flight = flight.filter((item) => arr.includes(item.AirlineCode));
    // time category sort
    if (timeSort[index]) {
      flight = flight.filter(
        (item) =>
          +timeSort[index] % 4 ===
          Math.floor(
            +moment(item.Segments[0][0].Origin.DepTime).format('HH') / 6,
          ),
      );
    }

    switch (sort[index]) {
      case 0:
        flight = flight.sort((X, Y) =>
          X.Fare.PublishedFare > Y.Fare.PublishedFare ? 1 : -1,
        );
        break;
      case 1:
        break;
      case 2:
        flight = flight.sort((X, Y) =>
          X.Segments[0][0].Origin.DepTime > Y.Segments[0][0].Origin.DepTime
            ? 1
            : -1,
        );
        break;
      case 3:
        flight = flight.sort((X, Y) =>
          X.Segments[0][0].Duration > Y.Segments[0][0].Duration ? 1 : -1,
        );
        break;
    }

    if (flight.length) {
      setFlightState({
        ...flightState,
        [key]: flight,
        [keyId]: flight[0].flightId,
        // ResultIndex
      });
    } else {
      setFlightState({
        ...flightState,
        [key]: [],
        [keyId]: '',
        // ResultIndex
      });
    }
  }, [state]);

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        barStyle="dark-content"
        translucent={true}
        backgroundColor={'transparent'}
      />
      {headerView()}
      {onLoadState === 'ready' && (
        <>
          {tabBarView()}
          {filter && sortView()}
          {!filter && bookButtonView()}
          {/* {!this.state.filter &&
              this.state.flightId &&
              this.bookButtonView(navigate)}
          </> */}
        </>
      )}
      {onLoadState === 'search' &&
        noFlightView(
          require('../assets/flightanimation.gif'),
          `Hold Up Tight!`,
          `Fetching best Fare for You`,
        )}
      {onLoadState === 'error' &&
        noFlightView(
          require('../assets/no-travelling.png'),
          `No flights found`,
          `No flights found on this route for the requested date.`,
        )}
      {filterScreen()}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    flexDirection: 'row',
    backgroundColor: '#FBD303',
    paddingTop: 25,
    paddingBottom: 10,
    paddingHorizontal: 10,
    alignItems: 'center',
  },
  headerback: {
    padding: 5,
    paddingRight: 10,
  },
  backImage: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
  headerTitle: {
    paddingHorizontal: 10,
  },
  flightText: {
    fontFamily: GLOBAL.heavy,
    fontSize: 18,
    color: '#09304B',
  },
  flightDateText: {
    fontFamily: GLOBAL.roman,
    fontSize: 12,
    color: '#09304B',
  },
  filter: {
    marginLeft: 'auto',
    padding: 10,
  },
  filterImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  sortView: {
    width: '100%',
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    padding: 10,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  sortTouch: {
    backgroundColor: '#FBD303',
    padding: 5,
    width: 70,
    alignItems: 'center',
    borderRadius: 8,
  },
  sortTouchSelect: {
    backgroundColor: '#09304B',
  },
  sortText: {
    color: '#09304B',
    fontFamily: GLOBAL.roman,
    fontSize: 14,
  },
  sortTextSelect: {
    color: 'white',
  },
  error_text_1: {
    color: 'black',
    fontWeight: '700',
    fontSize: 18,
    alignSelf: 'center',
    marginTop: 20,
  },
  error_text_2: {
    color: '#4a4a4a',
    fontWeight: '500',
    fontSize: 14,
    alignSelf: 'center',
    marginTop: 5,
  },
  noFlightImage: {
    alignSelf: 'center',
    width: 150,
    height: 150,
    resizeMode: 'contain',
    marginTop: '30%',
  },

  fc_view: {
    flexDirection: 'row',
    borderBottomColor: '#242E42',
    borderBottomWidth: 0.5,
  },
  fc_touch: {
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  fc_touch_active: {
    padding: 10,
    alignItems: 'center',
    backgroundColor: '#242E42',
  },
  fc_text: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 12,
    color: '#242E42',
  },
  fc_text_active: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 14,
    color: 'white',
  },
  chk_mainView: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 10,
  },
  chk_view: {
    paddingHorizontal: 10,
  },
  chk_mainText: {
    fontFamily: GLOBAL.medium,
    fontWeight: '500',
    fontSize: 15,
    color: 'grey',
  },

  al_mainView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: 20,
  },
  al_view: {
    flexDirection: 'row',
    margin: 5,
    alignItems: 'center',
  },
  al_title: {
    color: '#09304B',
    fontFamily: GLOBAL.roman,
    fontSize: 18,
    fontWeight: '900',
    marginHorizontal: 15,
  },
  al_text: {
    fontFamily: GLOBAL.medium,
    fontWeight: '500',
    fontSize: 15,
    color: 'grey',
  },

  time_container: {
    marginTop: 10,
  },
  time_view: {
    flexDirection: 'row',
    marginBottom: 10,
    justifyContent: 'space-evenly',
  },
  time_touch: {
    flex: 0.4,
    paddingVertical: 10,
    borderRadius: 15,
    borderColor: '#7d787880',
    borderWidth: 1,
  },
  time_touch_active: {
    backgroundColor: '#242E42',
    borderWidth: 0,
  },
  time_text: {
    color: '#09304B',
    fontFamily: GLOBAL.medium,
    fontSize: 14,
    alignSelf: 'center',
  },
  time_text_active: {
    color: 'white',
  },
  textRoman: {
    color: '#09304B',
    fontFamily: GLOBAL.roman,
    fontSize: 14,
  },
  sort_container: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    marginVertical: 10,
  },
  sort_touch: {
    padding: 5,
    width: 70,
    alignItems: 'center',
    borderRadius: 8,
  },
  bbv_container: {
    width: '100%',
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    paddingHorizontal: 15,
    paddingVertical: 5,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    alignItems: 'center',
  },
  bbv_view: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
  },
  bbv_fareText: {
    color: '#09304B',
    fontFamily: GLOBAL.roman,
    fontSize: 16,
    fontWeight: '900',
  },
  bbv_touch: {
    backgroundColor: '#09304B',
    borderRadius: 8,
    marginLeft: 'auto',
  },
  bbv_continueText: {
    color: 'white',
    padding: 10,
  },
  ft_container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 5,
  },
  ft_image: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
  ft_view: {
    paddingLeft: 10,
  },
  ft_flightText: {
    color: '#09304B',
    fontFamily: GLOBAL.roman,
    fontSize: 18,
    fontWeight: '900',
  },
  ft_codeText: {
    color: '#09304B80',
    fontFamily: GLOBAL.roman,
    fontSize: 12,
    fontWeight: '400',
  },
  ft_fareText: {
    color: '#09304B',
    fontFamily: GLOBAL.roman,
    fontSize: 18,
    fontWeight: '900',
    marginLeft: 'auto',
  },
});

// FlightDetails['navigationOptions'] = (screenProps) => ({
//   header: false,
// });
export default FlightDetails;

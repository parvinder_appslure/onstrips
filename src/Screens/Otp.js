import React, {Component, useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
  Dimensions,
  StatusBar,
  SafeAreaView,
} from 'react-native';
import OTPInputView from '@twotalltotems/react-native-otp-input';

const {width, height} = Dimensions.get('window');
import Loader from './Loader';
import {CustomAlert} from '../utils/Modal';
import {StatusBarDark} from '../utils/CustomStatusBar';
import {
  AsyncStorageSetUserId,
  ForgotOtpApi,
  GetProfileApi,
  SignUpApi,
  SignUpOtpApi,
} from '../service/Api';
import {useDispatch, useSelector} from 'react-redux';
import * as actions from '../redux/actions';
const Otp = ({navigation, route}) => {
  const {deviceInfo} = useSelector(state => state);
  const dispatch = useDispatch();
  const [state, setState] = useState({
    code: '',
    loading: false,
  });
  const [modalState, setModalState] = useState({
    title: '',
    type: '',
    visible: false,
    callback: '',
  });
  const onCloseModel = () => {
    const {callback} = modalState;
    setModalState({
      ...modalState,
      title: '',
      type: '',
      visible: false,
      callback: '',
    });
    if (typeof callback === 'function') {
      callback();
    }
  };
  const toggleLoading = loading => setState({...state, loading});
  const onCodeChanged = code => setState({...state, code});
  const verifyHandler = async () => {
    const {code} = state;
    const {type, data} = route.params;
    if (+code === +data.otp) {
      if (type === 'forgotPassword') {
        navigation.navigate('ResetPassword', {mobile: data.mobile});
      }
      if (type === 'register') {
        const body = {
          name: data.name,
          email: data.email,
          mobile: data.mobile,
          address: data.address,
          password: data.password,
          city: data.city,
          state: data.states,
          device_id: deviceInfo.id,
          device_type: deviceInfo.os,
          device_token: deviceInfo.token,
          referral_user_id: '',
        };
        toggleLoading(true);
        const response = await SignUpApi(body);

        const {user_id = '', message = 'Something went wrong'} = response;
        if (user_id !== '') {
          AsyncStorageSetUserId(user_id.toString());
          const {status = false, data = {}} = await GetProfileApi({user_id});
          toggleLoading(false);
          if (status) {
            dispatch(actions.Login(data));
            setModalState({
              ...modalState,
              title: 'Thank you of enrolling with us',
              visible: true,
              type: 'success',
              callback: () => navigation.pop(3),
            });
          }
        } else {
          toggleLoading(false);
          setModalState({
            ...modalState,
            title: message,
            visible: true,
            type: 'fail',
            callback: '',
          });
        }
      }
    } else {
      alert('Enter Valid Otp');
    }
  };

  const resendOtpHandler = async () => {
    const {type, data} = route.params;
    if (type === 'forgotPassword') {
      const {mobile} = data;
      toggleLoading(true);
      const {status = false, otp = ''} = await ForgotOtpApi({mobile});
      toggleLoading(false);
      if (status) {
        data.otp = otp;
        setState({...state, code: ''});
        alert('New Otp Send Successfully');
      } else {
        alert('Something went wrong please try again');
      }
    }
    if (type === 'register') {
      const {email, mobile} = data;
      const otp = Math.floor(1000 + Math.random() * 9000);
      toggleLoading(true);
      const {status = false, msg = 'Something went wrong'} = await SignUpOtpApi(
        {email, mobile, otp},
      );
      toggleLoading(false);
      if (status) {
        data.otp = otp;
        setState({...state, code: ''});
        alert('New Otp Send Successfully');
      } else {
        alert(msg);
      }
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBarDark />
      {state.loading && <Loader />}
      <CustomAlert modalState={modalState} onCloseModel={onCloseModel} />
      <TouchableOpacity
        style={styles.touchBack}
        onPress={() => navigation.goBack()}>
        <Image
          source={require('../assets/backicon.png')}
          style={styles.imageBack}
        />
      </TouchableOpacity>

      <Text style={styles.textTitle_1}>Phone Verifications</Text>
      <Text style={styles.textTitle_2}>Enter your OTP code here</Text>

      <View style={{}}>
        <OTPInputView
          style={styles.OTPInputView}
          pinCount={4}
          code={state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
          onCodeChanged={onCodeChanged}
          autoFocusOnLoad
          codeInputFieldStyle={styles.underlineStyleBase}
          codeInputHighlightStyle={styles.underlineStyleHighLighted}
          onCodeFilled={code => {
            console.log(`Code is ${code}, you are good to go!`);
          }}
        />

        <TouchableOpacity onPress={verifyHandler} style={styles.touchNext}>
          <Text style={styles.touchText}>Verify Now</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={resendOtpHandler}
          style={styles.touchRegister}>
          <Text style={styles.textReg_1}>Didn't you received any code ?</Text>
          <Text style={styles.textReg_2}>Resend Code</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FBD303',
  },
  touchBack: {
    marginVertical: 40,
    marginLeft: 20,
    padding: 10,
    alignSelf: 'flex-start',
  },
  imageBack: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
  textTitle_1: {
    color: '#09304B',
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 34,
    marginHorizontal: 34,
  },
  textTitle_2: {
    color: '#242E42',
    fontFamily: 'Avenir-Roman',
    fontWeight: '400',
    fontSize: 14,
    marginVertical: 20,
    marginHorizontal: 34,
  },
  textInput: {
    paddingHorizontal: 20,
    marginHorizontal: 34,
    marginBottom: 30,
    marginTop: '20%',
    color: '#242E42',
    fontSize: 17,
    fontFamily: 'Aviner-Heavy',
    fontWeight: '900',
    backgroundColor: '#F1F2F6',
    borderRadius: 8,
    borderColor: '#EAECEF',
    borderWidth: 1,
  },
  touchNext: {
    backgroundColor: '#09304B',
    width: '60%',
    alignSelf: 'center',
    borderRadius: 6,
    padding: 8,
    marginTop: 20,
  },
  touchText: {
    fontFamily: 'Aviner-Heavy',
    fontSize: 17,
    fontWeight: '900',
    color: '#fff',
    textAlign: 'center',
  },
  touchRegister: {
    marginTop: 15,
    alignSelf: 'center',
    padding: 5,
  },
  textReg_1: {
    alignSelf: 'center',
    color: '#767676',
    fontFamily: 'Aviner-Roman',
    fontSize: 14,
  },
  textReg_2: {
    alignSelf: 'center',
    color: '#09304B',
    fontFamily: 'Aviner-Medium',
    fontSize: 15,
  },

  underlineStyleBase: {
    color: '#242E42',
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 30,
    width: 60,
    height: 60,
    borderWidth: 1,
    borderRadius: 2.5,
    backgroundColor: '#F1F2F6',
    borderColor: '#EAECEF',
  },

  underlineStyleHighLighted: {
    borderColor: '#EAECEF',
  },
  OTPInputView: {
    height: 150,
    width: '80%',
    alignSelf: 'center',
  },
});
export default Otp;

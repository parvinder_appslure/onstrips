import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  ImageBackground,
  StatusBar,
  TouchableOpacity,
  Text,
  ScrollView,
  Dimensions,
} from 'react-native';
import Loader from './Loader';
import {SimpleHeader} from '../utils/Header';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import {Dropdown} from 'react-native-material-dropdown';
import {TextField} from 'react-native-material-textfield';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import * as actions from '../redux/actions';
import {PopularPackageListApi, PackageListApi} from '../service/Api';
import {useDispatch, useStore} from 'react-redux';
const {width, height} = Dimensions.get('window');
const locationProps = [
  {label: 'Domestic', value: 'Domestic'},
  {label: 'International', value: 'International'},
];

const packageTypeProps = [
  {label: 'Family Holidays', value: 'Family Holiday'},
  {label: 'Friends', value: 'Friends'},
  {label: 'Couples', value: 'Couples'},
  {label: 'Honeymoon', value: 'Honeymoon'},
];
const PackageSearch = ({navigation, route}) => {
  const dispatch = useDispatch();
  const {
    popularPackage = [],
    domesticCity = [],
    internationalCity = [],
  } = useStore().getState();
  const [state, setState] = useState({
    destinationType: 'Domestic',
    destination: '',
    travellingIn: '',
    travellingOn: '',
    packageType: '',
    date: '',
    showPicker: false,
    destinationProps: [],
    popularPackage,
    domesticCity,
    internationalCity,
    loading: false,
  });

  const toggleLoading = loading => setState({...state, loading});
  const CustomRadioForm = (
    props,
    type,
    radioFormStyle = {},
    radioButtonStyle = {},
  ) => (
    <RadioForm formHorizontal={true} animation={true} style={radioFormStyle}>
      {props.map((obj, i) => (
        <RadioButton labelHorizontal={true} key={i} style={radioButtonStyle}>
          <RadioButtonInput
            obj={obj}
            index={i}
            isSelected={state[type] === obj.value}
            onPress={() => setState({...state, [type]: obj.value})}
            borderWidth={1}
            buttonInnerColor={'#F8A301'}
            buttonOuterColor={state[type] === obj.value ? '#F8A301' : '#242E42'}
            buttonSize={10}
            buttonOuterSize={20}
            buttonStyle={{}}
            buttonWrapStyle={{}}
          />
          <RadioButtonLabel
            obj={obj}
            index={i}
            labelHorizontal={true}
            onPress={() => setState({...state, [type]: obj.value})}
            labelStyle={{
              fontSize: 18,
              color: '#242E42',
              fontFamily: 'Avenir-Medium',
              fontWeight: '500',
            }}
          />
        </RadioButton>
      ))}
    </RadioForm>
  );

  const showDateTimePicker = () => (
    <DateTimePicker
      value={state.date || new Date()}
      minimumDate={new Date()}
      mode={'date'}
      display="spinner"
      is24Hour={false}
      onChange={({type, nativeEvent}) => {
        if (type === 'set') {
          setState({
            ...state,
            showPicker: false,
            date: nativeEvent.timestamp,
            travellingIn: +moment(nativeEvent.timestamp).format('MM'),
            travellingOn: moment(nativeEvent.timestamp).format('YYYY-MM-DD'),
          });
        } else {
          setState({...state, showPicker: false});
        }
      }}
    />
  );

  useEffect(() => {
    (async () => {
      const {
        popularPackage = [],
        domesticCity = [],
        internationalCity = [],
      } = state;
      if (
        [
          popularPackage.length,
          domesticCity.length,
          internationalCity.length,
        ].includes(0)
      ) {
        const response = await PopularPackageListApi({user_id: ''});
        const {
          status = false,
          packages_details = [],
          citys = [],
          internationalcitys = [],
        } = response;
        if (status) {
          const domesticCity = [];
          const internationalCity = [];
          citys.forEach(item => domesticCity.push({label: item, value: item}));
          internationalcitys.forEach(item =>
            internationalCity.push({label: item, value: item}),
          );
          setState({
            ...state,
            popularPackage: packages_details,
            domesticCity,
            internationalCity,
            destinationProps: domesticCity,
          });
          dispatch(
            actions.SearchPackage(
              packages_details,
              domesticCity,
              internationalCity,
            ),
          );
        } else {
          consolejson(response);
        }
      }
    })();
  }, []);

  useEffect(() => {
    const {
      destinationType = '',
      domesticCity = [],
      internationalCity = [],
    } = state;
    let destinationProps = [];
    if (destinationType === 'Domestic') {
      destinationProps = domesticCity;
    }
    if (destinationType === 'International') {
      destinationProps = internationalCity;
    }
    setState({...state, destinationProps, destination: ''});
  }, [state.destinationType]);

  const onPopularPackageHandler = item =>
    navigation.navigate('PackageDetail', {
      item,
      traval_date: state.travellingOn,
    });

  const searchHandler = async () => {
    const {
      destinationType,
      destination,
      travellingOn,
      travellingIn,
      packageType,
    } = state;
    // navigation.navigate('Package');
    // if (destinationType === '') {
    //   alert('Please select package location');
    //   return;
    // }
    // if (destination === '') {
    //   alert('Please select Destination/Cites');
    //   return;
    // }
    // if (travellingOn === '') {
    //   alert('Please select travelling date');
    //   return;
    // }
    // if (packageType === '') {
    //   alert('Please select package type');
    //   return;
    // }
    const body = {
      user_id: '',
      type: destinationType,
      city: destination,
      month: travellingIn,
      date: travellingOn,
      package_type: packageType,
    };
    toggleLoading(true);
    const response = await PackageListApi(body);
    toggleLoading(false);
    const {
      status = false,
      msg = 'Something went wrong',
      packages_details = [],
    } = response;
    if (status) {
      navigation.navigate('Package', {
        packages_details,
        traval_date: travellingOn,
      });
    } else {
      alert(msg);
    }
  };
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      {state.loading && <Loader />}
      <ScrollView>
        {SimpleHeader('Search Packages', navigation.goBack)}
        {CustomRadioForm(locationProps, 'destinationType', styles.radioForm)}
        <Dropdown
          disabled={state.destinationType === ''}
          rippleOpacity={0.54}
          dropdownPosition={-4}
          baseColor={state.destination ? '#242E42' : '#8F8F8F'}
          itemTextStyle={styles.dropDownTextStyle}
          label="Destination/Cities"
          containerStyle={{marginHorizontal: 20}}
          inputContainerStyle={{paddingHorizontal: 5}}
          labelFontSize={14}
          pickerStyle={{width: '90%', left: '5%'}}
          data={state.destinationProps}
          value={state.destination}
          onChangeText={destination => setState({...state, destination})}
        />
        <TouchableOpacity
          style={{marginHorizontal: 20}}
          onPress={() => setState({...state, showPicker: true})}>
          <TextField
            label={'Travelling On'}
            defaultValue={state.travellingOn}
            labelFontSize={14}
            inputContainerStyle={{paddingHorizontal: 5}}
            editable={false}
            baseColor={state.travellingOn === '' ? '#8F8F8F' : '#242E42'}
          />
        </TouchableOpacity>

        <TitleHeading title={'Package Type'} />
        {CustomRadioForm(
          packageTypeProps,
          'packageType',
          styles.radioFormPackage,
          styles.radioButtonStyle,
        )}
        <TouchableOpacity onPress={searchHandler} style={styles.searchTouch}>
          <Text style={styles.searchText}>Search</Text>
        </TouchableOpacity>
        {state.popularPackage.length !== 0 && (
          <>
            <TitleHeading title={'Our Popular Package'} />
            <View style={styles.popContainer}>
              {state.popularPackage.map(item => {
                const {package_name, image} = item;
                return (
                  <TouchableOpacity
                    onPress={() => onPopularPackageHandler(item)}>
                    <ImageBackground
                      source={{uri: image}}
                      style={styles.popImageBack}>
                      <View style={styles.popView}>
                        <Text style={styles.popTitle}>{package_name}</Text>
                      </View>
                    </ImageBackground>
                  </TouchableOpacity>
                );
              })}
            </View>
          </>
        )}
        {state.showPicker && showDateTimePicker()}
      </ScrollView>
    </SafeAreaView>
  );
};
export default PackageSearch;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  searchTouch: {
    backgroundColor: '#09304B',
    borderRadius: 20,
    alignSelf: 'center',
    width: '60%',
    paddingVertical: 10,
    marginTop: 20,
  },
  searchText: {
    textAlign: 'center',
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 18,
    color: '#FFFFFF',
  },
  textTitleHeading: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 18,
    color: '#242E42',
    margin: 20,
    marginBottom: 5,
  },
  radioForm: {
    marginTop: 10,
    padding: 20,
    paddingBottom: 0,
    justifyContent: 'space-around',
  },
  radioFormPackage: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: 10,
    marginTop: 5,
  },
  radioButtonStyle: {
    width: width / 2 - 30,
    margin: 10,
  },
  dropDownTextStyle: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    color: '#242E42',
  },
  popContainer: {
    margin: 15,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  popImageBack: {
    width: width / 3 - 20,
    height: 70,
    margin: 5,
    borderRadius: 10,
    overflow: 'hidden',
  },
  popView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00000066',
  },
  popTitle: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    color: '#fff',
  },
});

const TitleHeading = ({title}) => (
  <Text style={styles.textTitleHeading}>{title}</Text>
);

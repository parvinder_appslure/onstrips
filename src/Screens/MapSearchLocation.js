import React, {useEffect} from 'react';
import {
  StyleSheet,
  Platform,
  StatusBar,
  SafeAreaView,
  View,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {GOOGLE_MAPS_APIKEY} from '../service/Config';
import {StatusBarDark} from '../utils/CustomStatusBar';
import {SimpleHeader} from '../utils/Header';

const MapSearchLocation = ({navigation, route}) => {
  const onPressHandler = (data, details = null) => {
    navigation.navigate('MapLocation', {
      location: details.geometry.location,
      description: data.description,
    });
  };
  return (
    <SafeAreaView style={styles.container}>
      <StatusBarDark />
      {SimpleHeader('Search Address', navigation.goBack)}
      <GooglePlacesAutocomplete
        placeholder="Search"
        minLength={3}
        fetchDetails={true}
        onPress={onPressHandler}
        query={{
          key: GOOGLE_MAPS_APIKEY,
          language: 'en',
          // types: '(locality)',
        }}
        onFail={(error) => console.log(error)}
        enablePoweredByContainer={false}
        styles={{
          container: {
            marginHorizontal: 20,
            marginTop: 20,
          },
          textInputContainer: {
            overflow: 'hidden',
            paddingHorizontal: 10,
            borderRadius: 15,
            borderColor: '#242E4280',
            borderWidth: 1,
          },
          textInput: {
            color: '#5d5d5d',
            fontSize: 16,
          },
          predefinedPlacesDescription: {
            color: '#1faadb',
          },
        }}
      />
    </SafeAreaView>
  );
};

export default MapSearchLocation;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  header: {
    justifyContent: 'center',
    marginBottom: 30,
  },
  headerTouch: {
    padding: 8,
  },
  headerImage: {
    width: 18,
    height: 16,
    resizeMode: 'contain',
  },
  title: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '500',
    fontSize: 30,
    color: '#0A1F44',
    alignSelf: 'center',
    textTransform: 'capitalize',
  },
});

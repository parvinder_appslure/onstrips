import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import {TextField} from 'react-native-material-textfield';
import {SimpleHeader} from '../utils/Header';
import Loader from './Loader';

import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import {GetCabApi, GoogleDirectionApi} from '../service/Api';
import {useSelector} from 'react-redux';

const Outstation = ({navigation, route}) => {
  const {settings} = useSelector(state => state);
  const [date, setDate] = useState(new Date());
  const [state, setState] = useState({
    condition_outstation: 'single',
    direction_type_id: '4',
    booking_way: '1',
    from: {
      lat: 0,
      lng: 0,
      description: '',
      adminArea: '',
      locality: '',
      subAdminArea: '',
    },
    to: {
      lat: 0,
      lng: 0,
      description: '',
      adminArea: '',
      locality: '',
      subAdminArea: '',
    },
    radio_props: [
      {label: 'One way', value: 'single'},
      {label: 'Round way', value: 'round'},
    ],
    mode: '',
    type: '',
    pickup_date: '',
    pickup_time: '',
    return_date: '',
    return_time: '',
  });
  const [isLoading, setIsLoading] = useState(false);
  const [mapState, setMapState] = useState({
    description: '',
    lat: 0,
    lng: 0,
    adminArea: '',
    locality: '',
    subAdminArea: '',
  });
  const validateLocation = location => {
    const {adminArea, description, lat, lng} = location;
    return [adminArea, description, lat, lng].includes('');
  };
  const searchHandler = async () => {
    const {
      to,
      from,
      condition_outstation,
      pickup_date,
      pickup_time,
      direction_type_id,
      booking_way,
    } = state;
    const {booking_hour} = settings;
    let {return_date, return_time} = state;

    if (validateLocation(to) || validateLocation(from)) {
      alert('Please Select your Location');
      return;
    }
    if (pickup_date === '' || pickup_time === '') {
      alert('Please Select Pickup Date and Time');
      return;
    }
    const datetime =
      moment(`${pickup_date} ${pickup_time}`, 'YYYY-MM-DD hh:mm a').diff(
        moment(),
      ) / 60000;
    if (datetime < +booking_hour * 60) {
      alert(`You can only book before ${booking_hour} hours`);
      return;
    }
    if (condition_outstation === 'round') {
      if (return_date === '' || return_time === '') {
        alert('Please Select Return Date and Time');
        return;
      }
    } else {
      return_date = '';
      return_time = '';
    }
    const directionDetails = {
      origin: {
        latitude: from.lat,
        longitude: from.lng,
        description: from.description,
        adminArea: from.adminArea,
        locality: from.locality,
        subAdminArea: from.subAdminArea,
      },
      destination: {
        latitude: to.lat,
        longitude: to.lng,
        description: to.description,
        adminArea: to.adminArea,
        locality: to.locality,
        subAdminArea: to.subAdminArea,
      },
      adminArea: from.adminArea,
      locality: from.locality,
      subAdminArea: from.subAdminArea,
      distance: 0,
      duration: 0,
      condition: 'outstation',
      condition_outstation,
      direction_type_id,
      booking_way,
      pickup_date,
      pickup_time,
      return_date,
      return_time,
    };

    setIsLoading(true);
    const {origin, destination} = directionDetails;
    const GDAResponse = await GoogleDirectionApi(
      `${origin.latitude},${origin.longitude}`,
      `${destination.latitude},${destination.longitude}`,
    );
    if (GDAResponse.status && GDAResponse.code === 200) {
      directionDetails.distance = GDAResponse.data.distance.value / 1000;
      directionDetails.duration = Math.round(
        GDAResponse.data.duration.value / 60,
      );
      const {
        condition,
        condition_outstation,
        distance,
        locality,
        subAdminArea,
        adminArea,
        pickup_date,
        pickup_time,
        return_date,
        return_time,
      } = directionDetails;
      const headerTitle =
        condition_outstation === 'single' ? 'One Way Cab' : 'Round Way Cab';

      const body = {
        condition,
        total_km: distance,
        city: locality,
        state: adminArea,
        condition_outstation,
        pickup_date,
        pickup_time,
        return_date,
        return_time,
        airport_id: 0,
        lat: 0,
        lng: 0,
      };
      const {status = false, price_array = []} = await GetCabApi(body);
      setIsLoading(false);
      if (status && price_array.length) {
        navigation.navigate('SearchCab', {
          price_array,
          directionDetails,
          headerTitle,
        });
      } else {
        alert('Something went wrong');
      }
    } else {
      setIsLoading(false);
      alert(GDAResponse);
    }
  };
  const setLocation = (key, data) => {
    setMapState(data);
    setState({...state, [key]: data});
  };

  const DateTimeView = (label, defaultValue, mode, type) => (
    <View style={{flex: 0.45}}>
      <TouchableOpacity
        onPress={() => setState({...state, showPicker: true, mode, type})}>
        <TextField
          label={label}
          defaultValue={defaultValue}
          editable={false}
          baseColor={defaultValue === '' ? '#8F8F8F' : '#242E42'}
        />
      </TouchableOpacity>
    </View>
  );

  const showDateTimePicker = () => (
    <DateTimePicker
      value={date}
      minimumDate={new Date()}
      mode={state.mode}
      display="spinner"
      is24Hour={false}
      onChange={({type, nativeEvent}) => {
        if (type === 'set') {
          if (state.mode === 'time') {
            setState({
              ...state,
              showPicker: false,
              [`${state.type}_time`]: moment(nativeEvent.timestamp).format(
                'hh:mm a',
              ),
            });
          } else {
            setState({
              ...state,
              showPicker: false,
              [`${state.type}_date`]: moment(nativeEvent.timestamp).format(
                'YYYY-MM-DD',
              ),
            });
          }
          setDate(nativeEvent.timestamp);
        } else {
          setState({...state, showPicker: false});
        }
      }}
    />
  );
  return (
    <SafeAreaView style={styles.container}>
      {isLoading && <Loader />}
      {SimpleHeader('Outstation', navigation.goBack)}
      <View style={styles.formView}>
        <RadioForm
          formHorizontal={true}
          animation={true}
          style={{
            marginHorizontal: 20,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          {state.radio_props.map((obj, i) => (
            <RadioButton labelHorizontal={true} key={i}>
              <RadioButtonInput
                obj={obj}
                index={i}
                isSelected={state.condition_outstation === obj.value}
                onPress={() =>
                  setState({
                    ...state,
                    condition_outstation: obj.value,
                    booking_way: `${i + 1}`,
                  })
                }
                borderWidth={1}
                buttonInnerColor={'#F8A301'}
                buttonOuterColor={
                  state.condition_outstation === obj.value
                    ? '#F8A301'
                    : '#242E42'
                }
                buttonSize={15}
                buttonOuterSize={25}
                buttonStyle={{}}
                buttonWrapStyle={{}}
              />
              <RadioButtonLabel
                obj={obj}
                index={i}
                labelHorizontal={true}
                onPress={() =>
                  setState({
                    ...state,
                    condition_outstation: obj.value,
                    booking_way: `${i + 1}`,
                  })
                }
                labelStyle={{
                  paddingRight: 20,
                  fontSize: 18,
                  color: '#242E42',
                  fontFamily: 'Avenir-Medium',
                  fontWeight: '500',
                }}
                labelWrapStyle={{}}
              />
            </RadioButton>
          ))}
        </RadioForm>

        <TouchableOpacity
          onPress={() =>
            navigation.navigate('MapLocation', {
              type: 'airport',
              data: {
                key: 'from',
                callback: setLocation,
              },
            })
          }>
          <View>
            <TextField
              label={'From'}
              defaultValue={state.from.description}
              editable={false}
              baseColor={state.from.description ? '#242E42' : '#8F8F8F'}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('MapLocation', {
              type: 'airport',
              data: {
                key: 'to',
                callback: setLocation,
              },
            })
          }>
          <View>
            <TextField
              label={'To'}
              defaultValue={state.to.description}
              editable={false}
              baseColor={state.to.description ? '#242E42' : '#8F8F8F'}
            />
          </View>
        </TouchableOpacity>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          {DateTimeView('Pickup Date', state.pickup_date, 'date', 'pickup')}
          {DateTimeView('Pickup Time', state.pickup_time, 'time', 'pickup')}
        </View>
        {state.condition_outstation === 'round' && (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            {DateTimeView('Return Date', state.return_date, 'date', 'return')}
            {DateTimeView('Return Time', state.return_time, 'time', 'return')}
          </View>
        )}
        {state.showPicker && showDateTimePicker()}
        <TouchableOpacity onPress={searchHandler} style={styles.searchTouch}>
          <Text style={styles.searchText}>Search</Text>
        </TouchableOpacity>
      </View>

      {/* <View style={{margin: 20}}>
        <Text>{`lng : ${mapState.lat}`}</Text>
        <Text>{`lat : ${mapState.lng}`}</Text>
        <Text>{`locality : ${mapState.locality}`}</Text>
        <Text>{`subAdminArea : ${mapState.subAdminArea}`}</Text>
        <Text>{`adminArea : ${mapState.adminArea}`}</Text>
        <Text>{`description : ${mapState.description}`}</Text>
      </View> */}
    </SafeAreaView>
  );
};
export default Outstation;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  formView: {
    marginHorizontal: 15,
    marginTop: '20%',
    paddingHorizontal: 20,
    paddingVertical: 30,
    borderRadius: 20,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  dropDownTextStyle: {
    fontFamily: 'Avenir-Medium',
    fontSize: 17,
    color: '#242E42',
  },
  mapViewStyle: {
    flexDirection: 'row',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  textField: {
    borderWidth: 1,
    borderColor: '#AAAAAA',
    margin: 5,
    padding: 5,
    width: '70%',
  },
  spacer: {
    height: 10,
  },

  title: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
  searchTouch: {
    backgroundColor: '#09304B',
    borderRadius: 20,
    alignSelf: 'center',
    width: '60%',
    paddingVertical: 10,
    marginTop: 20,
  },
  searchText: {
    textAlign: 'center',
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 18,
    color: '#FFFFFF',
  },
  condition_outstation: {
    backgroundColor: 'red',
  },
});

import React from 'react';
import {ActivityIndicator, View} from 'react-native';

const Loader = () => {
  return (
    <View
      style={{
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
        width: '100%',
        height: '90%',
        zIndex: 1,
        elevation: 10,
        alignSelf: 'center',
      }}>
      <ActivityIndicator animating={true} size="large" color="#09304B" />
    </View>
  );
};
export default Loader;

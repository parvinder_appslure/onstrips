import React, {useEffect} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Dimensions,
  ImageBackground,
} from 'react-native';
import {useDispatch} from 'react-redux';
import {AsyncStorageGetUserId, GetProfileApi} from '../service/Api';
import * as actions from '../redux/actions';
import {StatusBarLight} from '../utils/CustomStatusBar';
const {width, height} = Dimensions.get('window');
const Splash = ({navigation, route}) => {
  const dispatch = useDispatch();
  useEffect(() => {
    (async () => {
      const user_id = await AsyncStorageGetUserId();
      console.log('user_id', user_id);
      if (user_id && user_id !== '') {
        const {status = false, data = {}} = await GetProfileApi({user_id});
        if (status) {
          dispatch(actions.Login(data));
        }
      }
      navigation.replace('DrawerNavigator');
    })();
  }, []);

  return (
    <View style={{flex: 1}}>
      <StatusBarLight />
      <ImageBackground
        source={require('../assets/splash-cab.png')}
        style={styles.container}>
        <Image source={require('../assets/logo.png')} style={styles.mainLogo} />
      </ImageBackground>
    </View>
  );
};

export default Splash;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainLogo: {
    height: 170,
    width: 210,
    resizeMode: 'contain',
    position: 'absolute',
  },
  bottomView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 'auto',
    marginBottom: 20,
  },
  text: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 1,
    color: '#FFFFFF',
  },
  bottomLogo: {
    height: 45,
    width: 100,
    resizeMode: 'contain',
    marginLeft: 5,
  },
});

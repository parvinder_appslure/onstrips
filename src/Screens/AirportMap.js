import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
  Dimensions,
  ImageBackground,
  StatusBar,
  SafeAreaView,
  FlatList,
} from 'react-native';
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Airport from './Airport';
import {Marker} from 'react-native-maps';
const GLOBAL = require('./Global');
import MapView from 'react-native-maps';
const window = Dimensions.get('window');
const {width, height} = Dimensions.get('window');
const SCREEN_HEIGHT = height;
const SCREEN_WIDTH = width;
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

import {TabBar} from 'react-native-tab-view';
import {Button} from 'react-native';
import {globalStyle} from './style';
// import {Button} from 'react-native-elements';
export default class AirportMap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      location: '',
      lat: GLOBAL.lat,
      long: GLOBAL.long,
      isShown: false,
      data: [],
    };
  }
  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
      animations: {
        setRoot: {
          waitForRender: false,
        },
      },
    };
  };

  _renderScene = ({route}) => {
    switch (route.key) {
      case 'first':
        return <Airport />;
      case 'second':
        return <View />;
      case 'third':
        return <View />;
      default:
        return null;
    }
  };

  drop = (itemData) => {
    //        alert(JSON.stringify(itemData))
    GLOBAL.airid = itemData.id;
    GLOBAL.airportLocation = itemData.name;

    GLOBAL.airportLongitude = parseFloat(itemData.lon);
    GLOBAL.airportLatiude = parseFloat(itemData.lat);

    this.setState({location: itemData.name});
    this.setState({isShown: false});

    this.mapView.animateToRegion(
      {
        latitude: parseFloat(itemData.lat),
        longitude: parseFloat(itemData.lon),
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      1000,
    );

    this.setState({lat: parseFloat(itemData.lat)});
    this.setState({long: parseFloat(itemData.lon)});
  };
  renderRowItem1 = (itemData) => {
    return (
      <TouchableOpacity onPress={() => this.drop(itemData.item)}>
        <View>
          <Text
            style={{
              width: 200,
              marginLeft: 10,
              fontFamily: GLOBAL.roman,
              color: 'black',
              height: 40,
              marginTop: 20,
            }}>
            {itemData.item.name}
          </Text>

          <View
            style={{width: 200, height: 1, backgroundColor: '#efefe4'}}></View>
        </View>
      </TouchableOpacity>
    );
  };
  _handleStateChange = (state) => {
    const url = 'http://139.59.76.223/cab/webservices/get_airports_by_state';
    fetch(url, {
      method: 'POST',
      headers: {
        'x-api-key':
          '$2y$12$MOOt6dmiClUmITafZDyR2edjeJzx.UiXzG/ArWY8fl.zhNSi6FUfy',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        state_id: '11',
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.status == true) {
          console.log(responseJson.data);
          this.setState({data: responseJson.data});
        }
      })
      .catch((error) => {
        console.error(error);
      });

    this.setState({location: GLOBAL.searchLocation});
    if (GLOBAL.searchLocation != '') {
      this.setState({lat: GLOBAL.searchLatiude});
      this.setState({long: GLOBAL.searchLongitude});

      this.mapView.animateToRegion(
        {
          latitude: GLOBAL.searchLatiude,
          longitude: GLOBAL.searchLongitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        },
        1000,
      );
    }
  };
  componentDidMount() {
    this.setState({lat: GLOBAL.lat});
    this.setState({long: GLOBAL.long});

    this.props.navigation.addListener('willFocus', this._handleStateChange);
  }
  change = () => {
    // alert(GLOBAL.airportLocation)

    GLOBAL.airportLocation = this.state.location;

    this.props.navigation.goBack();
  };

  renderTabBar(props) {
    return (
      <TabBar
        style={{
          backgroundColor: '#FFFFFF',
          elevation: 0,
          borderColor: 'white',
          borderBottomWidth: 2.5,
          height: 50,
        }}
        labelStyle={{color: 'grey', fontSize: 10, fontFamily: GLOBAL.roman}}
        {...props}
        indicatorStyle={{backgroundColor: Colors.blue, height: 2.5}}
      />
    );
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
        }}>
        <StatusBar
          barStyle="dark-content"
          backgroundColor={'transparent'}
          translucent={true}
        />
        <SafeAreaView style={{flex: 0, backgroundColor: Colors.blue}} />

        <View style={[globalStyle.header_1]}>
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
            style={{
              padding: 8,
            }}>
            <Image
              source={require('../assets/back2.png')}
              style={globalStyle.backImage}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flex: 0.95,
            }}
            onPress={() => this.setState({isShown: !this.state.isShown})}>
            <View
              style={{
                borderRadius: 4,
                backgroundColor: 'white',
                flexDirection: 'row',
                alignItems: 'center',
                padding: 5,
                paddingHorizontal: 10,
              }}>
              <Image
                source={require('../assets/location.png')}
                style={{
                  width: 20,
                  height: 20,
                  resizeMode: 'contain',
                }}
              />
              <Text
                style={{
                  fontFamily: GLOBAL.heavy,
                  fontSize: 16,
                  color: 'black',
                  flex: 1,
                  marginHorizontal: 10,
                }}>
                {this.state.location}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <MapView
          style={{flex: 1}}
          showsUserLocation={true}
          showsMyLocationButton={true}
          showsTraffic={true}
          ref={(ref) => (this.mapView = ref)}
          region={{
            latitude: +this.state.lat,
            longitude: +this.state.long,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }}>
          <Marker
            coordinate={{
              latitude: +this.state.lat,
              longitude: +this.state.long,
              latitudeDelta: LATITUDE_DELTA,

              longitudeDelta: LONGITUDE_DELTA,
            }}>
            <Image
              source={require('../assets/location_pin.png')}
              style={{height: 60, width: 60, resizeMode: 'contain'}}
            />
          </Marker>
        </MapView>

        <TouchableOpacity
          onPress={this.change}
          style={{
            backgroundColor: '#09304B',
            position: 'absolute',
            alignSelf: 'center',
            bottom: 10,
            width: '60%',
            borderRadius: 6,
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: GLOBAL.heavy,
              fontWeight: '900',
              fontSize: 17,
              color: '#FFFFFF',
              paddingVertical: 8,
            }}>
            Apply
          </Text>
        </TouchableOpacity>
        {/* <Button
          buttonStyle={{
            backgroundColor: '#09304B',
            // width: window.width - 20,
            borderRadius: 5,
            alignSelf: 'center',
            // position: 'absolute',
            // bottom: 200,
            height: 50,
          }}
          titleStyle={{fontFamily: GLOBAL.roman, fontSize: 17}}
          onPress={this.change}
          title="Apply"
        /> */}

        {this.state.isShown == true && (
          <View
            style={{
              marginLeft: 60,
              height: 200,
              width: 300,
              position: 'absolute',
              top: 80,
              backgroundColor: 'white',
              borderRadius: 4,
            }}>
            <FlatList
              data={this.state.data}
              keyExtractor={this._keyExtractorss}
              horizontal={false}
              renderItem={this.renderRowItem1}
            />
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  button: {
    borderWidth: 1,
    borderColor: '#000000',
    margin: 5,
    padding: 5,
    width: '70%',
    backgroundColor: '#DDDDDD',
    borderRadius: 5,
  },
  textField: {
    borderWidth: 1,
    borderColor: '#AAAAAA',
    margin: 5,
    padding: 5,
    width: '70%',
  },
  spacer: {
    height: 10,
  },
  scene: {
    flex: 1,
  },

  title: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
  },
});

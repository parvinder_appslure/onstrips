import React, {useEffect, useState, useRef} from 'react';
import {
  TouchableOpacity,
  View,
  Image,
  Text,
  SafeAreaView,
  StyleSheet,
} from 'react-native';
import {StatusBarDark} from '../utils/CustomStatusBar';
import {globalStyle} from './style';
import MapView, {Marker} from 'react-native-maps';
import {useSelector} from 'react-redux';
import {
  GeocoderLocation,
  LatitudeDelta,
  LongitudeDelta,
  modeOfArray,
} from '../service/Api';

const MapLocation = ({navigation, route}) => {
  const mapRef = useRef();
  const {coords} = useSelector(state => state);
  const [state, setState] = useState({
    search: '',
    adminArea: '',
    latitude: 0,
    longitude: 0,
    locality: '',
    subAdminArea: '',
    adminArea: '',
  });
  const [region, setRegion] = useState({
    latitude: parseFloat(coords.latitude),
    longitude: parseFloat(coords.longitude),
    latitudeDelta: LatitudeDelta,
    longitudeDelta: LongitudeDelta(),
  });

  const filterAddress = result => ({
    locality: modeOfArray(
      result.map(({locality}) => locality).filter(item => item !== null),
    ),
    subAdminArea: modeOfArray(
      result
        .map(({subAdminArea}) => subAdminArea)
        .filter(item => item !== null),
    ),
    adminArea: modeOfArray(
      result.map(({adminArea}) => adminArea).filter(item => item !== null),
    ),
  });

  useEffect(() => {
    if (route.params) {
      const {description = '', location = {lat: 0, lng: 0}} = route.params;
      if (description !== '') {
        coordinateUpdateHandler(description, location);
      } else {
        console.log('params not change');
      }
    }
  }, [route.params]);
  const showMapView = () => (
    <MapView
      ref={mapRef}
      style={styles.map}
      initialRegion={region}
      showsUserLocation={true}
      showsMyLocationButton={false}
      pitchEnabled={true}
      rotateEnabled={true}
      zoomEnabled={true}
      scrollEnabled={true}
      onRegionChangeComplete={region => setRegion(region)}
      onPress={mapViewOnPressHandler}>
      {showMarker()}
    </MapView>
  );
  const showMarker = () =>
    state.latitude !== 0 &&
    state.longitude !== 0 && (
      <Marker
        coordinate={{latitude: state.latitude, longitude: state.longitude}}>
        <Image
          source={require('../assets/location.png')}
          style={styles.markerIcon}
        />
      </Marker>
    );
  const coordinateUpdateHandler = async (description, location) => {
    const result = await GeocoderLocation({
      latitude: location.lat,
      longitude: location.lng,
    });
    if (result.length === 0) return;
    const {formattedAddress} = result[0];
    if (formattedAddress) {
      const {locality, subAdminArea, adminArea} = filterAddress(result);
      setState({
        ...state,
        search: description,
        latitude: location.lat,
        longitude: location.lng,
        locality,
        subAdminArea,
        adminArea,
      });
      mapRef.current.animateToRegion({
        latitude: location.lat,
        longitude: location.lng,
        latitudeDelta: region.latitudeDelta,
        longitudeDelta: region.longitudeDelta,
      });
    }
  };
  const mapViewOnPressHandler = async ({nativeEvent}) => {
    const {coordinate} = nativeEvent;
    const result = await GeocoderLocation(coordinate);
    if (result.length === 0) return;
    const {formattedAddress} = result[0];
    if (formattedAddress) {
      const {locality, subAdminArea, adminArea} = filterAddress(result);
      setState({
        ...state,
        ...coordinate,
        search: formattedAddress,
        locality,
        subAdminArea,
        adminArea,
      });
    }
  };

  const applyHandler = () => {
    const {
      search = '',
      latitude = 0,
      longitude = 0,
      adminArea = '',
      locality = '',
      subAdminArea = '',
    } = state;
    if (
      search !== '' &&
      adminArea !== '' &&
      locality !== '' &&
      latitude !== 0 &&
      longitude !== 0
    ) {
      const {type = '', data} = route.params;
      switch (type) {
        case 'airport':
          data.callback(data.key, {
            description: search,
            lat: latitude,
            lng: longitude,
            locality,
            subAdminArea,
            adminArea,
          });
          navigation.goBack();
          break;
      }
    } else {
      alert('Something went wrong1');
    }
  };
  return (
    <SafeAreaView style={styles.container}>
      <StatusBarDark />
      <View style={globalStyle.header_1}>
        <TouchableOpacity
          style={styles.h_backTouch}
          onPress={navigation.goBack}>
          <Image
            style={globalStyle.backImage}
            source={require('../assets/backicon.png')}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.h_searchView}
          onPress={() => navigation.navigate('MapSearchLocation')}>
          <Image
            source={require('../assets/location.png')}
            style={styles.locationImage}
          />
          <Text style={styles.h_textInput}>{state.search}</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.map}>{showMapView()}</View>
      {state.search !== '' && (
        <TouchableOpacity style={styles.btn_touch} onPress={applyHandler}>
          <Text style={styles.btn_label}>Apply</Text>
        </TouchableOpacity>
      )}
    </SafeAreaView>
  );
};

export default MapLocation;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  map: {
    flex: 1,
  },

  markerIcon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
  h_backTouch: {
    padding: 8,
  },
  locationImage: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
  },
  h_textInput: {
    paddingHorizontal: 15,
  },
  h_searchView: {
    flexDirection: 'row',
    backgroundColor: 'white',
    alignItems: 'center',
    flex: 1,
    marginRight: 10,
    borderRadius: 10,
    padding: 10,
  },
  btn_touch: {
    backgroundColor: '#09304B',
    position: 'absolute',
    bottom: 30,
    alignSelf: 'center',
    width: '70%',
    paddingVertical: 10,
    borderRadius: 8,
    alignItems: 'center',
  },
  btn_label: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 17,
    color: '#FFFFFF',
  },
});

import {StyleSheet} from 'react-native';
import Global from './Global.js';

export const globalStyle = StyleSheet.create({
  header: {
    flexDirection: 'row',
    backgroundColor: '#FBD303',
    paddingTop: 45,
    paddingBottom: 15,
    paddingHorizontal: 10,
    justifyContent: 'center',
  },
  header_1: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#FBD303',
    paddingTop: 45,
    paddingBottom: 15,
    paddingHorizontal: 10,
  },
  header_2: {
    flexDirection: 'row',
    backgroundColor: '#FBD303',
    paddingTop: 30,
    paddingBottom: 15,
    paddingHorizontal: 10,
    justifyContent: 'center',
  },
  headerBackTouch: {
    position: 'absolute',
    padding: 10,
    bottom: 5,
    left: 5,
  },
  backImage: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
  headerTitleView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerTitle: {
    fontFamily: Global.heavy,
    fontWeight: '900',
    fontSize: 22,
    color: '#09304B',
  },
});
export const buttonStyle = StyleSheet.create({
  touch: {
    backgroundColor: '#09304B',
    width: '60%',
    alignSelf: 'center',
    borderRadius: 6,
    paddingVertical: 6,
    marginTop: 20,
  },
  title: {
    fontFamily: Global.heavy,
    fontSize: 20,
    fontWeight: '500',
    color: 'white',
    textAlign: 'center',
  },
});

export const containerStyle = StyleSheet.create({
  c1: {backgroundColor: '#FBD303', flex: 1},
  c2: {backgroundColor: '#FBD303', flex: 1, justifyContent: 'center'},
});

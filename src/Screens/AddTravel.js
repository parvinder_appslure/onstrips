import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  SafeAreaView,
} from 'react-native';

import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';
import {Dropdown} from 'react-native-material-dropdown';

const GLOBAL = require('./Global');

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {TextField} from 'react-native-material-textfield';
import {buttonStyle, globalStyle} from './style';
import Global from './Global';
import {StatusBarDark} from '../utils/CustomStatusBar';
import {SimpleHeader} from '../utils/Header';
import {useStore} from 'react-redux';
import {AddMemberApi, EditMemberApi} from '../service/Api';
const paxtype = {
  adult: '1',
  child: '2',
  infant: '3',
};
const AddTravel = ({navigation, route}) => {
  const {user} = useStore().getState();
  const [state, setState] = useState({
    name: '',
    email: '',
    mobile: '',
    gender: '1',
    title: '',
    firstname: '',
    lastname: '',
    address: '',
    dob: '',
    passport: '',
    passportExpire: '',
    showPicker: false,
    pickerFor: 'dob',
  });

  const handleClick = async () => {
    const {key, type, addTravelCallback} = route.params;

    const years = moment().diff(state.dob, 'years');
    const days = moment().diff(state.dob, 'days');
    if (route.params.key === 'adult' && years < 18) {
      alert('Adult age should be greater than 18 years');
      return;
    }
    if (route.params.key === 'child' && (years > 17 || years < 2)) {
      alert('Child age should be in-between 2 or 18 years');
      return;
    }
    if (route.params.key === 'infant' && (years > 1 || days < 7)) {
      alert('Infant age should be in-between 7 days or less than 2 years');
      return;
    }
    const body = {
      user_id: user.id,
      id: user.id,
      title: state.title,
      firstname: state.firstname,
      lastname: state.lastname,
      paxtype: paxtype[route.params.key],
      dateofbirth: state.dob,
      gender: ['Mr', 'Mrts'].includes(state.title) ? '1' : '2',
      passportNo: state.passport,
      passportExpiry: state.passportExpire,
      addressLine: state.address,
      city: '',
      countryCode: 'INR',
      cellCountryCode: '+91',
      contactNo: state.mobile,
      nationality: 'India',
      email: state.email,
      isLeadPax: '',
    };
    console.log(JSON.stringify(body, null, 2));
    // return;
    let _status = false;
    if (type === 'add') {
      const {status = false} = await AddMemberApi(body);
      _status = status;
      console.log('add api');
    } else {
      const {status = false} = await EditMemberApi(body);
      _status = status;
      console.log('edit api');
    }
    alert(
      _status
        ? `Successfully ${type === 'add' ? 'Added' : 'Updated'}`
        : 'Something went wrong',
    );
    if (_status) {
      addTravelCallback();
      navigation.goBack();
    }
  };

  let data = {
    adult: [
      {
        value: 'Mr',
      },
      {
        value: 'Mrs',
      },
    ],
    child: [
      {
        value: 'Miss',
      },
      {
        value: 'Mrts',
      },
    ],
  };
  useEffect(() => {
    console.log(JSON.stringify(route.params, null, 2));
    const {key, type} = route.params;
    if (type === 'add') {
      console.log('add');
    } else if (type === 'edit') {
      console.log('edit');
      const {
        firstname,
        lastname,
        title,
        dateofbirth,
        passportNo,
        passportExpiry,
        contactNo,
        email,
        addressLine,
      } = route.params.data;
      setState({
        ...state,
        name: '',
        email,
        mobile: contactNo,
        gender: '1',
        title,
        firstname,
        lastname,
        address: addressLine,
        dob: dateofbirth,
        passport: passportNo,
        passportExpire: passportExpiry,
      });
    }
  }, []);
  return (
    <View style={styles.container}>
      <StatusBarDark />
      <SafeAreaView style={{flex: 0, backgroundColor: '#FBD303'}} />
      {SimpleHeader('Traveller Details', navigation.goBack)}

      <KeyboardAwareScrollView>
        <View style={styles.formView}>
          <Text style={styles.formTitle}>Traveller Information</Text>

          <View style={styles.formInnerView}>
            <Dropdown
              rippleOpacity={0.54}
              dropdownPosition={0.2}
              onChangeText={(title) => setState({...state, title})}
              itemTextStyle={styles.db_itemTextStyle}
              label="Select title"
              value={state.title}
              data={data[route.params.key === 'adult' ? 'adult' : 'child']}
            />

            <TextField
              label={'First Name'}
              defaultValue={state.firstname}
              onChangeText={(firstname) => setState({...state, firstname})}
              tintColor="grey"
            />
            <TextField
              label={'Last Name'}
              defaultValue={state.lastname}
              onChangeText={(lastname) => setState({...state, lastname})}
              tintColor="grey"
            />
            <TextField
              label={'Email'}
              defaultValue={state.email}
              onChangeText={(email) => setState({...state, email})}
              tintColor="grey"
            />

            <TextField
              label={'Mobile'}
              defaultValue={state.mobile}
              onChangeText={(mobile) => setState({...state, mobile})}
              tintColor="grey"
            />
            <TextField
              label={'Address'}
              defaultValue={state.address}
              onChangeText={(address) => setState({...state, address})}
              tintColor="grey"
            />
            <TouchableOpacity
              onPress={() =>
                setState({...state, showPicker: true, pickerFor: 'dob'})
              }>
              <TextField
                label={'Date of Birth'}
                defaultValue={state.dob}
                editable={false}
                tintColor="grey"
              />
            </TouchableOpacity>

            <TextField
              label={'Passport No'}
              defaultValue={state.passport}
              onChangeText={(passport) => setState({...state, passport})}
              tintColor="grey"
            />
            <TouchableOpacity
              onPress={() =>
                setState({
                  ...state,
                  showPicker: true,
                  pickerFor: 'passportExpire',
                })
              }>
              <TextField
                label={'Passport Expire Date'}
                defaultValue={state.passportExpire}
                editable={false}
                tintColor="grey"
              />
            </TouchableOpacity>
          </View>
          {state.showPicker && (
            <DateTimePicker
              value={new Date()}
              maximumDate={state.pickerFor === 'dob' ? new Date() : null}
              mode={state.mode}
              display="default"
              is24Hour={true}
              onChange={({type, nativeEvent}) => {
                console.log(nativeEvent);
                if (type === 'set') {
                  setState({
                    ...state,
                    showPicker: false,
                    [state.pickerFor]: moment(nativeEvent.timestamp).format(
                      'YYYY-MM-DD',
                    ),
                  });
                } else {
                  setState({...state, showPicker: false});
                }
              }}
            />
          )}
        </View>
        <TouchableOpacity
          style={[buttonStyle.touch, {marginBottom: 20}]}
          onPress={handleClick}>
          <Text style={buttonStyle.title}>Submit</Text>
        </TouchableOpacity>
      </KeyboardAwareScrollView>
    </View>
  );
};

export default AddTravel;
const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: 'white'},
  headerView: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#FBD303',
    paddingTop: 35,
    paddingBottom: 15,
    paddingHorizontal: 10,
  },
  formView: {
    backgroundColor: 'white',
    borderColor: '#f7f7f7',
    margin: 20,
    padding: 10,
    borderRadius: 6,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.6,
    shadowRadius: 2,
    elevation: 5,
  },
  formTitle: {
    fontFamily: GLOBAL.roman,
    fontSize: 17,
    color: '#8a8a8f',
    paddingVertical: 10,
  },
  formInnerView: {
    marginHorizontal: 10,
    marginBottom: 15,
  },
  db_itemTextStyle: {
    fontFamily: GLOBAL.heavy,
    fontSize: 16,
    color: 'grey',
  },
  genderView: {margin: 20},
  genderText: {
    color: '#8f8f8f',
    fontFamily: GLOBAL.roman,
    fontSize: 16,
    marginBottom: 20,
  },
});

import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
  Dimensions,
  StatusBar,
  SafeAreaView,
  FlatList,
} from 'react-native';
const window = Dimensions.get('window');
import moment from 'moment';
import RBSheet from 'react-native-raw-bottom-sheet';
import airlineicon from './airlineicon';
import {Api_removeToken, Api_updateToken, _post} from './Api';
const GLOBAL = require('./Global');
import CheckBox from '@react-native-community/checkbox';
import {ScrollView} from 'react-native-gesture-handler';
export default class FlightDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      discount: 0,
      flightResponse: [],
      flight: [],
      checked: false,
      partial: false,
      full: false,
      offer: [],
      debit: false,
      credit: false,
      wallet: false,
      isShown: false,
      finalPrice: 0,
      detail: [],
      lodingView: 'search',
      data: this.props.route.params.data,
      sort: 0,
      filter: false,
      flightId: '',
      // flightSelect: {},
      flightFare: '',
      fareCalender: [],
      departure_date: this.props.route.params.data.departure_date,
      chk_direct: false,
      chk_lcc: false,
      chk_gds: false,
      airlineList: [],
      timeSort: '',
      TokenId: this.props.route.params.data.TokenId,
      TraceId: '',
      ResultIndex: '',
    };
  }
  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
      animations: {
        setRoot: {
          waitForRender: false,
        },
      },
    };
  };

  convertMinsToTime = mins => {
    let hours = Math.floor(mins / 60);
    let minutes = mins % 60;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    return `${hours}hrs:${minutes}mins`;
  };

  flightTitle(flight, fare, flightCode, flightNumber) {
    return (
      <View style={styles.ft_container}>
        <Image source={airlineicon[flightCode]} style={styles.ft_image} />
        <View style={styles.ft_view}>
          <Text style={styles.ft_flightText}>{flight}</Text>
          <Text style={styles.ft_codeText}>
            {`${flightCode} - ${flightNumber}`}
          </Text>
        </View>
        <Text style={styles.ft_fareText}>₹{fare}</Text>
      </View>
    );
  }

  bookButtonView(navigate) {
    return (
      <View style={styles.bbv_container}>
        <View style={styles.bbv_view}>
          <Text style={styles.bbv_fareText}>
            {`\u20B9 ${parseInt(this.state.flightFare)
              .toFixed(0)
              .replace(/(\d)(?=(\d\d)+\d$)/g, '$1,')}`}
          </Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            const {adultcount, childcount, infantcount} = this.state.data;
            const {TokenId, ResultIndex, TraceId} = this.state;
            navigate('FlightBooking', {
              TokenId,
              ResultIndex,
              TraceId,
              adultcount,
              childcount,
              infantcount,
            });
          }}
          style={styles.bbv_touch}>
          <Text style={styles.bbv_continueText}>CONTINUE</Text>
        </TouchableOpacity>
      </View>
    );
  }
  handleAirlineListCheck(AirlineCode, value) {
    let airlineList = [...this.state.airlineList];
    airlineList.find(item => item.AirlineCode === AirlineCode).check = value;
    this.setState({airlineList});
  }
  timeSortHandler(value) {
    let timeSort =
      this.state.timeSort === '' || this.state.timeSort !== value ? value : '';
    this.setState({timeSort});
  }
  timeSortTouch = (key, title) => {
    return (
      <TouchableOpacity
        onPress={() => this.timeSortHandler(key)}
        style={[
          styles.time_touch,
          this.state.timeSort === key && styles.time_touch_active,
        ]}>
        <Text
          style={[
            styles.time_text,
            this.state.timeSort === key && styles.time_text_active,
          ]}>
          {title}
        </Text>
      </TouchableOpacity>
    );
  };

  filterScreen() {
    return (
      <RBSheet
        ref={ref => {
          this.RBSheet = ref;
        }}
        height={650}
        openDuration={250}
        customStyles={{
          container: {
            borderTopRightRadius: 15,
            borderTopLeftRadius: 15,
            paddingTop: 15,
          },
        }}>
        <View>
          <View style={styles.chk_mainView}>
            {[
              {title: 'Non-Stop Flight', key: 'chk_direct'},
              {title: 'LCC', key: 'chk_lcc'},
              {title: 'GDS', key: 'chk_gds'},
            ].map(({title, key}) => (
              <>
                <View style={styles.chk_view}>
                  <CheckBox
                    tintColors={{true: '#F8A301', false: '#242E42'}}
                    value={this.state[key]}
                    onValueChange={newValue => {
                      this.setState({[key]: newValue});
                    }}
                  />
                </View>
                <Text style={styles.chk_mainText}>{title}</Text>
              </>
            ))}
          </View>
          <Text style={styles.al_title}>Timing</Text>
          <View style={styles.time_container}>
            <View style={styles.time_view}>
              {this.timeSortTouch('1', 'Morning 6 AM-12')}
              {this.timeSortTouch('2', 'Afternoon 12-6 PM')}
            </View>
            <View style={styles.time_view}>
              {this.timeSortTouch('3', 'Evening 6 PM-12')}
              {this.timeSortTouch('4', 'Night 12-6 AM')}
            </View>

            <Text style={styles.al_title}>Sort By</Text>
            <View style={styles.sort_container}>
              {['Cheapest', 'Best', 'Earliest', 'Shortest'].map(
                (title, index) => (
                  <TouchableOpacity
                    key={`sort_${title}`}
                    onPress={() => this.setState({sort: +index, filter: false})}
                    style={[
                      {
                        backgroundColor:
                          this.state.sort === index ? '#09304B' : '#FBD303',
                      },
                      styles.sort_touch,
                    ]}>
                    <Text
                      style={[
                        styles.textRoman,
                        this.state.sort === index && {color: 'white'},
                      ]}>
                      {title}
                    </Text>
                  </TouchableOpacity>
                ),
              )}
            </View>

            <Text style={styles.al_title}>Airlines</Text>
            <ScrollView>
              <View style={styles.al_mainView}>
                {this.state.airlineList.map(
                  ({AirlineCode, AirlineName, check}) => (
                    <View style={styles.al_view}>
                      <CheckBox
                        tintColors={{true: '#F8A301', false: '#242E42'}}
                        value={check}
                        onValueChange={value =>
                          this.handleAirlineListCheck(AirlineCode, value)
                        }
                      />
                      <Text style={styles.al_text}>{AirlineName}</Text>
                    </View>
                  ),
                )}
              </View>
            </ScrollView>
          </View>
        </View>
      </RBSheet>
    );
  }
  renderText(value) {
    return (
      <Text
        style={{
          color: '#09304B80',
          fontFamily: GLOBAL.roman,
          fontSize: 14,
          textAlign: 'center',
        }}>
        {value}
      </Text>
    );
  }
  renderItem = itemData => {
    const selected = itemData.item.flightId === this.state.flightId;
    const segment = itemData.item.Segments[0];
    const segmentLength = segment.length;
    const timeFormat = time => moment(time).format('h:mm:a');
    const flightDetails = {
      originCode: segment[0].Origin.Airport.AirportCode,
      originTerminal: segment[0].Origin.Airport.Terminal,
      originTime: timeFormat(segment[0].Origin.DepTime),
      destinationCode:
        segment[segmentLength - 1].Destination.Airport.AirportCode,
      destinationTime: timeFormat(
        segment[segmentLength - 1].Destination.ArrTime,
      ),
      duration: this.convertMinsToTime(
        moment(segment[segmentLength - 1].Destination.ArrTime).diff(
          moment(segment[0].Origin.DepTime),
          'minute',
        ),
      ),
      destinationTerminal:
        segment[segmentLength - 1].Destination.Airport.Terminal,
      stop: 'Non-Stop',
    };
    if (segmentLength > 1) {
      flightDetails.stop = `${segmentLength - 1} Stop`;
      for (let i = 0; i < segmentLength - 1; i++) {
        flightDetails.stop = `${flightDetails.stop}${i === 0 ? '\n' : ','}${
          segment[i].Destination.Airport.AirportCode
        }`;
      }
    }

    return (
      <TouchableOpacity
        onPress={() => {
          console.log(this.state.flightId);
          console.log(JSON.stringify(itemData, null, 2));
          this.setState({
            flightId: itemData.item.flightId,
            // flightSelect: itemData.item,
            ResultIndex: itemData.item.ResultIndex,
            flightFare: itemData.item.Fare.PublishedFare,
          });
        }}
        style={[
          {
            backgroundColor: selected ? '#FFF9C4' : 'white',
            margin: 15,
            borderRadius: 8,
          },
          !selected && styles.itemSelected,
        ]}>
        <View style={styles.item_view}>
          <View style={styles.item_view_1}>
            <View style={styles.item_view_left}>
              <Text style={styles.originCode}>{flightDetails.originCode}</Text>
              <Text style={styles.originTime}>{flightDetails.originTime}</Text>

              <Text style={styles.originTerminal}>
                {`Terminal ${flightDetails.originTerminal || '-'}`}
              </Text>
            </View>
            <View style={{justifyContent: 'center'}}>
              <Text style={styles.flightDetails}>{flightDetails.duration}</Text>
              <Image
                source={require('../assets/cad.png')}
                style={styles.cadImage}
              />
              <Text style={styles.flightStop}>{flightDetails.stop}</Text>
            </View>
            <View style={styles.item_view_right}>
              <Text style={styles.destinationCode}>
                {flightDetails.destinationCode}
              </Text>
              <Text style={styles.destinationTime}>
                {flightDetails.destinationTime}
              </Text>
              <Text style={styles.destinationTerminal}>
                {`Terminal ${flightDetails.destinationTerminal || '-'}`}
              </Text>
            </View>
          </View>
          <View style={styles.item_hz} />
          {this.flightTitle(
            segment[0].Airline.AirlineName,
            itemData.item.Fare.PublishedFare,
            segment[0].Airline.AirlineCode,
            segment[0].Airline.FlightNumber,
          )}
        </View>
      </TouchableOpacity>
    );
  };

  flightSearchList() {
    console.log('search--------');
    const data = {
      ...this.state.data,
      departure_date: this.state.departure_date,
    };
    consolejson(data);
    _post('search_flights', data)
      .then(async responseJson => {
        console.log('response\n\n');
        if (responseJson.status == true) {
          console.log('search success');
          const TraceId = responseJson.TraceId;

          // let flight = responseJson.Results[0];

          let airlineList = [];
          let flightResponse = responseJson.Results[0];
          flightResponse.map((item, index) => {
            item['flightId'] = 'id_' + index;
            item.Segments[0].forEach(({Airline}) => {
              const {AirlineCode, AirlineName} = Airline;
              if (
                airlineList.find(obj => obj.AirlineCode === AirlineCode) ===
                undefined
              ) {
                airlineList.push({AirlineCode, AirlineName, check: false});
              }
            });
          });
          let flight = [...flightResponse];
          if (flightResponse.length === 0) {
            console.log('flight zero');
            this.setState({lodingView: 'fail'});
            return;
          }
          this.setState({
            flightFare: flight[0].Fare.PublishedFare,
            // flightSelect: flight[0],
            ResultIndex: flight[0].ResultIndex,
            TraceId,
            flightId: 'id_0',
            detail: responseJson,
            airlineList,
            flight,
            flightResponse,
            lodingView: 'ready',
            chk_direct: false,
            chk_gds: false,
            chk_lcc: false,
            timeSort: '',
            sort: 0,
          });
        } else {
          console.log(responseJson);
          const result = Api_removeToken();
          if (result) {
            const {status, tokenId} = await Api_updateToken();
            if (status) {
              const data = {...this.state.data};
              data.TokenId = tokenId;
              this.setState({data, TokenId: tokenId});
              this.flightSearchList();
              this.fareCalenderList();
            }
          }
        }
      })
      .catch(error => {
        console.error(error);
        this.setState({lodingView: 'fail'});
      });
  }
  fareCalenderList() {
    _post('get_calendar_fare', this.state.data)
      .then(res => {
        if (res.status) {
          this.setState({fareCalender: res.searchResults});
        } else {
        }
      })
      .catch(error => console.log(error));
  }
  componentDidMount() {
    this.flightSearchList();
    this.fareCalenderList();
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    let update = false;
    if (this.state.sort !== prevState.sort) {
      update = true;
    }
    if (this.state.departure_date !== prevState.departure_date) {
      console.log('updating ...........');
      this.setState({lodingView: 'search'});
      this.flightSearchList();
    }
    if (this.state.airlineList !== prevState.airlineList) {
      console.log('update5555');
      update = true;
    }
    if (this.state.chk_direct !== prevState.chk_direct) {
      update = true;
    }
    if (this.state.chk_gds !== prevState.chk_gds) {
      update = true;
    }
    if (this.state.chk_lcc !== prevState.chk_lcc) {
      update = true;
    }
    if (this.state.timeSort !== prevState.timeSort) {
      update = true;
    }
    if (update) {
      const {chk_lcc, chk_gds, chk_direct, airlineList, timeSort, sort} =
        this.state;
      let flight = [...this.state.flightResponse];
      let arr = [];
      airlineList.map(item => {
        if (item.check) {
          arr.push(item.AirlineCode);
        }
      });
      // direct flight
      if (chk_direct)
        flight = flight.filter(item => item.Segments[0].length === 1);
      // lcc and gds flight
      if (chk_lcc ^ chk_gds)
        flight = flight.filter(item => item.IsLCC === chk_lcc);
      // specific airline
      if (arr.length)
        flight = flight.filter(item => arr.includes(item.AirlineCode));
      // time category sort
      if (timeSort) {
        flight = flight.filter(
          item =>
            +timeSort % 4 ===
            Math.floor(
              +moment(item.Segments[0][0].Origin.DepTime).format('HH') / 6,
            ),
        );
      }

      switch (sort) {
        case 0:
          flight = flight.sort((X, Y) =>
            X.Fare.PublishedFare > Y.Fare.PublishedFare ? 1 : -1,
          );
          break;
        case 1:
          break;
        case 2:
          flight = flight.sort((X, Y) =>
            X.Segments[0][0].Origin.DepTime > Y.Segments[0][0].Origin.DepTime
              ? 1
              : -1,
          );
          break;
        case 3:
          flight = flight.sort((X, Y) =>
            X.Segments[0][0].Duration > Y.Segments[0][0].Duration ? 1 : -1,
          );
          break;
      }
      if (flight.length) {
        this.setState({
          flight,
          // flightSelect: flight[0],
          flightId: flight[0].flightId,
          ResultIndex: flight[0].ResultIndex,
          flightFare: flight[0].Fare.PublishedFare,
        });
      } else {
        this.setState({
          flight: [],
          // flightSelect: {},
          flightId: '',
          ResultIndex: '',
          flightFare: '--',
        });
      }
      console.log('flight lenght', flight.length);
    }
  }

  handleCheckBox = () => this.setState({checked: !this.state.checked});
  showFareCalender = () => (
    <View style={styles.fc_view}>
      <FlatList
        data={this.state.fareCalender}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        renderItem={({item}) => {
          const DepartureDate = moment(item.DepartureDate).format('YYYY-MM-DD');
          const isSelected =
            this.state.departure_date === DepartureDate ? '_active' : '';
          return (
            <TouchableOpacity
              style={styles['fc_touch' + isSelected]}
              onPress={() => this.setState({departure_date: DepartureDate})}>
              <Text
                style={[
                  styles['fc_text' + isSelected],
                  {backgroundColor: 'golden'},
                ]}>
                {moment(item.DepartureDate).format('DD-MMM')}
              </Text>
              <Text
                style={
                  styles['fc_text' + isSelected]
                }>{`\u20b9 ${~~item.Fare}`}</Text>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
  noFlightView(source, h1, h2) {
    return (
      <>
        <Image source={source} style={styles.noFlightImage} />
        <Text style={styles.error_text_1}>{h1}</Text>
        <Text style={styles.error_text_2}>{h2}</Text>
      </>
    );
  }
  render() {
    const {navigate} = this.props.navigation;
    const subTitleText = `${moment(this.state.departure_date).format(
      'DD-MMM',
    )} ${GLOBAL.flide.totalCount} Travellers | ${GLOBAL.flide.classflight}`;

    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="dark-content"
          translucent={true}
          backgroundColor={'transparent'}
        />
        <SafeAreaView style={styles.safeAreaView} />
        <View style={styles.header_view}>
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
            style={styles.header_backTouch}>
            <Image
              source={require('../assets/back2.png')}
              style={styles.header_backImage}
            />
          </TouchableOpacity>
          <View style={styles.header_titleView}>
            <Text style={styles.header_titleText}>
              {this.state.data.flight_from} - {this.state.data.flight_to}
            </Text>
            <Text style={styles.header_subTitleText}>{subTitleText}</Text>
          </View>
          {this.state.lodingView === 'ready' && (
            <TouchableOpacity
              style={styles.header_filterTouch}
              // onPress={() => this.setState({ filter: !this.state.filter })}
              onPress={() => this.RBSheet.open()}>
              <Image
                source={require('../assets/filter-filled-tool-symbol.png')}
                style={styles.header_filterImage}
              />
            </TouchableOpacity>
          )}
        </View>
        {this.state.lodingView === 'search' &&
          this.noFlightView(
            require('../assets/flightanimation.gif'),
            `Hold Up Tight!`,
            `Fetching best Fare for You`,
          )}
        {this.state.lodingView === 'fail' &&
          this.noFlightView(
            require('../assets/no-travelling.png'),
            `No flights found`,
            `No flights found on this route for the requested date.`,
          )}
        {this.state.lodingView === 'ready' && (
          <>
            {this.state.fareCalender.length !== 0 && this.showFareCalender()}
            {this.state.flightId !== '' && (
              <FlatList
                contentContainerStyle={styles.flatlist_contentContainer}
                data={this.state.flight}
                // keyExtractor={this._keyExtractorss}
                horizontal={false}
                renderItem={this.renderItem}
                numColumns={1}
                extraData={this.state.flightId}
              />
            )}

            {!this.state.filter &&
              this.state.flightId !== '' &&
              this.bookButtonView(navigate)}
            {this.state.flightId === '' &&
              this.noFlightView(
                require('../assets/no-travelling.png'),
                `No flights found`,
                `You might want to remove a filter to broaden your search criteria.`,
              )}
          </>
        )}
        {this.filterScreen()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  safeAreaView: {flex: 0, backgroundColor: '#FBD303'},
  header_view: {
    flexDirection: 'row',
    backgroundColor: '#FBD303',
    paddingTop: 25,
    paddingBottom: 10,
    paddingHorizontal: 10,
    alignItems: 'center',
  },
  header_backTouch: {
    padding: 5,
    paddingRight: 10,
  },
  header_backImage: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
  header_titleView: {
    paddingHorizontal: 10,
  },
  header_titleText: {
    fontFamily: GLOBAL.heavy,
    fontSize: 18,
    color: '#09304B',
  },
  header_subTitleText: {
    fontFamily: GLOBAL.roman,
    fontSize: 12,
    color: '#09304B',
  },
  header_filterTouch: {
    marginLeft: 'auto',
    padding: 10,
  },
  header_filterImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  searchImage: {
    width: 150,
    height: 150,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: '30%',
  },
  noFlightImage: {
    alignSelf: 'center',
    width: 150,
    height: 150,
    resizeMode: 'contain',
    marginTop: '30%',
  },
  flatlist_contentContainer: {
    paddingTop: 10,
    paddingBottom: 50,
  },
  itemSelected: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  item_view: {
    padding: 10,
  },
  item_view_1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 5,
  },
  item_view_left: {
    paddingTop: 10,
  },
  originCode: {
    color: '#09304B80',
    fontFamily: GLOBAL.roman,
    fontWeight: '500',
    fontSize: 17,
  },
  originTime: {
    color: '#F8A301',
    fontFamily: GLOBAL.roman,
    fontWeight: '500',
    fontSize: 15,
  },
  originTerminal: {
    color: '#09304B80',
    fontFamily: GLOBAL.roman,
    fontWeight: '500',
    fontSize: 12,
  },
  flightDetails: {
    color: '#09304B80',
    fontFamily: GLOBAL.roman,
    fontSize: 14,
    textAlign: 'center',
  },
  cadImage: {
    width: 140,
    height: 20,
    marginVertical: 2,
    resizeMode: 'contain',
  },
  flightStop: {
    color: '#09304B80',
    fontFamily: GLOBAL.roman,
    fontSize: 14,
    textAlign: 'center',
  },
  item_view_right: {
    paddingTop: 10,
    alignItems: 'flex-end',
  },
  destinationCode: {
    color: '#09304B80',
    fontFamily: GLOBAL.roman,
    fontWeight: '500',
    fontSize: 17,
  },
  destinationTime: {
    color: '#F8A301',
    fontFamily: GLOBAL.roman,
    fontWeight: '500',
    fontSize: 15,
  },
  destinationTerminal: {
    color: '#09304B80',
    fontFamily: GLOBAL.roman,
    fontWeight: '500',
    fontSize: 12,
  },
  item_hz: {
    marginTop: 8,
    height: 1,
    backgroundColor: '#f1f1f1',
    width: '100%',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  button: {
    borderWidth: 1,
    borderColor: '#000000',
    margin: 5,
    padding: 5,
    width: '70%',
    backgroundColor: '#DDDDDD',
    borderRadius: 5,
  },
  textField: {
    borderWidth: 1,
    borderColor: '#AAAAAA',
    margin: 5,
    padding: 5,
    width: '70%',
  },
  spacer: {
    height: 10,
  },
  scene: {
    flex: 1,
  },

  title: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
  },
  error_text_1: {
    color: 'black',
    fontWeight: '700',
    fontSize: 18,
    alignSelf: 'center',
    marginTop: 20,
  },
  error_text_2: {
    color: '#4a4a4a',
    fontWeight: '500',
    fontSize: 14,
    alignSelf: 'center',
    marginTop: 5,
    marginHorizontal: 20,
    textAlign: 'center',
  },
  fc_view: {
    flexDirection: 'row',
    borderBottomColor: '#242E42',
    borderBottomWidth: 0.5,
  },
  fc_touch: {
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  fc_touch_active: {
    padding: 10,
    alignItems: 'center',
    backgroundColor: '#242E42',
  },
  fc_text: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 12,
    color: '#242E42',
  },
  fc_text_active: {
    fontFamily: 'Avenir',
    fontWeight: '400',
    fontSize: 14,
    color: 'white',
  },
  chk_mainView: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 10,
  },
  chk_view: {
    paddingHorizontal: 10,
  },
  chk_mainText: {
    fontFamily: GLOBAL.medium,
    fontWeight: '500',
    fontSize: 15,
    color: 'grey',
  },
  al_mainView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: 20,
  },
  al_view: {
    flexDirection: 'row',
    margin: 5,
    alignItems: 'center',
  },
  al_title: {
    color: '#09304B',
    fontFamily: GLOBAL.roman,
    fontSize: 18,
    fontWeight: '900',
    marginHorizontal: 15,
  },
  al_text: {
    fontFamily: GLOBAL.medium,
    fontWeight: '500',
    fontSize: 15,
    color: 'grey',
  },
  time_container: {
    marginTop: 10,
  },
  time_view: {
    flexDirection: 'row',
    marginBottom: 10,
    justifyContent: 'space-evenly',
  },
  time_touch: {
    flex: 0.4,
    paddingVertical: 10,
    borderRadius: 15,
    borderColor: '#7d787880',
    borderWidth: 1,
  },
  time_touch_active: {
    backgroundColor: '#242E42',
    borderWidth: 0,
  },
  time_text: {
    color: '#09304B',
    fontFamily: GLOBAL.medium,
    fontSize: 14,
    alignSelf: 'center',
  },
  time_text_active: {
    color: 'white',
  },
  textRoman: {
    color: '#09304B',
    fontFamily: GLOBAL.roman,
    fontSize: 14,
  },
  sort_container: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    marginVertical: 10,
  },
  sort_touch: {
    padding: 5,
    width: 70,
    alignItems: 'center',
    borderRadius: 8,
  },
  bbv_container: {
    width: '100%',
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    paddingHorizontal: 15,
    paddingVertical: 5,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    alignItems: 'center',
  },
  bbv_view: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
  },
  bbv_fareText: {
    color: '#09304B',
    fontFamily: GLOBAL.roman,
    fontSize: 16,
    fontWeight: '900',
  },
  bbv_touch: {
    backgroundColor: '#09304B',
    borderRadius: 8,
    marginLeft: 'auto',
  },
  bbv_continueText: {
    color: 'white',
    padding: 10,
  },
  ft_container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 5,
  },
  ft_image: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
  ft_view: {
    paddingLeft: 10,
  },
  ft_flightText: {
    color: '#09304B',
    fontFamily: GLOBAL.roman,
    fontSize: 18,
    fontWeight: '900',
  },
  ft_codeText: {
    color: '#09304B80',
    fontFamily: GLOBAL.roman,
    fontSize: 12,
    fontWeight: '400',
  },
  ft_fareText: {
    color: '#09304B',
    fontFamily: GLOBAL.roman,
    fontSize: 18,
    fontWeight: '900',
    marginLeft: 'auto',
  },
});

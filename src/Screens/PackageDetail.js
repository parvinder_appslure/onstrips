import React, {useEffect, useState} from 'react';
import {
  ImageBackground,
  Text,
  Image,
  View,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
  Dimensions,
} from 'react-native';
const {width, height} = Dimensions.get('window');
import {StatusBarDark, StatusBarLight} from '../utils/CustomStatusBar';
import {PackageDetailApi} from '../service/Api';
import {TabView, TabBar} from 'react-native-tab-view';
import HTMLView from 'react-native-htmlview';
import moment from 'moment';
import Loader from './Loader';

const PackageDetail = ({navigation, route}) => {
  const [state, setState] = useState({
    name: '',
    base_price: '',
    price: {},
    addons: {},
    tour_details: {},
    itinerary: [],
    city: [],
    pricebasedperson: [],
    pricebasedsingleroom: [],
    pricebasedextrabed: [],
    hotel_type: [],
    persons_type: [],
    status: false,
    item: route.params.item,
    traval_date: route.params.traval_date,
    isLoading: true,
  });
  const [tabState, setTabState] = useState({
    index: 0,
    routes: [],
  });
  const headerView = () => {
    const {item} = state;
    return (
      <ImageBackground source={{uri: item.image}} style={styles.bg_image}>
        <View style={styles.child}>
          <TouchableOpacity
            style={styles.backTouch}
            onPress={() => navigation.goBack()}>
            <Image
              source={require('../assets/back.png')}
              style={styles.backImage}
            />
          </TouchableOpacity>
          <View style={styles.titleView}>
            <Text style={styles.title}>{item.package_name}</Text>
            <Text style={styles.subTitle}>{`${item.no_of_days} ${
              item.no_of_days === '1' ? 'Day' : 'Days'
            } | ${item.no_of_city} ${
              item.no_of_city === '1' ? 'City' : 'Cities'
            }`}</Text>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.subTitle}>
                {`Valid: ${moment(item.from_date).format('DD-MM')} | ${moment(
                  item.to_date,
                ).format('DD-MM-YYYY')}`}
              </Text>
              <Text style={styles.baseFare}>{`\u20B9 ${item.base_price}`}</Text>
            </View>
          </View>
        </View>
      </ImageBackground>
    );
  };
  const focusView = () => (
    <View style={styles.tb_container}>
      <TabView
        navigationState={tabState}
        indicatorStyle={{backgroundColor: 'white'}}
        renderTabBar={props => (
          <TabBar
            style={tabStyles.style}
            labelStyle={tabStyles.labelStyle}
            tabStyle={{}}
            scrollEnabled={true}
            activeColor={'#09304B'}
            inactiveColor={'#4B4B4B99'}
            inactiveOpacity={0.5}
            {...props}
            indicatorStyle={{backgroundColor: '#FBD303', height: 3}}
          />
        )}
        renderScene={({route}) => {
          let value = state.tour_details.data[route.key];
          if (Array.isArray(value)) {
            value = value.join('');
            value = value.replace(/\\n/g, '');
          } else if (typeof value === 'string') {
            value = value.replace(/\\n/g, '');
          }
          return <HTMLView value={value} stylesheet={htmlStyles} />;
        }}
        onIndexChange={index => setTabState({...tabState, index})}
        initialLayout={{width: width}}
        scrollEnabled={true}
      />
    </View>
  );

  const submitButton = () => (
    <TouchableOpacity
      style={styles.bt_touch}
      onPress={() => navigation.navigate('PackageBooking', state)}>
      <Text style={styles.bt_text}>Book Now</Text>
    </TouchableOpacity>
  );

  useEffect(() => {
    (async () => {
      const response = await PackageDetailApi({user_id: '', id: state.item.id});
      const {
        status = false,
        name,
        base_price,
        price,
        addons,
        tour_details,
        itinerary,
        city,
        pricebasedperson,
        pricebasedsingleroom,
        pricebasedextrabed,
        hotel_type,
        persons_type,
      } = response;
      // consolejson(response);
      if (status) {
        tour_details.title.unshift('Itinerary');
        tour_details.data.unshift(itinerary);
        setState({
          ...state,
          status,
          name,
          base_price,
          price,
          addons,
          tour_details,
          itinerary,
          city,
          isLoading: false,
          pricebasedperson,
          pricebasedsingleroom,
          pricebasedextrabed,
          hotel_type,
          persons_type,
        });
        const routes = [];
        tour_details.title.map((title, index) => {
          routes.push({key: index, title});
        });
        setTabState({...tabState, routes});
      } else {
        setState({...state, isLoading: false});
      }
    })();
  }, []);
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <StatusBarDark />

      <ScrollView>
        {state.status && (
          <>
            {headerView()}
            {focusView()}
          </>
        )}
      </ScrollView>
      {state.status && submitButton()}
      {state.isLoading && <Loader />}
    </SafeAreaView>
  );
};

export default PackageDetail;

const styles = StyleSheet.create({
  bg_image: {
    width: width,
    height: 280,
  },
  backTouch: {
    alignSelf: 'flex-start',
    padding: 5,
  },
  backImage: {
    width: 12,
    height: 20,
  },
  title: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 19,
    color: '#FFFFFF',
  },
  baseFare: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#FFFFFF',
    marginLeft: 'auto',
  },
  subTitle: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 18,
    color: '#FFFFFF',
  },
  ratingVeiw: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  child: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    padding: 20,
    paddingTop: 40,
    justifyContent: 'space-between',
  },
  tb_container: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  tb_touch: {
    paddingVertical: 10,
    width: width / 3,
    alignItems: 'center',
    borderBottomColor: '#E0E0E0',
    borderBottomWidth: 2,
  },
  tb_touch_active: {
    borderBottomColor: '#F8A301',
  },
  tb_title: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#000521',
  },
  tb_title_active: {
    color: '#F8A301',
  },
  opt_container: {
    padding: 20,
    borderTopColor: '#E0E0E0',
    borderTopWidth: 1,
    marginVertical: 10,
  },
  opt_title: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 20,
    color: '#242E42',
    marginBottom: 20,
  },
  opt_text_1: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    color: '#242E42',
  },
  opt_text_2: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    color: '#8A8A8F',
  },
  bt_text: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#fff',
  },
  bt_touch: {
    backgroundColor: '#09304B',
    width: '60%',
    paddingVertical: 8,
    borderRadius: 15,
    bottom: 20,
    alignSelf: 'center',
    alignItems: 'center',
    position: 'absolute',
  },
  opt_container_2: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    margin: 10,
  },
  opt_view_1: {
    flexDirection: 'row',
    alignItems: 'center',
    width: (width - 40) / 3,
    marginVertical: 5,
  },
  opt_image_1: {
    height: 30,
    width: 30,
    marginRight: 10,
  },
});

const tabStyles = StyleSheet.create({
  style: {
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  labelStyle: {
    fontSize: 12,
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
  },
});

const htmlStyles = StyleSheet.create({
  p: {
    color: '#09304B', // make links coloured pink
    fontSize: 16,
    paddingHorizontal: 20,
  },
  h2: {
    paddingHorizontal: 20,
    fontSize: 30,
    color: '#09304B',
  },
});

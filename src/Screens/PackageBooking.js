import React, {useEffect, useState, useRef} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {TextField} from 'react-native-material-textfield';
import {StatusBarDark} from '../utils/CustomStatusBar';
import {SimpleHeader} from '../utils/Header';
import {useSelector} from 'react-redux';

import Paytm from '@philly25/react-native-paytm';
import {checkSumApi} from './paytm';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';

import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';

import {Dropdown} from 'react-native-material-dropdown';
import {Platform} from 'react-native';
import Loader from './Loader';
import {PackageBookingApi, CouponApi} from '../service/Api';
import {Image} from 'react-native';
const room = {
  single: 'Single Room',
  double: 'Double Room',
  twin: 'Twin Room',
  triple: 'Triple Room',
};

const PackageBooking = ({navigation, route}) => {
  const {user, couponList, isLogin} = useSelector(store => store);
  const [state, setState] = useState({
    remark: '',
    user_id: user?.id || '',
    traveller_name: user.name,
    traveller_email: user.email,
    traveller_mobile: user.mobile,
    traveller_gender: user.gender || 'male',
    traval_date: '',
    radio_props: [
      {label: 'Male', value: 'male'},
      {label: 'Female', value: 'female'},
    ],
    star_props: [],
    showPicker: false,
    datePicker: '',
    price_id: '',
    addons: [],
  });
  const [dataState, setDataState] = useState({
    personData: [],
    hotelData: [],
    personKey: -1,
    hotelKey: -1,
  });
  const [fareState, setFareState] = useState({
    amount: 0,
    tax: 0,
    discount: 0,
    total: 0,
  });
  const [packageState, setPackageState] = useState({
    packageList: [],
    packageSelect: -1,
  });
  const [couponState, setCouponState] = useState({
    id: 0,
    code: '',
    input: '',
    discount_type: '',
    discount_amount: '',
  });
  const bookingJsonRef = useRef();
  const [isLoading, setIsLoading] = useState(false);
  const titleView = title => (
    <View style={styles.paymentView}>
      <Text style={styles.paymentText}>{title}</Text>
    </View>
  );

  const fareField = (description, fare) => (
    <View style={styles.fare_titleView}>
      <View style={styles.fare_titleDes}>
        <Text style={styles.fare_subTitleText}>{description}</Text>
      </View>
      <View style={styles.fare_titleSize}>
        <Text style={styles.fare_subTitleText2}>{`\u20B9 ${fare}`}</Text>
      </View>
    </View>
  );
  const bookingHandler = async () => {
    if (!isLogin) {
      navigation.navigate('Login');
      return;
    }
    const {item, persons_type, hotel_type} = route.params;
    const {total, tax} = fareState;
    const {packageList, packageSelect} = packageState;
    const {personKey, hotelKey} = dataState;
    const {label = ''} = packageList[packageSelect];
    // const {id, mobile, email} = user;
    const {
      traval_date,
      remark,
      traveller_name,
      traveller_email,
      traveller_mobile,
      traveller_gender,
      user_id,
      mobile,
      email,
    } = state;

    if (traval_date === '') {
      alert('Please Select Travel Date');
      return;
    }

    const traveller_details = [
      {
        traveller_name,
        traveller_email,
        traveller_mobile,
        traveller_gender,
      },
    ];
    bookingJsonRef.current = {
      user_id,
      package_id: item.id,
      total_amount: total,
      coupan_code: couponState.code,
      coupan_id: couponState.id,
      transaction_mode: 'paytm',
      total_persons: persons_type[personKey],
      traval_date: traval_date,
      travel_to_date: '2021-04-10',
      hotel_select: hotel_type[hotelKey],
      tax_amount: tax,
      wallet_amount: '0',
      payment_status: '0',
      booking_note: remark,
      packageType: label,
      traveller_details,
    };
    consolejson(bookingJsonRef);
    const config = {
      orderId: `OID_${user_id}_${new Date().getTime()}`,
      mobile: mobile,
      email: email,
      customerId: user_id,
      taxAmt: total,
    };
    setIsLoading(true);
    checkSumApi(config)
      .then(details => Paytm.startPayment(details))
      .catch(error => {
        setIsLoading(false);
        console.log(error);
      });
  };
  const bookingRequest = async paymentdetail => {
    const body = {
      ...bookingJsonRef.current,
      ...paymentdetail,
    };
    const response = await PackageBookingApi(body);
    const {status = false} = response;
    setIsLoading(false);
    if (status) {
      navigation.navigate('PaymentSuccess');
    } else {
      alert('Something Went Wrong');
    }
  };
  const couponHandler = async () => {
    const {user_id} = state;
    const {input} = couponState;
    if (input) {
      const body = {
        coupan_code: input,
        user_id,
        module: 8,
      };
      setIsLoading(true);
      const apiResponse = await CouponApi(body);
      const {
        status = false,
        msg,
        discount_type,
        value,
        coupan_id,
      } = apiResponse;

      setIsLoading(false);
      if (status) {
        setCouponState({
          ...couponState,
          id: coupan_id,
          code: input,
          input: '',
          discount_type: discount_type,
          discount_amount: value,
        });
        alert('Coupon Applied');
      } else {
        setCouponState({
          ...couponState,
          id: 0,
          code: '',
          input: '',
          discount_type: '',
          discount_amount: 0,
        });
        alert('Invalid Coupon');
      }
    }
  };
  const onPayTmResponse = async resp => {
    const {STATUS, status, response} = resp;

    if (Platform.OS === 'ios') {
      if (status === 'Success') {
        const jsonResponse = JSON.parse(response);
        const {STATUS} = jsonResponse;

        if (STATUS && STATUS === 'TXN_SUCCESS') {
          // Payment succeed!
        }
      }
    } else {
      if (STATUS && STATUS === 'TXN_SUCCESS') {
        bookingRequest(resp);
      } else {
        alert('Something went wrong');
        setIsLoading(false);
      }
    }
  };

  useEffect(() => {
    Paytm.addListener(Paytm.Events.PAYTM_RESPONSE, onPayTmResponse);
    const {
      persons_type,
      hotel_type,
      item = '',
      traval_date = '',
    } = route.params;
    const personData = [];
    const hotelData = [];
    persons_type.map((item, index) =>
      personData.push({value: index, key: `${item}  Person`}),
    );
    hotel_type.map((item, index) =>
      hotelData.push({value: index, key: `${item} Star`}),
    );
    const diff = moment(item.to_date).diff(moment(traval_date));
    if (!isNaN(diff) && diff > 0) {
      setState({...state, traval_date});
    }
    setDataState({...dataState, personData, hotelData});
    return () => {
      Paytm.removeListener(Paytm.Events.PAYTM_RESPONSE, onPayTmResponse);
    };
  }, []);

  useEffect(() => {
    const {packageList, packageSelect} = packageState;
    if (packageSelect > -1) {
      const {amount} = packageList[packageSelect];
      const {id = '', discount_type = '', discount_amount = 0} = couponState;
      let discount = 0;
      if (id !== 0) {
        if (discount_type === 'percentage') {
          discount = Math.round(+amount * discount_amount) / 100;
        } else {
          discount = Math.round(+amount - +discount_amount);
        }
      }
      setFareState({
        ...fareState,
        amount: Math.abs(amount),
        tax: Math.round((+amount - discount) * 0.05),
        discount,
        total: Math.round(+amount - discount + (+amount - discount) * 0.05),
      });
    }
  }, [packageState, couponState]);

  useEffect(() => {
    const {hotelKey, personKey} = dataState;
    let packageList = [];
    if (hotelKey !== -1 && personKey !== -1) {
      const {
        pricebasedperson,
        pricebasedsingleroom,
        pricebasedextrabed,
        persons_type,
      } = route.params;
      switch (persons_type[personKey]) {
        case 1:
          packageList = [
            {
              label: `1 ${room.single}`,
              amount: pricebasedperson[personKey][hotelKey],
              value: 0,
            },
          ];
          break;
        case 2:
          packageList = [
            {
              label: `1 ${room.double}`,
              amount: pricebasedperson[personKey][hotelKey] * 2,
              value: 0,
            },
            {
              label: `1 ${room.twin}`,
              amount: pricebasedperson[personKey][hotelKey] * 2,
              value: 1,
            },
            {
              label: `2 ${room.single}`,
              amount:
                2 *
                (+pricebasedperson[personKey][hotelKey] +
                  +pricebasedsingleroom[hotelKey]),
              value: 2,
            },
          ];
          break;
        case 3:
          packageList = [
            {
              label: `1 ${room.triple}`,
              amount:
                2 * +pricebasedperson[personKey - 1][hotelKey] +
                +pricebasedextrabed[hotelKey],
              value: 0,
            },
            {
              label: `1 ${room.double} + 1 ${room.single}`,
              amount:
                3 * +pricebasedperson[personKey][hotelKey] +
                +pricebasedsingleroom[hotelKey],
              value: 1,
            },
            {
              label: `1 ${room.twin} + 1 ${room.single}`,
              amount:
                3 * +pricebasedperson[personKey][hotelKey] +
                +pricebasedsingleroom[hotelKey],
              value: 2,
            },
            {
              label: `3 ${room.single}`,
              amount:
                3 *
                (+pricebasedperson[personKey][hotelKey] +
                  +pricebasedsingleroom[hotelKey]),
              value: 3,
            },
          ];
          break;
        case 4:
          packageList = [
            {
              label: `2 ${room.double}`,
              amount: 4 * +pricebasedperson[personKey][hotelKey],
              value: 0,
            },
            {
              label: `2 ${room.twin}`,
              amount: 4 * +pricebasedperson[personKey][hotelKey],
              value: 1,
            },
            {
              label: `4 ${room.single}`,
              amount:
                4 *
                (+pricebasedperson[personKey][hotelKey] +
                  +pricebasedsingleroom[hotelKey]),
              value: 2,
            },
            {
              label: `1 ${room.double} + 2 ${room.single}`,
              amount:
                4 * +pricebasedperson[personKey][hotelKey] +
                2 * +pricebasedsingleroom[hotelKey],
              value: 3,
            },
            {
              label: `1 ${room.twin} + 2 ${room.single}`,
              amount:
                4 * +pricebasedperson[personKey][hotelKey] +
                2 * +pricebasedsingleroom[hotelKey],
              value: 4,
            },
            {
              label: `1 ${room.triple} + 1 ${room.single}`,
              amount:
                4 * +pricebasedperson[personKey - 1][hotelKey] +
                +pricebasedsingleroom[hotelKey] +
                +pricebasedextrabed[hotelKey],
              value: 5,
            },
          ];

          break;
        case 5:
          packageList = [
            {
              label: `1 ${room.double} + 1 ${room.triple}`,
              amount:
                4 * +pricebasedperson[personKey][hotelKey] +
                +pricebasedextrabed[hotelKey],
              value: 0,
            },
            {
              label: `1 ${room.twin} + 1 ${room.triple}`,
              amount:
                4 * +pricebasedperson[personKey][hotelKey] +
                +pricebasedextrabed[hotelKey],
              value: 1,
            },
            {
              label: `2 ${room.double} + 1 ${room.single}`,
              amount:
                5 * +pricebasedperson[personKey][hotelKey] +
                +pricebasedsingleroom[hotelKey],
              value: 2,
            },
            {
              label: `2 ${room.twin} + 1 ${room.single}`,
              amount:
                5 * +pricebasedperson[personKey][hotelKey] +
                +pricebasedsingleroom[hotelKey],
              value: 3,
            },
          ];
          break;
      }
      setPackageState({...packageState, packageList, packageSelect: 0});
    } else {
      setPackageState({...packageState, packageList, packageSelect: -1});
      setFareState({...fareState, amount: 0, tax: 0, total: 0, discount: 0});
    }
  }, [dataState.personKey, dataState.hotelKey]);

  useEffect(() => {
    if (isLogin) {
      const {id, name = '', email = '', mobile = '', gender = 'male'} = user;
      setState({
        ...state,
        traveller_name: name,
        traveller_email: email,
        traveller_mobile: mobile,
        traveller_gender: gender,
        user_id: id,
      });
    }
  }, [isLogin]);

  const fareCalculation = () => {
    const {amount, tax, total, discount} = fareState;
    const {packageList, packageSelect} = packageState;
    const {label} = packageList[packageSelect];
    return (
      <>
        {showSubTitle('Apply Coupon')}
        {showCoupon()}
        {couponState.code === '' && showCouponList()}
        {titleView('Fare Estimate')}
        <View style={styles.fare_view}>
          <View style={styles.fare_titleView}>
            <View style={styles.fare_titleDes}>
              <Text style={styles.fare_titleText}>Description</Text>
            </View>
            <View style={styles.fare_titleSize}>
              <Text style={styles.fare_titleText}>Charges</Text>
            </View>
          </View>
          {fareField(label, amount)}
          {fareField('Tax 5%', tax)}
          {discount !== 0 && fareField('Discount', discount)}
          <View style={styles.fare_titleView_2}>
            <Text style={styles.paymentText}>Total Fare</Text>
            <Text style={styles.paymentText}>{`\u20B9 ${total}`}</Text>
          </View>
          <TextField
            label={'Remark'}
            defaultValue={state.remark}
            keyboardType={'default'}
            maxLength={60}
            tintColor="grey"
            onChangeText={remark => setState({...state, remark})}
            baseColor={state.remark === '' ? 'grey' : 'black'}
            inputContainerStyle={styles.inputContainerStyle}
            affixTextStyle={{top: -2.2}}
            multiline
            rows={4}
          />
          <TouchableOpacity style={styles.btnTouch} onPress={bookingHandler}>
            <Text style={styles.btnTitle}>Book Now</Text>
          </TouchableOpacity>
        </View>
      </>
    );
  };
  const showSubTitle = title => (
    <View style={styles.paymentView}>
      <Text style={styles.paymentText}>{title}</Text>
    </View>
  );
  const showCouponList = () => {
    return (
      <>
        {couponList.map(item => (
          <TouchableOpacity
            style={styles.cpl_touch}
            onPress={() => setCouponState({...couponState, input: item.code})}>
            <View style={styles.cpl_view}>
              <Text style={styles.cpl_text_1}>Code</Text>
              <Text style={styles.cpl_text_2}>{item.code}</Text>
            </View>

            <View style={styles.cpl_view}>
              <Text style={styles.cpl_text_3}>{item.heading}</Text>
            </View>
          </TouchableOpacity>
        ))}
      </>
    );
  };

  const showCoupon = () => (
    <View style={styles.cp_view}>
      {couponState.code === '' && (
        <>
          <TextInput
            style={styles.cp_input}
            value={couponState.input}
            onChangeText={value =>
              setCouponState({...couponState, input: value})
            }
          />
          <TouchableOpacity style={styles.cp_touch} onPress={couponHandler}>
            <Text style={styles.cp_text}>APPLY</Text>
          </TouchableOpacity>
        </>
      )}
      {couponState.code !== '' && (
        <>
          <Text
            style={[
              styles.cp_input,
              {color: '#16b922'},
            ]}>{`${couponState.code} Applied`}</Text>
          <TouchableOpacity
            style={styles.cp_touch}
            onPress={() =>
              setCouponState({
                ...couponState,
                id: 0,
                code: '',
                input: '',
                discount_type: '',
                discount_amount: '',
              })
            }>
            <Image
              source={require('../assets/close.png')}
              style={styles.cp_closeImage}
            />
          </TouchableOpacity>
        </>
      )}
    </View>
  );
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <StatusBarDark />
      {isLoading && <Loader />}
      <ScrollView>
        {SimpleHeader('Package Booking', navigation.goBack)}
        <TextField
          label={'Name'}
          defaultValue={state.traveller_name}
          keyboardType={'default'}
          tintColor="grey"
          onChangeText={traveller_name => setState({...state, traveller_name})}
          baseColor={state.traveller_name === '' ? 'grey' : 'black'}
          inputContainerStyle={styles.inputContainerStyle}
        />
        <TextField
          label={'Email'}
          keyboardType={'email-address'}
          defaultValue={state.traveller_email}
          tintColor="grey"
          onChangeText={traveller_email =>
            setState({...state, traveller_email})
          }
          baseColor={state.traveller_email === '' ? 'grey' : 'black'}
          inputContainerStyle={styles.inputContainerStyle}
        />
        <TextField
          label={'Moble'}
          prefix={'+91'}
          defaultValue={state.traveller_mobile}
          keyboardType={'phone-pad'}
          maxLength={10}
          tintColor="grey"
          onChangeText={traveller_mobile =>
            setState({...state, traveller_mobile})
          }
          baseColor={state.traveller_mobile === '' ? 'grey' : 'black'}
          inputContainerStyle={styles.inputContainerStyle}
          affixTextStyle={{top: -2.2}}
        />

        <View style={styles.genderView}>
          <Text style={styles.genderText}>Gender</Text>
          <RadioForm formHorizontal={true} animation={true}>
            {state.radio_props.map((obj, i) => (
              <RadioButton labelHorizontal={true} key={i}>
                <RadioButtonInput
                  obj={obj}
                  index={i}
                  isSelected={state.traveller_gender === obj.value}
                  onPress={() =>
                    setState({...state, traveller_gender: obj.value})
                  }
                  borderWidth={1}
                  buttonInnerColor={'#F8A301'}
                  buttonOuterColor={
                    state.traveller_gender === obj.value ? '#F8A301' : '#242E42'
                  }
                  buttonSize={10}
                  buttonOuterSize={20}
                  buttonStyle={{}}
                  buttonWrapStyle={{}}
                />
                <RadioButtonLabel
                  obj={obj}
                  index={i}
                  labelHorizontal={true}
                  onPress={() =>
                    setState({...state, traveller_gender: obj.value})
                  }
                  labelStyle={{
                    paddingRight: 20,
                  }}
                  labelWrapStyle={{
                    fontSize: 18,
                    color: '#242E42',
                    fontFamily: 'Avenir-Medium',
                    fontWeight: '500',
                  }}
                />
              </RadioButton>
            ))}
          </RadioForm>
        </View>
        {state.showPicker && (
          <DateTimePicker
            value={state.datePicker || new Date()}
            minimumDate={new Date()}
            maximumDate={new Date(route.params.item.to_date)}
            mode={'date'}
            display="spinner"
            is24Hour={true}
            onChange={({type, nativeEvent}) => {
              if (type === 'set') {
                setState({
                  ...state,
                  showPicker: false,
                  datePicker: nativeEvent.timestamp,
                  traval_date: moment(nativeEvent.timestamp).format(
                    'YYYY-MM-DD',
                  ),
                });
              } else {
                setState({...state, showPicker: false});
              }
            }}
          />
        )}
        <TouchableOpacity
          onPress={() => setState({...state, showPicker: true, mode: 'date'})}>
          <TextField
            label={'Departure Date'}
            defaultValue={state.traval_date}
            editable={false}
            tintColor="grey"
            baseColor={state.traval_date === '' ? 'grey' : 'black'}
            inputContainerStyle={styles.inputContainerStyle}
          />
        </TouchableOpacity>

        <Dropdown
          rippleOpacity={0.54}
          dropdownPosition={3.2}
          containerStyle={styles.inputContainerStyle}
          itemTextStyle={styles.inputTextStyle}
          label="No. of Person"
          data={dataState.personData}
          onChangeText={(value, index) =>
            setDataState({...dataState, personKey: index})
          }
          labelExtractor={item => item.key}
          useNativeDriver={true}
        />
        <Dropdown
          rippleOpacity={0.54}
          dropdownPosition={3.2}
          containerStyle={styles.inputContainerStyle}
          itemTextStyle={styles.inputTextStyle}
          label="Hotel Type"
          data={dataState.hotelData}
          onChangeText={(value, index) =>
            setDataState({...dataState, hotelKey: index})
          }
          labelExtractor={item => item.key}
          useNativeDriver={true}
        />
        {dataState.personKey !== -1 && dataState.hotelKey !== -1 && (
          <View style={{marginHorizontal: 0}}>
            {titleView('Select Package Options')}
            <RadioForm
              formHorizontal={false}
              animation={true}
              style={{marginHorizontal: 15}}>
              {packageState.packageList.map((obj, i) => (
                <RadioButton
                  labelHorizontal={true}
                  key={i}
                  style={{marginHorizontal: 20, marginVertical: 10}}>
                  <RadioButtonInput
                    obj={obj}
                    index={i}
                    isSelected={packageState.packageSelect === obj.value}
                    onPress={() =>
                      setPackageState({
                        ...packageState,
                        packageSelect: obj.value,
                      })
                    }
                    borderWidth={1}
                    buttonInnerColor={'#F8A301'}
                    buttonOuterColor={
                      packageState.packageSelect === obj.value
                        ? '#F8A301'
                        : '#242E42'
                    }
                    buttonSize={10}
                    buttonOuterSize={20}
                    buttonStyle={{}}
                    buttonWrapStyle={{}}
                    labelExtractor
                  />
                  <RadioButtonLabel
                    obj={obj}
                    index={i}
                    labelHorizontal={true}
                    onPress={() => {
                      setPackageState({
                        ...packageState,
                        packageSelect: obj.value,
                      });
                    }}
                    labelStyle={
                      {
                        // paddingRight: 20,
                        // backgroundColor: 'red',
                      }
                    }
                    labelWrapStyle={{
                      fontSize: 18,
                      color: '#242E42',
                      fontFamily: 'Avenir-Medium',
                      fontWeight: '500',
                    }}
                  />
                </RadioButton>
              ))}
            </RadioForm>
            {packageState.packageSelect !== -1 && fareCalculation()}
          </View>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default PackageBooking;

const styles = StyleSheet.create({
  inputContainerStyle: {
    marginHorizontal: 30,
    marginVertical: 10,
    backgroundColor: 'white',
  },

  genderView: {
    marginVertical: 10,
    marginHorizontal: 30,
  },
  genderText: {
    color: 'black',
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    marginBottom: 20,
  },
  inputTextStyle: {
    color: 'black',
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
  },
  paymentView: {
    marginVertical: 15,
    paddingVertical: 12,
    paddingHorizontal: 20,
    backgroundColor: '#F7F7F7',
  },
  paymentText: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    color: '#0A1F44',
  },

  cp_view: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#E5E5E5',
    borderRadius: 8,
    margin: 20,
    alignItems: 'center',
  },
  cp_input: {
    flex: 1,
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 16,
    color: 'black',
    paddingHorizontal: 20,
  },
  cp_touch: {
    padding: 10,
  },
  cp_closeImage: {
    height: 15,
    width: 15,
    resizeMode: 'contain',
    marginRight: 15,
  },
  cp_text: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 14,
    color: '#09304B',
  },
  cpl_touch: {
    marginHorizontal: 30,
    marginVertical: 5,
    paddingHorizontal: 5,
  },
  cpl_view: {flexDirection: 'row'},
  cpl_text_1: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    color: '#242E42',
  },
  cpl_text_2: {
    marginLeft: 20,
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 18,
    color: '#F8A301',
  },
  cpl_text_3: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 12,
    color: '#767676',
  },

  fare_view: {},
  fare_titleView: {
    flexDirection: 'row',
    padding: 5,
    paddingHorizontal: 25,
  },
  fare_titleView_2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 5,
    marginVertical: 15,
    paddingVertical: 12,
    paddingHorizontal: 25,
    backgroundColor: '#F7F7F7',
  },
  fare_titleDes: {
    flex: 0.5,
  },
  fare_titleSize: {
    flex: 0.5,
    alignItems: 'flex-end',
  },
  fare_titleText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 16,
    color: '#0A1F44',
  },
  fare_subTitleText: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 15,
    color: '#282727',
  },
  fare_subTitleText2: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 15,
    color: '#EF4236',
  },
  btnTouch: {
    marginTop: 15,
    marginBottom: 25,
    backgroundColor: '#09304B',
    width: '80%',
    paddingVertical: 8,
    borderRadius: 15,
    alignSelf: 'center',
    alignItems: 'center',
  },
  btnTitle: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#fff',
  },
});

const modalStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#00000099',
    justifyContent: 'center',
  },
  view: {
    backgroundColor: 'white',
    marginHorizontal: 30,
    borderRadius: 20,
    paddingVertical: 20,
    height: '50%',
  },
  headerView: {
    marginBottom: 10,
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  closeTouch: {
    padding: 10,
  },
  closeImage: {
    height: 12,
    width: 12,
    resizeMode: 'contain',
  },
  meal_view: {
    flexDirection: 'row',
    marginBottom: 5,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
  },
  meal_view_1: {
    flex: 0.75,
    paddingRight: 5,
  },
  meal_view_2: {
    flex: 0.25,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  m2_view: {
    flexDirection: 'row',
    margin: 10,
    alignItems: 'center',
  },
  m2_qty: {
    marginRight: 10,
  },
  m2_disc: {
    flex: 0.7,
  },
  m2_price: {
    marginLeft: 'auto',
  },
  count_view: {
    borderRadius: 15,
    borderColor: '#09304B',
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 5,
  },
  countTouch: {
    paddingHorizontal: 5,
  },

  text_1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    color: '#ed5a6b',
  },
  count_view_1: {
    paddingHorizontal: 5,
    backgroundColor: '#f7ebec',
  },
  text_2: {
    color: '#09304B',
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
  },
  addText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    color: '#09304B',
  },
  addTouch: {
    borderRadius: 15,
    borderColor: '#09304B',
    borderWidth: 1,
    paddingHorizontal: 15,
    alignItems: 'center',
  },
  quantitiy: {},
  meal_text_count: {},
  meal_view_count: {},
});

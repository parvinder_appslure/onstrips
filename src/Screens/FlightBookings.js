import React, {useEffect, useState} from 'react';
import {ScrollView} from 'react-native';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  Image,
  AsyncStorage,
} from 'react-native';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import GLOBAL from './Global';
import Global from './Global';
import moment from 'moment';
import Paytm from '@philly25/react-native-paytm';
import {checkSumApi} from './paytm';
import airlineicon from './airlineicon';
import {GetFlightFareApi} from '../service/Api';
const FlightBookings = ({route, navigation}) => {
  const [state, setState] = useState({});
  const [count, setCount] = useState({
    adultcount: 0,
    childcount: 0,
    infantcount: 0,
    adult: [],
    child: [],
    infant: [],
  });

  const convertMinsToTime = mins => {
    let hours = Math.floor(mins / 60);
    let minutes = mins % 60;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    return `${hours}hrs:${minutes}mins`;
  };
  const headerView = () => (
    <View style={styles.header}>
      <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={styles.headerback}>
        <Image
          source={require('../assets/back2.png')}
          style={styles.backImage}
        />
      </TouchableOpacity>
      <View style={styles.headerTitle}>
        {/* <Text style={styles.flightText}> */}
        {/* {state.flight_from} - {state.flight_to} */}
        {/* </Text> */}
        <Text style={styles.flightDateText}>
          {`${moment(GLOBAL.flide.departure_date).format('DD-MMM')} ${
            GLOBAL.flide.totalCount
          } Travellers | ${GLOBAL.flide.classflight}`}
        </Text>
      </View>
    </View>
  );

  const flightTitle = ({AirlineName, AirlineCode, FlightNumber}) => {
    return (
      <View style={styles.ft_container}>
        <Image
          source={airlineicon[AirlineCode]}
          style={styles.ft_airlineImage}
        />
        <View style={styles.ft_view_1}>
          <Text style={styles.ft_text_1}>{AirlineName}</Text>
          <Text style={styles.ft_text_2}>
            {`${AirlineCode} - ${FlightNumber}`}
          </Text>
        </View>
      </View>
    );
  };
  const renderText = value => <Text style={styles.fm_text_9}>{value}</Text>;

  const flightMain = ({Segments, Fare}) => {
    console.log(JSON.stringify(Fare, null, 2));
    return (
      <>
        {Segments[0].map(segment => (
          <View style={styles.fm_container}>
            <View style={styles.fm_view_1}>
              <Text style={styles.fm_text_1}>
                {segment.Origin.Airport.AirportCode}
              </Text>
              <Text style={styles.fm_text_2}>
                {moment(segment.Origin.DepTime).format('h:mm:a')}
              </Text>
              <Text style={styles.fm_text_3}>
                {`Terminal ${segment.Origin.Airport.Terminal || '-'}`}
              </Text>
              <Text style={styles.fm_text_3}>
                {segment.Origin.Airport.AirportName}
              </Text>
            </View>
            <View style={styles.fm_view_2}>
              <Text style={styles.fm_text_4}>
                {convertMinsToTime(segment.Duration)}
              </Text>
              <Image
                source={require('../assets/cad.png')}
                style={styles.fm_image_1}
              />
            </View>
            <View style={styles.fm_view_3}>
              <Text style={styles.fm_text_5}>
                {segment.Destination.Airport.AirportCode}
              </Text>
              <Text style={styles.fm_text_6}>
                {moment(segment.Destination.ArrTime).format('h:mm:a')}
              </Text>
              <Text style={styles.fm_text_7}>
                {`Terminal ${segment.Destination.Airport.Terminal || '-'}`}
              </Text>
              <Text style={styles.fm_text_7}>
                {segment.Destination.Airport.AirportName}
              </Text>
            </View>
          </View>
        ))}
        <Text style={styles.fm_text_8}>Fare Type | Ecnomy Basic</Text>
        <View style={styles.fm_view_4}>
          <Text style={styles.fm_text_a1}>
            .Baggage -{Segments[0][0].Baggage}
          </Text>
          <Text style={styles.fm_text_a1}>
            .Cabin Baggage - {Segments[0][0].CabinBaggage}
          </Text>
          <Text style={styles.fm_text_a1}>.Base Fare -{Fare.BaseFare}</Text>
          <Text style={styles.fm_text_a1}>.Tax -{Fare.Tax}</Text>
        </View>
      </>
    );
  };

  const flightView = mflight => (
    <View style={styles.flightContainer}>
      {flightTitle(mflight.Segments[0][0].Airline)}
      {flightMain(mflight)}
    </View>
  );
  const fareView = ({
    BaseFare,
    Tax,
    YQTax,
    OtherCharges,
    Discount,
    PublishedFare,
  }) => {
    const title = (a, b) => {
      return (
        <View style={fareStyles.fieldContainer}>
          <Text style={fareStyles.text}>{a}</Text>
          <Text style={fareStyles.text}>{`\u20B9 ${b}`}</Text>
        </View>
      );
    };
    return (
      <View style={fareStyles.container}>
        {title('BaseFare', BaseFare)}
        {title('Tax', Tax)}
        {title('YQTax', YQTax)}
        {title('Other Charges', OtherCharges)}
        {title('Discount', Discount)}
        <View style={fareStyles.fieldContainer2} />
        {title('Total Price', PublishedFare)}
      </View>
    );
  };
  const discountView = () => (
    <View style={styles.dv_container}>
      <Text style={styles.td_text_1}>Discount Applied</Text>
      <View style={fareStyles.fieldContainer2} />
      <Text style={styles.td_text_2}>ENTER A COUPON CODE</Text>
    </View>
  );
  const travellerDetail = () => (
    <View style={styles.td_container}>
      <Text style={styles.td_text_1}>Traveller Details</Text>
      {+GLOBAL.flide.adultcount !== 0 && adultViewRender()}
      {+GLOBAL.flide.childcount !== 0 && childViewRender()}
      {+GLOBAL.flide.infantcount !== 0 && inflantViewRender()}
    </View>
  );

  const td_titleView = (subTitle, selectedCount) => (
    <View style={styles.tdtv_container}>
      <Text style={styles.tdtv_title}>{subTitle}</Text>
      <Text style={styles.tdtv_count}>{selectedCount}</Text>
    </View>
  );

  const td_radioView = key => (
    <RadioForm formHorizontal={false} animation={true}>
      {count[key].map((itemData, i) => (
        <RadioButton labelHorizontal={true} key={i}>
          <RadioButtonInput
            obj={{
              label: `${itemData.firstname} - ${itemData.lastname}`,
            }}
            index={i}
            isSelected={itemData.is_selected === 1}
            onPress={() => changeIndexd(i, key)}
            borderWidth={1}
            buttonInnerColor={'#09304B'}
            buttonOuterColor={'#09304B'}
            buttonSize={10}
            buttonOuterSize={20}
            buttonStyle={{}}
            buttonWrapStyle={radioStyle.buttonWrapStyle}
          />
          <RadioButtonLabel
            obj={{
              label: `${itemData.firstname} - ${itemData.lastname}`,
            }}
            index={i}
            labelHorizontal={true}
            onPress={() => changeIndexd(i, key)}
            labelStyle={radioStyle.labelStyle}
            labelWrapStyle={radioStyle.labelWrapStyle}
          />
        </RadioButton>
      ))}
    </RadioForm>
  );

  const td_buttonView = (title, id) => (
    <TouchableOpacity
      onPress={() =>
        navigation.navigate('AddTravel', {
          id: id,
          callback: () => handleuserData(false),
        })
      }>
      <View style={styles.tdb_container}>
        <Text style={styles.tdb_title}>{title}</Text>
      </View>
    </TouchableOpacity>
  );

  const adultViewRender = () => {
    return (
      <>
        {td_titleView(
          'Adult (12 yrs +)',
          `${count.adultcount}/${GLOBAL.flide.adultcount}`,
        )}
        {td_radioView('adult')}
        {td_buttonView('+ ADD ADULT TRAVELLER', '1')}
      </>
    );
  };
  const childViewRender = () => {
    return (
      <>
        {td_titleView(
          'CHILD (2 - 12 Yrs)',
          `${count.childcount}/${GLOBAL.flide.childcount}`,
        )}
        {td_radioView('child')}
        {td_buttonView('+ ADD CHILD TRAVELLER', '2')}
      </>
    );
  };
  const inflantViewRender = () => {
    return (
      <>
        {td_titleView(
          'INFANT (2 Yrs)',
          `${count.infantcount}/${GLOBAL.flide.infantcount}`,
        )}
        {td_radioView('infant')}
        {td_buttonView('+ ADD INFANT TRAVELLER', '3')}
      </>
    );
  };
  const bottomView = () => {
    return (
      <View style={bottomStyles.container}>
        <View>
          <View style={bottomStyles.view_1}>
            <Text style={bottomStyles.fareText}>{`\u20B9 ${state.fare}`}</Text>
            <Image
              source={require('../assets/info.png')}
              style={bottomStyles.image}
            />
          </View>
          <Text style={bottomStyles.fareText_2}>All inclusive price</Text>
        </View>
        <TouchableOpacity
          style={bottomStyles.payButton}
          onPress={payNowHandler}>
          <Text style={bottomStyles.payText}>Pay Now</Text>
        </TouchableOpacity>
      </View>
    );
  };

  const handleuserData = flag => {
    console.log('flag\n');
    const url = 'http://139.59.76.223/cab/webservices/get_members';
    const body = JSON.stringify({
      user_id: GLOBAL.user_id,
      flag: flag,
    });
    console.log(body);
    fetch(url, {
      method: 'POST',
      headers: {
        'x-api-key':
          '$2y$12$MOOt6dmiClUmITafZDyR2edjeJzx.UiXzG/ArWY8fl.zhNSi6FUfy',
        'Content-Type': 'application/json',
      },
      body: body,
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.status == true) {
          //   alert(JSON.stringify(responseJson))
          console.log('respons');
          console.log(Object.keys(responseJson));
          setCount({
            ...count,
            adult: responseJson.adult,
            child: responseJson.child,
            infant: responseJson.infant,
          });
        }
      })
      .catch(error => {
        console.error(error);
      });
  };
  const changeIndexd = (index, key) => {
    console.log(index + ' ' + key);
    let mList = [...count[key]];
    mList[index].is_selected = mList[index].is_selected === 1 ? 0 : 1;
    //adultcount,childcount,infantcount
    if (count[key + 'count'] === +GLOBAL.flide[key + 'count']) {
      if (mList[index].is_selected) {
        alert(`You can select maximum ${GLOBAL.flide[key + 'count']} ${key}`);
        return;
      }
    }
    const url = 'http://139.59.76.223/cab/webservices/toggle_select_member';
    const body = JSON.stringify({
      id: mList[index].id,
      is_selected: mList[index].is_selected,
    });
    console.log(body);
    fetch(url, {
      method: 'POST',
      headers: {
        'x-api-key':
          '$2y$12$MOOt6dmiClUmITafZDyR2edjeJzx.UiXzG/ArWY8fl.zhNSi6FUfy',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        id: mList[index].id,
        is_selected: mList[index].is_selected,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.status == true) {
          let mCount = 0;
          mList.forEach(item => {
            if (item.is_selected === 1) {
              mCount++;
            }
          });
          setCount({
            ...count,
            [key]: mList,
            [key + 'count']: mCount,
          });
        }
      })
      .catch(error => {
        console.error(error);
      });
  };

  const _log = d => console.log(JSON.stringify(d, null, 2));
  const payNowHandler = async () => {
    if (
      count.adultcount !== Global.flide.adultcount ||
      count.childcount !== Global.flide.childcount ||
      count.infantcount !== Global.flide.infantcount
    ) {
      alert('Please complete Traveller Details');
      return;
    }
    const {fare, goFlight, backFlight} = state;
    // const taxAmt = parseInt(Fare.PublishedFare) - parseInt(this.state.discount);
    const taxAmt = fare;
    // console.log('taxAmt =>', taxAmt);
    const userDetails = JSON.parse(await AsyncStorage.getItem('userDetails'));
    if (Object.keys(userDetails).length === 0) {
      alert('Something went wrong please start app again');
      return;
    }
    const config = {
      orderId: `OID_${userDetails.id}_${new Date().getTime()}`,
      mobile: userDetails.mobile,
      email: userDetails.email,
      customerId: userDetails.id,
      taxAmt: taxAmt,
    };
    // _log(config);
    // if (!goFlight.IsLCC) {
    //   console.log('goflight hold');
    // }
    // if (!backFlight.IsLCC) {
    //   console.log('backflight hold');
    // }
    checkSumApi(config)
      .then(details => Paytm.startPayment(details))
      .catch(error => console.log(error));
    // console.log(JSON.stringify(userDetails, null, 2));
    // const url = 'http://139.59.76.223/cab/webservices/round_ticket_for_lcc';
    // var body = JSON.stringify({
    //   ResultIndex: `${state.goFlight.ResultIndex},${state.backFlight.ResultIndex}`,
    //   TokenId: GLOBAL.flighttoken,
    //   TraceId: GLOBAL.TraceId,
    //   user_id: GLOBAL.user_id,
    // });
    // console.log(JSON.stringify(body, null, 2));

    // fetch(url, {
    //   method: 'POST',
    //   headers: {
    //     'x-api-key':
    //       '$2y$12$MOOt6dmiClUmITafZDyR2edjeJzx.UiXzG/ArWY8fl.zhNSi6FUfy',
    //     'Content-Type': 'application/json',
    //   },
    //   body: body,
    // })
    //   .then((response) => response.json())
    //   .then((responseJson) => {
    //     console.log(JSON.stringify(responseJson, null, 2));
    //     alert(responseJson.message);
    //     if (responseJson.status == true) {
    //       alert('Booking Successfull');
    //     } else {
    //       alert('Something went wrong');
    //     }
    //   })
    //   .catch((error) => {
    //     console.error(error);
    //   });
  };

  const onPayTmResponse = resp => {
    const {STATUS, status, response} = resp;

    if (Platform.OS === 'ios') {
      if (status === 'Success') {
        const jsonResponse = JSON.parse(response);
        const {STATUS} = jsonResponse;

        if (STATUS && STATUS === 'TXN_SUCCESS') {
          // Payment succeed!
          // const body = {
          //   ResultIndex: this.props.route.params.ResultIndex,
          //   TokenId: Global.flighttoken,
          //   TraceId: Global.TraceId,
          //   user_id: Global.user_id,
          // };
          // console.log(body);
        }
      }
    } else {
      if (STATUS && STATUS === 'TXN_SUCCESS') {
        // Payment succeed!
        const body = {
          ResultIndex: this.props.route.params.ResultIndex,
          TokenId: Global.flighttoken,
          TraceId: Global.TraceId,
          user_id: Global.user_id,
        };
        console.log(body);
      }
    }
  };
  const bookingHandler = () => {
    const body = {
      ResultIndex: `${state.goFlight.ResultIndex},${state.backFlight.ResultIndex}`,
      TokenId: GLOBAL.flighttoken,
      TraceId: GLOBAL.TraceId,
      user_id: GLOBAL.user_id,
    };
    return new Promise((resolve, reject) => {
      fetch(Global.BASE_URL + 'ticket_for_lcc', {
        method: 'POST',
        headers: {
          'x-api-key':
            '$2y$12$MOOt6dmiClUmITafZDyR2edjeJzx.UiXzG/ArWY8fl.zhNSi6FUfy',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
      })
        .then(response => response.json())
        .then(responseJson => {
          this.hideLoading();
          console.log(responseJson);
          if (responseJson.status == true) {
            // alert('Booking Successfull');
            resolve({status: true, data: responseJson});
          } else {
            reject({status: false, data: responseJson});
            // alert('Something went wrong');
          }
        })
        .catch(error => {
          reject({status: false, data: error});
          console.error(error);
        });
    });
  };

  const fetchFlightDetails = async () => {
    const {
      TokenId,
      TraceId,
      ResultIndex,
      adultcount = 0,
      infantcount = 0,
      childcount = 0,
    } = route.params;
    console.log({
      TokenId,
      TraceId,
      ResultIndex,
    });
    const TravellersCount = adultcount + infantcount + childcount;
    // const { status = false, Results = {} }
    const res = await GetFlightFareApi({
      TokenId,
      TraceId,
      ResultIndex,
    });
    console.log(JSON.stringify(res, null, 2));
    console.log('------------------------asdfsdf');
    return;
    // if (status) {
    //   if (Object.keys(Results).length) {
    //     const {Fare, FareBreakdown, Segments} = Results;
    //     (Fare.OtherCharges += 200),
    //       (Fare.PublishedFare += 200),
    //       setState({
    //         ...state,
    //         Fare,
    //         FareBreakdown,
    //         Segments,
    //         TravellersCount,
    //         initialize: false,
    //       });
    //     setLoadingState(false);
    //   }
    // }
  };

  useEffect(() => {
    fetchFlightDetails();
    handleuserData(true);
    Paytm.addListener(Paytm.Events.PAYTM_RESPONSE, onPayTmResponse);
    return () => {
      Paytm.removeListener(Paytm.Events.PAYTM_RESPONSE, onPayTmResponse);
    };
  }, []);
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        barStyle="dark-content"
        translucent={true}
        backgroundColor={'transparent'}
      />
      {headerView()}
      <ScrollView>
        {/* {flightView(state.goFlight)}
        {fareView(state.goFlight.Fare)}
        {flightView(state.backFlight)}
        {fareView(state.backFlight.Fare)}
        {discountView()}
        {travellerDetail()}
        {bottomView()} */}
      </ScrollView>
    </SafeAreaView>
  );
};

FlightBookings['navigationOptions'] = screenProps => ({
  header: false,
});
export default FlightBookings;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    flexDirection: 'row',
    backgroundColor: '#FBD303',
    paddingTop: 25,
    paddingBottom: 10,
    paddingHorizontal: 10,
    alignItems: 'center',
  },
  headerback: {
    padding: 5,
    paddingRight: 10,
  },
  backImage: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
  flightContainer: {
    backgroundColor: 'white',
    color: 'white',
    flexDirection: 'column',
    borderColor: '#f7f7f7',
    borderRadius: 6,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.6,
    shadowRadius: 2,
    elevation: 5,
    marginHorizontal: 20,
    marginTop: 20,
    paddingHorizontal: 10,
  },
  ft_container: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    borderBottomColor: '#E0E0E0',
    borderBottomWidth: 0.5,
  },
  ft_airlineImage: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
  ft_view_1: {
    marginHorizontal: 10,
  },
  ft_text_1: {
    color: '#09304B',
    fontFamily: GLOBAL.roman,
    fontSize: 14,
    fontWeight: '900',
  },
  ft_text_2: {
    color: '#09304B80',
    fontFamily: GLOBAL.roman,
    fontSize: 12,
    fontWeight: '400',
  },
  fm_container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 5,
    borderBottomColor: '#E0E0E0',
    borderBottomWidth: 0.5,
    paddingBottom: 10,
  },
  fm_view_1: {
    paddingTop: 10,
    width: 80,
  },
  fm_text_1: {
    color: '#09304B80',
    fontFamily: GLOBAL.roman,
    fontWeight: '500',
    fontSize: 17,
  },
  fm_text_2: {
    color: '#F8A301',
    fontFamily: GLOBAL.roman,
    fontWeight: '500',
    fontSize: 15,
  },
  fm_text_3: {
    color: '#09304B80',
    fontFamily: GLOBAL.roman,
    fontWeight: '500',
    fontSize: 12,
  },
  fm_view_2: {
    justifyContent: 'center',
  },
  fm_text_4: {
    color: '#09304B80',
    fontFamily: GLOBAL.roman,
    fontSize: 14,
    textAlign: 'center',
  },
  fm_image_1: {
    width: 140,
    height: 20,
    marginVertical: 2,
    resizeMode: 'contain',
  },
  fm_view_3: {
    paddingTop: 10,
    width: 80,
    alignItems: 'flex-end',
  },
  fm_text_5: {
    color: '#09304B80',
    fontFamily: GLOBAL.roman,
    fontWeight: '500',
    fontSize: 17,
  },
  fm_text_6: {
    color: '#F8A301',
    fontFamily: GLOBAL.roman,
    fontWeight: '500',
    fontSize: 15,
  },
  fm_text_7: {
    color: '#09304B80',
    fontFamily: GLOBAL.roman,
    fontWeight: '500',
    fontSize: 12,
  },
  fm_text_8: {
    color: '#09304B',
    fontFamily: GLOBAL.heavy,
    fontSize: 14,
    fontWeight: '500',
    padding: 10,
  },
  fm_text_9: {
    color: '#09304B80',
    fontFamily: GLOBAL.roman,
    fontSize: 14,
    textAlign: 'center',
  },
  fm_view_4: {
    paddingHorizontal: 10,
    paddingBottom: 10,
  },
  fm_text_a1: {
    color: '#8396A4',
    fontFamily: GLOBAL.heavy,
    fontSize: 12,
  },
  dv_container: {
    margin: 20,
    marginBottom: 0,
    padding: 10,
    backgroundColor: 'white',
    borderColor: '#f7f7f7',
    borderRadius: 6,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.6,
    shadowRadius: 2,
    elevation: 5,
  },
  td_container: {
    backgroundColor: 'white',
    borderColor: '#f7f7f7',
    borderRadius: 6,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.6,
    shadowRadius: 2,
    elevation: 5,
    margin: 20,
    padding: 10,
  },
  td_text_1: {
    fontFamily: GLOBAL.roman,
    fontSize: 18,
    fontWeight: '500',
    color: '#09304B',
  },
  td_text_2: {
    fontFamily: GLOBAL.roman,
    fontWeight: '500',
    fontSize: 16,
    color: '#F8A301',
    marginTop: 5,
    marginHorizontal: 20,
  },
  tdtv_container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
  },
  tdtv_title: {
    fontFamily: GLOBAL.heavy,
    fontSize: 15,
    color: '#09304B',
  },
  tdtv_count: {
    fontFamily: GLOBAL.heavy,
    fontSize: 15,
    color: '#09304B',
  },
  tdb_container: {
    marginVertical: 10,
    alignSelf: 'center',
  },
  tdb_title: {
    fontFamily: GLOBAL.heavy,
    fontSize: 14,
    color: '#F8A301',
    textAlign: 'center',
  },
});

const radioStyle = StyleSheet.create({
  buttonWrapStyle: {
    margin: 10,
  },
  labelStyle: {
    fontSize: 14,
    color: '#09304B',
    fontFamily: GLOBAL.heavy,
    fontWeight: '500',
  },
  labelWrapStyle: {
    marginHorizontal: 0,
  },
});

const fareStyles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderColor: '#f7f7f7',
    borderRadius: 6,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.6,
    shadowRadius: 2,
    elevation: 5,
    margin: 20,
    marginBottom: 0,
    padding: 10,
  },
  fieldContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  fieldContainer2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderBottomColor: '#efefe4',
    borderBottomWidth: 1,
  },
  text: {
    fontFamily: GLOBAL.roman,
    fontSize: 15,
    color: '#09304B',
  },
});

const bottomStyles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#FBD303',
    padding: 20,
  },
  view_1: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  image: {
    height: 18,
    width: 18,
    resizeMode: 'contain',
    marginHorizontal: 10,
  },
  fareText: {
    fontFamily: GLOBAL.heavy,
    fontSize: 25,
    color: '#09304B',
  },
  fareText_2: {
    fontFamily: GLOBAL.roman,
    fontSize: 14,
    color: '#09304B',
  },
  payButton: {
    backgroundColor: '#09304B',
    borderRadius: 25,
    alignSelf: 'center',
  },
  payText: {
    fontFamily: Global.heavy,
    fontWeight: '900',
    fontSize: 19,
    color: '#FFFFFF',
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
});

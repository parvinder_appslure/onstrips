import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Dimensions,
  StatusBar,
  SafeAreaView,
  FlatList,
} from 'react-native';
import SearchBar from 'react-native-search-bar';
import {useDebounce} from 'use-debounce';

import {useStore} from 'react-redux';
import {StatusBarDark} from '../utils/CustomStatusBar';
const FromAirport = ({navigation, route}) => {
  const {airportCodes} = useStore().getState();
  const [state, setState] = useState({
    search: '',
    data: [...airportCodes],
    filter: [...airportCodes],
    key: route.params.key,
    updateAirportCode: route.updateAirportCode,
  });

  const [value] = useDebounce(state.search, 200);

  useEffect(() => {
    const filter = state.data.filter(item => {
      let key = item.code + item.map_address;
      key = key.toLocaleLowerCase();
      return key.includes(value.toLocaleLowerCase());
    });
    setState({...state, filter});
  }, [value]);

  return (
    <SafeAreaView style={styles.container}>
      <StatusBarDark />
      <View style={styles.searchContainer}>
        <SearchBar
          placeholder="Enter Airport Name"
          value={state.search}
          onChangeText={text => setState({...state, search: text})}
          textColor={'#242E42'}
          showsCancelButton={true}
        />
      </View>

      {state.search !== '' && (
        <FlatList
          style={{marginTop: 10}}
          data={state.filter}
          numColumns={1}
          horizontal={false}
          extraData={value}
          keyExtractor={item => item.id.toString()}
          renderItem={({item}) => {
            const {id, name, map_address, code} = item;
            return (
              <TouchableOpacity
                key={`key_${id}`}
                style={{paddingHorizontal: 20}}
                onPress={() => {
                  route.params.updateAirportCode({
                    [state.key === 0 ? 'airport' : 'toLocation']: code,
                  });
                  navigation.goBack();
                }}>
                <View style={styles.flatListView}>
                  <View style={{flex: 0.85}}>
                    <Text style={styles.flatListTextName}>{name}</Text>
                    <Text style={styles.flatListTextAddress}>
                      {map_address}
                    </Text>
                  </View>
                  <View style={{flex: 0.15}}>
                    <Text style={styles.flatListTextCode}>{code}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            );
          }}
        />
      )}
    </SafeAreaView>
  );
};

export default FromAirport;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  searchContainer: {
    marginTop: 50,
    marginBottom: 10,
    width: '90%',
    alignSelf: 'center',
  },
  flatListView: {
    borderBottomWidth: 1,
    borderBottomColor: '#DADADA',
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: 12,
    paddingHorizontal: 5,
  },
  flatListTextName: {
    color: '#242E42',
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '500',
  },
  flatListTextCode: {
    color: '#242E42',
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  flatListTextAddress: {
    color: '#242E42',
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
  },
});

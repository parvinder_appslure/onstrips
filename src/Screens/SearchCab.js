import React, {useEffect} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
  Dimensions,
  StatusBar,
  SafeAreaView,
  FlatList,
} from 'react-native';
const {width, height} = Dimensions.get('window');
import {StatusBarDark} from '../utils/CustomStatusBar';
import {SimpleHeader} from '../utils/Header';

const SearchCab = ({navigation, route}) => {
  const itemSelectHandler = (item) => {
    const {directionDetails} = route.params;
    navigation.navigate('CabDetail', {
      item,
      directionDetails,
    });
  };
  const totalFareCalculate = (item) => {
    const {total_price_discount, total_price} = item;
    return +total_price_discount === 0 ? total_price : total_price_discount;
  };
  const discountRateCalculate = (item) => {
    const {price_per_km_discount, price_per_km} = item;
    const flag = +price_per_km_discount === 0;
    const finalPrice = Math.round(flag ? price_per_km : price_per_km_discount);
    return (
      <Text style={styles.fl_text_price_3}>
        {`\u20B9`}
        {!flag && (
          <Text style={styles.fl_text_price_2}>{`  ${Math.round(
            price_per_km,
          )}`}</Text>
        )}
        {` ${finalPrice}/Km`}
      </Text>
    );
  };

  const FlatListViewLocal = (flag) => (
    <FlatList
      contentContainerStyle={styles.fl_contentContainerStyle}
      data={route.params.price_array}
      horizontal={false}
      renderItem={({item}) => (
        <TouchableOpacity
          key={`key_${item.id}`}
          onPress={() => itemSelectHandler(item)}
          style={styles.fl_container}>
          <View style={styles.fl_view_1}>
            <Image style={styles.fl_cab_image} source={{uri: item.image}} />
            {!flag && (
              <View style={{alignItems: 'flex-end'}}>
                {+item.total_price_discount !== 0 && (
                  <Text style={styles.fl_text_price_2}>
                    {`\u20B9 ${Math.round(item.total_price)}`}
                  </Text>
                )}
                <Text style={styles.fl_text_price}>
                  {`\u20B9 ${Math.round(totalFareCalculate(item))}`}
                </Text>
              </View>
            )}
          </View>
          <View style={styles.fl_view_2}>
            <Text style={styles.fl_text_cat}>{item.cab_category}</Text>
            {!flag && discountRateCalculate(item)}
          </View>
          <View style={styles.fl_view_2}>
            <Text style={styles.fl_text_feature}>{item.feature}</Text>
            <Text style={styles.fl_text_feature}>
              excl. of all Toll and Taxes
            </Text>
          </View>
          {flag &&
            item.prices_array_local.map((obj) => (
              <View style={styles.fl_view_2}>
                <Text style={styles.fl_text_feature}>{obj.title}</Text>
                <Text
                  style={styles.fl_text_feature}>{`\u20B9 ${obj.price}`}</Text>
              </View>
            ))}
        </TouchableOpacity>
      )}
      numColumns={1}
    />
  );

  return (
    <SafeAreaView style={styles.container}>
      <StatusBarDark />

      {SimpleHeader(route.params.headerTitle || 'Cabs', navigation.goBack)}
      {FlatListViewLocal(route.params.directionDetails.condition === 'local')}
    </SafeAreaView>
  );
};
export default SearchCab;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  fl_contentContainerStyle: {
    flex: 1,
    paddingHorizontal: 16,
  },
  fl_container: {
    marginTop: 16,
    paddingVertical: 10,
    backgroundColor: 'white',
    borderColor: '#f7f7f7',
    borderRadius: 6,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.6,
    shadowRadius: 2,
    elevation: 5,
  },
  fl_view_1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
  },
  fl_cab_image: {
    width: 100,
    height: 70,
    resizeMode: 'contain',
  },
  fl_view_2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    alignItems: 'center',
    borderLeftColor: '#FBD303',
    borderLeftWidth: 2,
  },
  fl_text_cat: {
    color: '#242E42',
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: '900',
  },
  fl_text_price: {
    color: '#242E42',
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
  },
  fl_text_price_2: {
    color: '#767676',
    fontFamily: 'Avenir-Roman',
    fontSize: 16,
    fontWeight: '500',
    textDecorationLine: 'line-through',
  },
  fl_text_price_3: {
    color: '#767676',
    fontFamily: 'Avenir-Roman',
    fontSize: 16,
    fontWeight: '500',
  },
  fl_text_feature: {
    color: '#767676',
    fontFamily: 'Avenir-Roman',
    fontSize: 12,
    fontWeight: '500',
  },
});

// comment for future use
// const FlatListView = () => (
//   <FlatList
//     contentContainerStyle={styles.fl_contentContainerStyle}
//     data={route.params.price_array}
//     horizontal={false}
//     renderItem={({item}) => (
//       <TouchableOpacity
//         key={`key_${item.id}`}
//         onPress={() => itemSelectHandler(item)}
//         style={styles.fl_container}>
//         <View style={styles.fl_view_1}>
//           <Image style={styles.fl_cab_image} source={{uri: item.image}} />
//           <View style={{alignItems: 'flex-end'}}>
//             {+item.total_price_discount !== 0 && (
//               <Text style={styles.fl_text_price_2}>
//                 {`\u20B9 ${Math.round(item.total_price)}`}
//               </Text>
//             )}
//             <Text style={styles.fl_text_price}>
//               {`\u20B9 ${Math.round(totalFareCalculate(item))}`}
//             </Text>
//           </View>
//         </View>
//         <View style={styles.fl_view_2}>
//           <Text style={styles.fl_text_cat}>{item.cab_category}</Text>
//           {discountRateCalculate(item)}
//         </View>
//         <View style={styles.fl_view_2}>
//           <Text style={styles.fl_text_feature}>{item.feature}</Text>
//           <Text style={styles.fl_text_feature}>
//             excl. of all Toll and Taxes
//           </Text>
//         </View>
//       </TouchableOpacity>
//     )}
//     numColumns={1}
//   />
// );

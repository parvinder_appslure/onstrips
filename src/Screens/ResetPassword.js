import React, {useEffect, useState} from 'react';
import {
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {ResetPasswordApi} from '../service/Api';
import {StatusBarDark} from '../utils/CustomStatusBar';

import Loader from './Loader';

const ResetPassword = ({navigation, route}) => {
  const [state, setState] = useState({
    password: '',
    confirmPassword: '',
  });
  const [loading, setLoading] = useState(false);

  const resetHandler = async () => {
    const {mobile} = route.params;
    const {password, confirmPassword} = state;
    if (password.length < 5) {
      alert('Password length must be greater than 4 characters.');
      return;
    }
    if (password !== confirmPassword) {
      alert('Password and Confirm password do not match');
      return;
    }
    setLoading(true);
    const {status = false} = await ResetPasswordApi({mobile, password});
    setLoading(false);
    if (status) {
      alert('Your password has been change Successfully');
      navigation.pop(3);
    } else {
      alert('Something went wrong');
    }
  };
  return (
    <SafeAreaView style={styles.container}>
      <StatusBarDark />
      {loading && <Loader />}
      <TouchableOpacity
        style={styles.touchBack}
        onPress={() => navigation.goBack()}>
        <Image
          source={require('../assets/backicon.png')}
          style={styles.imageBack}
        />
      </TouchableOpacity>
      <Text style={styles.textTitle_1}>Reset your Password</Text>
      <Text style={styles.textTitle_2}>
        {`Enter your new password for ${route.params.mobile}`}
      </Text>
      <TextInput
        style={styles.textInput}
        onChangeText={password => setState({...state, password})}
        placeholder="Password"
        keyboardType={'default'}
        secureTextEntry={true}
        value={state.password}
        placeholderTextColor={'#09304B'}
      />
      <TextInput
        style={styles.textInput}
        onChangeText={confirmPassword => setState({...state, confirmPassword})}
        placeholder="Confirm Password"
        keyboardType={'default'}
        secureTextEntry={true}
        value={state.confirmPassword}
        placeholderTextColor={'#09304B'}
      />

      <TouchableOpacity onPress={resetHandler} style={styles.touchNext}>
        <Text style={styles.touchText}>Reset</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FBD303',
  },
  touchBack: {
    marginVertical: 40,
    marginLeft: 20,
    padding: 10,
    alignSelf: 'flex-start',
  },
  imageBack: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
  textTitle_1: {
    color: '#09304B',
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 34,
    marginHorizontal: 34,
  },
  textTitle_2: {
    color: '#242E42',
    fontFamily: 'Avenir-Roman',
    fontWeight: '400',
    fontSize: 14,
    marginVertical: 20,
    marginBottom: 40,
    marginHorizontal: 34,
  },
  textInput: {
    paddingHorizontal: 20,
    marginHorizontal: 34,
    marginBottom: 20,
    // marginTop: '20%',
    color: '#242E42',
    fontSize: 17,
    fontFamily: 'Aviner-Heavy',
    fontWeight: '900',
    backgroundColor: '#F1F2F6',
    borderRadius: 8,
    borderColor: '#EAECEF',
    borderWidth: 1,
  },

  touchNext: {
    backgroundColor: '#09304B',
    width: '60%',
    alignSelf: 'center',
    borderRadius: 6,
    padding: 8,
    marginTop: 20,
  },
  touchText: {
    fontFamily: 'Aviner-Heavy',
    fontSize: 17,
    fontWeight: '900',
    color: '#fff',
    textAlign: 'center',
  },
});

export default ResetPassword;

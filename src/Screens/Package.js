import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  FlatList,
} from 'react-native';
const {width, height} = Dimensions.get('window');
import {SimpleHeader} from '../utils/Header';

import moment from 'moment';
import Loader from './Loader';
const Package = ({navigation, route}) => {
  const [state, setstate] = useState({
    detail: '',
    eventLists: ['1', '2'],
    selectedHours: 0,
    isShown: false,
    selectedMinutes: 0,
    packages_details: route.params.packages_details,
    traval_date: route.params.traval_date,
    isLoading: false,
  });

  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" />
      {SimpleHeader('Package', navigation.goBack)}
      {state.isLoading && <Loader />}
      <FlatList
        data={state.packages_details}
        horizontal={false}
        numColumns={2}
        contentContainerStyle={{
          paddingVertical: 20,
          paddingHorizontal: 5,
        }}
        renderItem={({item}) => (
          <TouchableOpacity
            key={`id_${item.id}`}
            style={styles.pk_container}
            onPress={() =>
              navigation.navigate('PackageDetail', {
                item,
                traval_date: state.traval_date,
              })
            }>
            <Image style={styles.pk_image} source={{uri: item.image}} />
            <View style={styles.pk_priceView}>
              <Text style={styles.pk_priceText}>₹ {item.base_price}</Text>
            </View>
            <Text style={styles.pk_name}>{item.package_name}</Text>
            <Text style={styles.pk_place}>{`${item.no_of_days} ${
              item.no_of_days === '1' ? 'Day' : 'Days'
            } | ${item.no_of_city} ${
              item.no_of_city === '1' ? 'City' : 'Cities'
            }`}</Text>
            <Text style={styles.pk_place}>{`Valid: ${moment(
              item.from_date,
            ).format('DD-MM')} | ${moment(item.to_date).format(
              'DD-MM-YYYY',
            )}`}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  button: {
    borderWidth: 1,
    borderColor: '#000000',
    margin: 5,
    padding: 5,
    width: '70%',
    backgroundColor: '#DDDDDD',
    borderRadius: 5,
  },
  textField: {
    borderWidth: 1,
    borderColor: '#AAAAAA',
    margin: 5,
    padding: 5,
    width: '70%',
  },
  spacer: {
    height: 10,
  },

  title: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
  pk_container: {
    backgroundColor: 'white',
    borderColor: '#f7f7f7',
    width: width / 2 - 15,
    marginHorizontal: 5,
    borderRadius: 6,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.6,
    shadowRadius: 2,
    elevation: 5,
    marginBottom: 20,
    paddingBottom: 10,
  },
  pk_name: {
    color: '#242e43',
    fontFamily: GLOBAL.heavy,
    fontWeight: 'bold',
    fontSize: 14,
    paddingHorizontal: 10,
  },
  pk_place: {
    color: '#8a8a8a',
    fontFamily: GLOBAL.roman,
    fontSize: 12,
    paddingHorizontal: 10,
  },
  pk_priceText: {
    color: 'white',
    fontFamily: GLOBAL.heavy,
    fontSize: 14,
    fontWeight: 'bold',
  },
  pk_priceView: {
    backgroundColor: '#09304B',
    borderTopLeftRadius: 20,
    top: 20,
    position: 'absolute',
    alignSelf: 'flex-end',
    paddingHorizontal: 15,
    paddingVertical: 2,
  },
  pk_image: {
    height: 150,
    borderRadius: 6,
  },
});

export default Package;

import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
const {width, height} = Dimensions.get('window');

import DateTimePicker from '@react-native-community/datetimepicker';
import {TextField} from 'react-native-material-textfield';
import Loader from './Loader';
import {SimpleHeader} from '../utils/Header';
import {GetCabApi, GoogleDirectionApi} from '../service/Api';
import {useSelector} from 'react-redux';
import moment from 'moment';

const CarRental = ({navigation, route}) => {
  const {settings} = useSelector(state => state);
  const [date, setDate] = useState(new Date());
  const [state, setState] = useState({
    dropDownLabel: '',
    from: {
      lat: 0,
      lng: 0,
      description: '',
      adminArea: '',
      locality: '',
      subAdminArea: '',
    },
    to: {
      lat: 0,
      lng: 0,
      description: '',
      adminArea: '',
      locality: '',
      subAdminArea: '',
    },
    condition: 'local',
    direction_type_id: '3',
    booking_way: '1',
    mode: '',
    pickup_date: '',
    pickup_time: '',
    return_date: '',
    return_time: '',
  });
  const [isLoading, setIsLoading] = useState(false);
  const [mapState, setMapState] = useState({
    description: '',
    lat: 0,
    lng: 0,
    adminArea: '',
    locality: '',
    subAdminArea: '',
  });
  const setLocation = (key, data) => {
    setMapState(data);
    setState({...state, [key]: data});
  };

  const validateLocation = location => {
    const {adminArea, description, lat, lng} = location;
    return [adminArea, description, lat, lng].includes('');
  };
  // search handler to find cabs
  const searchHandler = async () => {
    const {
      condition,
      direction_type_id,
      booking_way,
      from,
      to,
      pickup_date,
      pickup_time,
      return_date,
      return_time,
    } = state;
    const directionDetails = {
      origin: {
        latitude: 0,
        longitude: 0,
        description: '',
        adminArea: '',
        locality: '',
        subAdminArea: '',
      },
      destination: {
        latitude: 0,
        longitude: 0,
        description: '',
        adminArea: '',
        locality: '',
        subAdminArea: '',
      },
      adminArea: '',
      locality: '',
      subAdminArea: '',
      distance: 0,
      duration: 0,
      condition,

      direction_type_id,
      booking_way,
      pickup_date,
      pickup_time,
      return_date,
      return_time,
    };
    const {booking_hour} = settings;
    if (validateLocation(to) || validateLocation(from)) {
      alert('Please Select Location');
      return;
    }
    if (pickup_date === '' || pickup_time === '') {
      alert('Please Select Date and Time');
      return;
    }
    const datetime =
      moment(`${pickup_date} ${pickup_time}`, 'YYYY-MM-DD hh:mm a').diff(
        moment(),
      ) / 60000;
    if (datetime < +booking_hour * 60) {
      alert(`You can only book before ${booking_hour} hours`);
      return;
    }
    directionDetails.origin = {
      latitude: from.lat,
      longitude: from.lng,
      description: from.description,
      adminArea: from.adminArea,
      locality: from.locality,
      subAdminArea: from.subAdminArea,
    };
    directionDetails.destination = {
      latitude: to.lat,
      longitude: to.lng,
      description: to.description,
      adminArea: to.adminArea,
      locality: to.locality,
      subAdminArea: to.subAdminArea,
    };
    directionDetails.adminArea = from.adminArea;
    directionDetails.locality = from.locality;
    directionDetails.subAdminArea = from.subAdminArea;
    console.log('validate', condition);
    setIsLoading(true);
    const {origin, destination} = directionDetails;
    const GDAResponse = await GoogleDirectionApi(
      `${origin.latitude},${origin.longitude}`,
      `${destination.latitude},${destination.longitude}`,
    );
    if (GDAResponse.status && GDAResponse.code === 200) {
      directionDetails.distance = GDAResponse.data.distance.value / 1000;
      directionDetails.duration = Math.round(
        GDAResponse.data.duration.value / 60,
      );
      const {
        condition,
        distance,
        locality,
        subAdminArea,
        adminArea,
        pickup_date,
        pickup_time,
        return_date,
        return_time,
      } = directionDetails;
      const headerTitle = 'Rent Cab';
      const body = {
        condition,
        condition_outstation: '',
        total_km: distance,
        city: locality,
        state: adminArea,
        pickup_date,
        pickup_time,
        return_date,
        return_time,
        airport_id: 0,
        lat: 0,
        lng: 0,
      };
      const {status = false, price_array = []} = await GetCabApi(body);
      setIsLoading(false);
      if (status && price_array.length) {
        navigation.navigate('SearchCab', {
          price_array,
          directionDetails,
          headerTitle,
        });
      } else {
        alert('Something went wrong');
      }
    } else {
      setIsLoading(false);
      alert(GDAResponse);
    }
  };
  const DateTimeView = (label, defaultValue, mode, type) => (
    <View style={{flex: 0.45}}>
      <TouchableOpacity
        onPress={() => setState({...state, showPicker: true, mode, type})}>
        <TextField
          label={label}
          defaultValue={defaultValue}
          editable={false}
          baseColor={defaultValue === '' ? '#8F8F8F' : '#242E42'}
        />
      </TouchableOpacity>
    </View>
  );
  const showDateTimePicker = () => (
    <DateTimePicker
      value={date}
      minimumDate={new Date()}
      mode={state.mode}
      display="spinner"
      is24Hour={false}
      onChange={({type, nativeEvent}) => {
        if (type === 'set') {
          if (state.mode === 'time') {
            setState({
              ...state,
              showPicker: false,
              pickup_time: moment(nativeEvent.timestamp).format('hh:mm a'),
            });
          } else {
            setState({
              ...state,
              showPicker: false,
              pickup_date: moment(nativeEvent.timestamp).format('YYYY-MM-DD'),
            });
          }
          setDate(nativeEvent.timestamp);
        } else {
          setState({...state, showPicker: false});
        }
      }}
    />
  );

  return (
    <View style={styles.container}>
      {isLoading && <Loader />}
      {SimpleHeader('Rent Cab', navigation.goBack)}
      <View style={styles.formView}>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('MapLocation', {
              type: 'airport',
              data: {
                key: 'from',
                callback: setLocation,
              },
            })
          }>
          <View>
            <TextField
              label={'From'}
              defaultValue={state.from.description}
              editable={false}
              baseColor={state.from.description ? '#242E42' : '#8F8F8F'}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('MapLocation', {
              type: 'airport',
              data: {
                key: 'to',
                callback: setLocation,
              },
            })
          }>
          <View>
            <TextField
              label={'To'}
              defaultValue={state.to.description}
              editable={false}
              baseColor={state.to.description ? '#242E42' : '#8F8F8F'}
            />
          </View>
        </TouchableOpacity>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          {DateTimeView('Pickup Date', state.pickup_date, 'date', 'pickup')}
          {DateTimeView('Pickup Time', state.pickup_time, 'time', 'pickup')}
        </View>
        {state.showPicker && showDateTimePicker()}
        <TouchableOpacity onPress={searchHandler} style={styles.searchTouch}>
          <Text style={styles.searchText}>Search</Text>
        </TouchableOpacity>
      </View>

      {/* <View style={{margin: 20}}>
        <Text>{`lng : ${mapState.lat}`}</Text>
        <Text>{`lat : ${mapState.lng}`}</Text>
        <Text>{`locality : ${mapState.locality}`}</Text>
        <Text>{`subAdminArea : ${mapState.subAdminArea}`}</Text>
        <Text>{`adminArea : ${mapState.adminArea}`}</Text>
        <Text>{`description : ${mapState.description}`}</Text>
      </View> */}
    </View>
  );
};

export default CarRental;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  formView: {
    marginHorizontal: 15,
    marginTop: '20%',
    paddingHorizontal: 20,
    paddingVertical: 30,
    borderRadius: 20,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  dropDownTextStyle: {
    fontFamily: 'Avenir-Medium',
    fontSize: 17,
    color: '#242E42',
  },
  mapViewStyle: {
    flexDirection: 'row',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  textField: {
    borderWidth: 1,
    borderColor: '#AAAAAA',
    margin: 5,
    padding: 5,
    width: '70%',
  },
  spacer: {
    height: 10,
  },

  title: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
  searchTouch: {
    backgroundColor: '#09304B',
    borderRadius: 20,
    alignSelf: 'center',
    width: '60%',
    paddingVertical: 10,
    marginTop: 20,
  },
  searchText: {
    textAlign: 'center',
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 18,
    color: '#FFFFFF',
  },
});

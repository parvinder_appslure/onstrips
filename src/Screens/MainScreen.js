import React, {useEffect, useState} from 'react';
import {ImageBackground} from 'react-native';
import {Text} from 'react-native';
import {View, Platform} from 'react-native';
import {
  Image,
  SafeAreaView,
  TouchableOpacity,
  FlatList,
  StyleSheet,
  Dimensions,
  ScrollView,
  PermissionsAndroid,
  StatusBar,
} from 'react-native';
import {StatusBarDark} from '../utils/CustomStatusBar';
import {
  CouponListApi,
  GeolocationInfo,
  GetAirportListApi,
  GetAirportsApi,
  GetHomePageApi,
} from '../service/Api';
import {useDispatch, useSelector} from 'react-redux';
import * as actions from '../redux/actions';
const {width, height} = Dimensions.get('window');
const MainScreen = ({navigation, route}) => {
  const {user, deviceInfo} = useSelector(state => state);
  const dispatch = useDispatch();
  const [state, setState] = useState({
    location: '',
    weather: '',
    weathers: {},
    data: [],
    stateArr: [],
    direction: [],
    isShown: false,
  });
  const dataList = [
    {
      image_url: 'http://139.59.76.223/cab/assets/upload/package/GoaBeach1.jpg',
      id: 1,
    },
    {
      image_url: 'http://139.59.76.223/cab/assets/upload/package/GoaBeach1.jpg',
      id: 2,
    },
  ];
  const headerView = () => (
    <View style={styles.h_container}>
      <TouchableOpacity onPress={navigation.openDrawer}>
        <Image
          source={require('../assets/menu.png')}
          style={styles.h_menuImage}
        />
      </TouchableOpacity>
    </View>
  );
  const topBannerView = () => (
    <FlatList
      horizontal
      pagingEnabled={true}
      showsHorizontalScrollIndicator={false}
      data={dataList}
      renderItem={({item}) => (
        <TouchableOpacity key={`$id_${item.id}`} style={styles.offer_touch}>
          <Image source={{uri: item.image_url}} style={styles.offer_Image} />
        </TouchableOpacity>
      )}
      keyExtractor={item => item.id.toString()}
    />
  );
  const offerView = () => (
    <>
      <Text style={styles.offer}>offer</Text>
      <FlatList
        horizontal
        pagingEnabled={true}
        showsHorizontalScrollIndicator={false}
        data={dataList}
        renderItem={({item}) => (
          <TouchableOpacity key={`$id_${item.id}`} style={styles.offer_touch}>
            <Image
              source={require('../assets/offer.png')}
              style={styles.offer_Image}
            />
          </TouchableOpacity>
        )}
        keyExtractor={item => item.id.toString()}
      />
    </>
  );
  const menuView = () => (
    <View style={styles.mn_container}>
      {optionView(
        'Book\nFlight',
        require('../assets/flight-book.png'),
        'Flight',
      )}
      {optionView(
        'Book\nVacation',
        require('../assets/vacation-book.png'),
        'PackageSearch',
      )}
      {optionView(
        'Airport\nTransport',
        require('../assets/flight-trans.png'),
        'Airport',
      )}
      {optionView(
        'Cab\nRent \nor \nOutstation',
        require('../assets/cab-rent.png'),
        'CabOption',
      )}
    </View>
  );
  const optionView = (title, source, nav) => (
    <TouchableOpacity
      style={styles.mn_touch}
      onPress={() => navigation.navigate(nav, state)}>
      <ImageBackground source={source} style={styles.mn_bg}>
        <Text style={styles.mn_title}>{title}</Text>
      </ImageBackground>
    </TouchableOpacity>
  );

  useEffect(() => {
    fetchData();
    fetchAirport();
    fetchAirportCab();
    fetchCouponList();
  }, []);
  const fetchData = async () => {
    PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: 'Location Permission',
        message: 'Onstrips needs access to your location ',
      },
    ).then(async granted => {
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        const info = await GeolocationInfo();
        dispatch(actions.SetCoords(info.coords));
        const responseJson = await GetHomePageApi({
          user_id: user.id,
          deviceID: deviceInfo.id,
          device_type: deviceInfo.os,
          device_token: deviceInfo.token,
        });
        // console.log(Object.keys(responseJson));
        consolejson(responseJson.booking_hour);
        consolejson(responseJson.settings);
        dispatch(actions.SetSettings(responseJson.settings));
        if (responseJson.status == true) {
          let states = responseJson.states;
          let stateArray = [];
          for (let i = 0; i < states.length; i++) {
            stateArray.push(states[i].state_name);
          }
          setState({
            ...state,
            location: stateArray[0],
            weather: responseJson.weather.goa,
            weathers: responseJson.weather,
            data: stateArray,
            stateArr: states,
            direction: responseJson.direction_type,
          });
        }
      } else {
        Alert.alert('Location Permission Not Granted');
      }
    });
  };
  const fetchAirport = async () => {
    const {status = false, data = []} = await GetAirportsApi();
    if (status) {
      dispatch(actions.SetAirportCode(data));
    }
  };
  const fetchAirportCab = async () => {
    const {status = false, list = []} = await GetAirportListApi();
    if (status) {
      dispatch(actions.SetAirportCab(list));
    }
  };
  const fetchCouponList = async () => {
    const {status = false, list = []} = await CouponListApi({user_id: user.id});
    status && dispatch(actions.SetCouponList(list));
  };

  return (
    <SafeAreaView>
      <StatusBarDark />
      <ScrollView>
        {headerView()}
        {topBannerView()}
        {menuView()}
        {offerView()}
      </ScrollView>
    </SafeAreaView>
  );
};

export default MainScreen;

const styles = StyleSheet.create({
  offer_touch: {
    width: width,
    alignItems: 'center',
    marginTop: 10,
  },
  offer_Image: {
    height: 180,
    width: '90%',
    resizeMode: 'cover',
    borderRadius: 5,
  },
  mn_container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginVertical: 5,
    marginHorizontal: 10,
  },
  mn_touch: {},
  mn_bg: {
    width: width / 2 - 30,
    marginHorizontal: 10,
    marginVertical: 15,
    height: 120,
    borderRadius: 8,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
  },
  mn_title: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#FFFFFF',
    textAlign: 'center',
  },
  offer: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#1D1E2C',
    marginHorizontal: 20,
  },

  h_container: {
    backgroundColor: '#FBD303',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingBottom: 15,
    paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight + 20 : 10,
    marginBottom: 10,
  },
  h_menuImage: {
    width: 24,
    height: 20,
    resizeMode: 'contain',
  },
  h_logo: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
    marginHorizontal: 10,
  },
  h_arrowImage: {
    width: 14,
    height: 14,
    resizeMode: 'contain',
    marginHorizontal: 5,
  },
  h_sunImage: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
  },
  h_view_1: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  h_view_2: {
    flexDirection: 'row',
    marginLeft: 'auto',
    alignItems: 'center',
  },
  h_text_1: {
    fontFamily: GLOBAL.heavy,
    fontSize: 16,
    color: '#09304B',
    fontWeight: '500',
  },
});

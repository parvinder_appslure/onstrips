import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {Dropdown} from 'react-native-material-dropdown';
const {width, height} = Dimensions.get('window');

import DateTimePicker from '@react-native-community/datetimepicker';
import {TextField} from 'react-native-material-textfield';
import Loader from './Loader';
import {SimpleHeader} from '../utils/Header';
import {GetCabApi, GoogleDirectionApi} from '../service/Api';
import {useSelector, useStore} from 'react-redux';
import moment from 'moment';

const dropDownList = [
  {
    id: '1',
    value: 'From Airport',
  },
  {
    id: '2',
    value: 'To Airport',
  },
];

const Airport = ({navigation, route}) => {
  const store = useStore();
  const {settings} = useSelector(state => state);

  const [date, setDate] = useState(new Date());
  const [state, setState] = useState({
    dropDownIndex: '',
    airport_id: 0,
    airport_name: '',
    airportCab: store.getState().airportCab,
    dropDownLabel: '',
    from: {
      lat: 0,
      lng: 0,
      description: '',
      subAdminArea: '',
      locality: '',
      adminArea: '',
    },
    to: {
      lat: 0,
      lng: 0,
      description: '',
      subAdminArea: '',
      locality: '',
      adminArea: '',
    },
    condition: '',
    direction_type_id: '',
    booking_way: '1',
    airport_id: 0,
    mode: '',
    pickup_date: '',
    pickup_time: '',
    return_date: '',
    return_time: '',
  });
  const [isLoading, setIsLoading] = useState(false);
  const [mapState, setMapState] = useState({
    description: '',
    lat: 0,
    lng: 0,
    adminArea: '',
    locality: '',
    subAdminArea: '',
  });
  const setLocation = (key, data) => {
    setMapState(data);
    setState({...state, [key]: data});
  };

  const validateLocation = location => {
    const {adminArea, description, lat, lng} = location;
    return [adminArea, description, lat, lng].includes('');
  };
  // search handler to find cabs
  const searchHandler = async () => {
    const {
      dropDownIndex,
      condition,
      direction_type_id,
      booking_way,
      airportCab,
      airport_id,
      from,
      to,
      pickup_date,
      pickup_time,
      return_date,
      return_time,
    } = state;
    const {booking_hour} = settings;
    const directionDetails = {
      origin: {
        latitude: 0,
        longitude: 0,
        description: '',
        adminArea: '',
        locality: '',
        subAdminArea: '',
      },
      destination: {
        latitude: 0,
        longitude: 0,
        description: '',
        adminArea: '',
        locality: '',
        subAdminArea: '',
      },
      adminArea: '',
      locality: '',
      subAdminArea: '',
      distance: 0,
      duration: 0,
      condition,
      direction_type_id,
      airport_id,
      booking_way,
      airportDetail: {},
      pickup_date,
      pickup_time,
      return_date,
      return_time,
    };
    if (dropDownIndex === '' || condition === '') {
      alert('Please Select Cab Direction');
      return;
    }
    if (pickup_date === '' || pickup_time === '') {
      alert('Please Select Date and Time');
      return;
    }
    const datetime =
      moment(`${pickup_date} ${pickup_time}`, 'YYYY-MM-DD hh:mm a').diff(
        moment(),
      ) / 60000;
    if (datetime < +booking_hour * 60) {
      alert(`You can only book before ${booking_hour} hours`);
      return;
    }
    const flag = condition === 'to_airport';
    const location = flag ? from : to;
    if (airport_id === '' || validateLocation(location)) {
      alert('Please Select Location');
      return;
    } else {
      directionDetails[flag ? 'origin' : 'destination'] = {
        latitude: location.lat,
        longitude: location.lng,
        description: location.description,
      };

      const airportDetail = airportCab.find(({id}) => id === airport_id);
      const {name, lat, lon, city_name} = airportDetail;
      directionDetails[!flag ? 'origin' : 'destination'] = {
        latitude: lat,
        longitude: lon,
        description: name,
      };
      directionDetails.adminArea = flag ? from.adminArea : to.adminArea;
      directionDetails.locality = flag ? from.locality : to.locality;
      directionDetails.subAdminArea = flag
        ? from.subAdminArea
        : to.subAdminArea;
      directionDetails.lat = flag ? from.lat : to.lat;
      directionDetails.lng = flag ? from.lng : to.lng;
      directionDetails.airportDetail = airportDetail;
    }

    setIsLoading(true);
    const {origin, destination} = directionDetails;
    const GDAResponse = await GoogleDirectionApi(
      `${origin.latitude},${origin.longitude}`,
      `${destination.latitude},${destination.longitude}`,
    );
    if (GDAResponse.status && GDAResponse.code === 200) {
      directionDetails.distance = GDAResponse.data.distance.value / 1000;
      directionDetails.duration = Math.round(
        GDAResponse.data.duration.value / 60,
      );
      const {
        condition,
        distance,
        locality,
        subAdminArea,
        adminArea,
        pickup_date,
        pickup_time,
        return_date,
        return_time,
        airport_id,
        lat,
        lng,
      } = directionDetails;
      const headerTitle = 'Airport Cab';
      const body = {
        condition,
        condition_outstation: '',
        total_km: distance,
        city: locality,
        state: adminArea,
        pickup_date,
        pickup_time,
        return_date,
        return_time,
        airport_id,
        lat,
        lng,
      };
      const {status = false, price_array = []} = await GetCabApi(body);
      setIsLoading(false);
      if (status && price_array.length) {
        navigation.navigate('SearchCab', {
          price_array,
          directionDetails,
          headerTitle,
        });
      } else {
        alert('Something went wrong');
      }
    } else {
      setIsLoading(false);
      alert(GDAResponse);
    }
  };
  const DateTimeView = (label, defaultValue, mode, type) => (
    <View style={{flex: 0.45}}>
      <TouchableOpacity
        onPress={() => setState({...state, showPicker: true, mode, type})}>
        <TextField
          label={label}
          defaultValue={defaultValue}
          editable={false}
          baseColor={defaultValue === '' ? '#8F8F8F' : '#242E42'}
        />
      </TouchableOpacity>
    </View>
  );
  const showDateTimePicker = () => (
    <DateTimePicker
      value={date}
      minimumDate={new Date()}
      mode={state.mode}
      display="spinner"
      is24Hour={false}
      onChange={({type, nativeEvent}) => {
        if (type === 'set') {
          if (state.mode === 'time') {
            setState({
              ...state,
              showPicker: false,
              pickup_time: moment(nativeEvent.timestamp).format('hh:mm a'),
            });
          } else {
            setState({
              ...state,
              showPicker: false,
              pickup_date: moment(nativeEvent.timestamp).format('YYYY-MM-DD'),
            });
          }
          setDate(nativeEvent.timestamp);
        } else {
          setState({...state, showPicker: false});
        }
      }}
    />
  );

  useEffect(() => {
    if (state.pickup_date !== '' && state.pickup_time !== '') {
      console.log(state.pickup_time);
      console.log(`${state.pickup_date} ${state.pickup_time}`);
      console.log(new Date());
      console.log(new Date(`${state.pickup_date} ${state.pickup_time}`));
      console.log(
        moment(
          `${state.pickup_date} ${state.pickup_time}`,
          'YYYY-MM-DD hh:mm a',
        ),
      );
    }
  }, [state.pickup_date, state.pickup_time]);
  useEffect(() => {
    consolejson(state.airportCab);
  }, []);
  const setAirportId = (airport_id, airport_name) => {
    console.log(airport_id);
    setState({...state, airport_id, airport_name});
  };
  return (
    <View style={styles.container}>
      {isLoading && <Loader />}
      {SimpleHeader('Airport Transport', navigation.goBack)}
      <View style={styles.formView}>
        <Dropdown
          rippleOpacity={0.54}
          dropdownPosition={2}
          baseColor={state.dropDownIndex ? '#242E42' : '#8F8F8F'}
          onChangeText={(value, index) => {
            let dropDownLabel = '';
            let condition = '';
            let direction_type_id = '';
            switch (index) {
              case 0:
                dropDownLabel = 'From';
                condition = 'from_airport';
                direction_type_id = '1';
                break;
              case 1:
                dropDownLabel = 'To';
                condition = 'to_airport';
                direction_type_id = '2';
                break;
              default:
                dropDownLabel = '';
                condition = '';
                direction_type_id = '';
            }
            setState({
              ...state,
              dropDownIndex: index.toString(),
              dropDownLabel,
              condition,
              direction_type_id,
            });
          }}
          itemTextStyle={styles.dropDownTextStyle}
          label="Select Direction"
          data={dropDownList}
        />
        {state.dropDownIndex !== '0' && (
          <TouchableOpacity
            disabled={state.dropDownIndex === ''}
            onPress={() =>
              navigation.navigate('MapLocation', {
                type: 'airport',
                data: {
                  key: 'from',
                  callback: setLocation,
                },
              })
            }>
            <View>
              <TextField
                label={'From'}
                defaultValue={state.from.description}
                editable={false}
                baseColor={state.from.description ? '#242E42' : '#8F8F8F'}
              />
            </View>
          </TouchableOpacity>
        )}
        {['0', '1'].includes(state.dropDownIndex) && (
          // <Dropdown
          //   rippleOpacity={0.54}
          //   dropdownPosition={-2}
          //   baseColor={state.airport_id ? '#242E42' : '#8F8F8F'}
          //   onChangeText={(value, index) => {
          //     setState({...state, airport_id: value});
          //   }}
          //   itemTextStyle={styles.dropDownTextStyle}
          //   label={state.dropDownLabel}
          //   data={state.airportCab}
          //   labelExtractor={item => item.name}
          //   valueExtractor={item => item.id}
          // />
          <TouchableOpacity
            disabled={state.dropDownIndex === ''}
            onPress={() =>
              navigation.navigate('AirportCabList', {
                dropDownIndex: state.dropDownIndex,
                airportCab: state.airportCab,
                setAirportId,
              })
            }>
            <View>
              <TextField
                label={state.dropDownLabel}
                defaultValue={state.airport_name}
                editable={false}
                baseColor={state.airport_id ? '#242E42' : '#8F8F8F'}
              />
            </View>
          </TouchableOpacity>
        )}

        {state.dropDownIndex !== '1' && (
          <TouchableOpacity
            disabled={state.dropDownIndex === ''}
            onPress={() =>
              navigation.navigate('MapLocation', {
                type: 'airport',
                data: {
                  key: 'to',
                  callback: setLocation,
                },
              })
            }>
            <View>
              <TextField
                label={'To'}
                defaultValue={state.to.description}
                editable={false}
                baseColor={state.to.description ? '#242E42' : '#8F8F8F'}
              />
            </View>
          </TouchableOpacity>
        )}
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          {DateTimeView('Pickup Date', state.pickup_date, 'date', 'pickup')}
          {DateTimeView('Pickup Time', state.pickup_time, 'time', 'pickup')}
        </View>
        {state.showPicker && showDateTimePicker()}
        <TouchableOpacity onPress={searchHandler} style={styles.searchTouch}>
          <Text style={styles.searchText}>Search</Text>
        </TouchableOpacity>
      </View>

      {/* <View style={{margin: 20}}>
        <Text>{`lng : ${mapState.lat}`}</Text>
        <Text>{`lat : ${mapState.lng}`}</Text>
        <Text>{`locality : ${mapState.locality}`}</Text>
        <Text>{`subAdminArea : ${mapState.subAdminArea}`}</Text>
        <Text>{`adminArea : ${mapState.adminArea}`}</Text>
        <Text>{`description : ${mapState.description}`}</Text>
      </View> */}
    </View>
  );
};

export default Airport;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  formView: {
    marginHorizontal: 15,
    marginTop: '20%',
    paddingHorizontal: 20,
    paddingVertical: 30,
    borderRadius: 20,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  dropDownTextStyle: {
    fontFamily: 'Avenir-Medium',
    fontSize: 17,
    color: '#242E42',
  },
  mapViewStyle: {
    flexDirection: 'row',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  textField: {
    borderWidth: 1,
    borderColor: '#AAAAAA',
    margin: 5,
    padding: 5,
    width: '70%',
  },
  spacer: {
    height: 10,
  },

  title: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
  searchTouch: {
    backgroundColor: '#09304B',
    borderRadius: 20,
    alignSelf: 'center',
    width: '60%',
    paddingVertical: 10,
    marginTop: 20,
  },
  searchText: {
    textAlign: 'center',
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 18,
    color: '#FFFFFF',
  },
});

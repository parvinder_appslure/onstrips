import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  Dimensions,
  StatusBar,
} from 'react-native';
const window = Dimensions.get('window');
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Button} from 'react-native-elements';
import {Dropdown} from 'react-native-material-dropdown';
import SegmentedControl from '@react-native-community/segmented-control';

const {width, height} = Dimensions.get('window');
import {TextField} from 'react-native-material-textfield';
import moment from 'moment';
const GLOBAL = require('./Global');
import {Calendar} from 'react-native-calendars';
import Loader from './Loader';
import {Api_getToken, Api_updateToken} from './Api';
import {SimpleHeader} from '../utils/Header';
const Flight = ({navigation, route}) => {
  const [state, setState] = useState({
    detail: '',
    package: [],
    blog: [],
    click: '0',
    pick: '0',
    token: '',
    adult: '1',
    child: '0',
    infant: '0',
    FlightCabinClass: '1',
    classflight: '1',

    direction: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'],
    from: GLOBAL.searchLocations,
    date: '',
    dropdate: '',
    markedDates: {},
    time: '',
    aid: '',
    airport: 'DEL',
    toLocation: 'BOM',
    index: 0,
    eventLists: ['1', '2'],
    selectedHours: 0,
    isShown: false,
    selectedMinutes: 0,
    selectedIndex: 0,
    isLoading: false,
    multicity: [
      {
        id: 1,
        date: '2021-03-28',
        to: 'DEL',
        from: 'BOM',
      },
      {
        id: 2,
        date: '2021-03-29',
        to: 'BOM',
        from: 'GOI',
      },
    ],
    multicityId: 1,
  });

  const getIndex = (index, value) => {
    setState({
      ...state,
      classflight: value,
      index: index,
      FlightCabinClass: String(index + 1),
    });
  };

  useEffect(() => {
    getToken();
  }, []);

  useEffect(() => {
    if (state.selectedIndex === 0) {
      console.log('updated index', state.selectedIndex);
      let markedDates = state.markedDates;
      delete markedDates[state.dropdate];
      setState({...state, dropdate: '', markedDates: markedDates});
      console.log('updated');
    }
  }, [state.selectedIndex]);

  const setToken = async () => {
    const {status, token, error} = await Api_updateToken();
    console.log('token', token);
    status ? setState({...state, token}) : console.log('token fail', error);
  };

  const getToken = async () => {
    const {status, token} = await Api_getToken();
    status ? setState({...state, token}) : setToken();
  };

  const handleClick = () => {
    const {selectedIndex, multicity} = state;
    const Segments = [];
    if (state.token === '') {
      alert('Something went wrong try again');
      setToken();
      return;
    }
    if (state.FlightCabinClass === '') {
      alert('Please Select Travel Class');
      return;
    }
    if (state.adult === '') {
      alert('Please Select Number of Travellers');
      return;
    }
    if (selectedIndex === 2) {
      for (let i = 0; i < multicity.length; i++) {
        const item = multicity[i];
        if (item.date === '' || item.from === '' || item.to === '') {
          alert('Please add Field and then continue');
          return;
        }
        Segments.push({
          Origin: item.from,
          Destination: item.to,
          FlightCabinClass: state.FlightCabinClass,
          PreferredDepartureTime: moment(item.date),
          PreferredArrivalTime: moment(item.date),
        });
      }
      const data = {
        TokenId: state.token,
        // journeyType: ,
        adultcount: state.adult,
        childcount: state.child,
        infantcount: state.infant,
        direct_flight: false,
        OneStopFlight: false,
        Segments,
      };
      console.log(JSON.stringify(data, null, 2));
      navigation.navigate('MulticityFlight', {...data});
    } else {
      var directflight = true;
      if (state.selectedIndex == 2) {
        directflight = false;
      }

      var journeyType = '1';
      if (state.selectedIndex == 0) {
        journeyType = '1';
      } else if (state.selectedIndex == 1) {
        journeyType = '2';
      } else {
        journeyType = '3';
      }

      if (state.airport === '') {
        alert('Please Select Departure Location');
        return;
      }
      if (state.toLocation === '') {
        alert('Please Select Arrival Location');
        return;
      }

      if (state.selectedIndex === 1 && state.dropdate === '') {
        alert('Please Select Return Date');
        return;
      }
      if (state.date === '') {
        alert('Please Select Pickup Date');
        return;
      }
      const data = {
        TokenId: state.token,
        journeyType: journeyType,
        adultcount: state.adult,
        childcount: state.child,
        infantcount: state.infant,
        flight_from: state.airport,
        flight_to: state.toLocation,
        FlightCabinClass: state.FlightCabinClass,
        departure_date: state.date,
        arrival_date: state.dropdate,
        direct_flight: false,
      };

      GLOBAL.flide = data;
      GLOBAL.flide.classflight = state.classflight;
      GLOBAL.flide.adultcount = +state.adult;
      GLOBAL.flide.childcount = +state.child;
      GLOBAL.flide.infantcount = +state.infant;
      GLOBAL.flide.totalCount =
        parseInt(state.adult) + parseInt(state.child) + parseInt(state.infant);
      console.log(GLOBAL.flide.totalCount);
      if (state.selectedIndex === 0) {
        console.log('oneway');
        console.log(JSON.stringify(data, null, 2));
        navigation.navigate('FlightDetail', {data: data});
      }
      if (state.selectedIndex === 1) {
        console.log('round');
        console.log(JSON.stringify(data, null, 2));
        navigation.navigate('FlightDetails', {
          data: data,
          goTitle: `${data.flight_from}-${data.flight_to} ${moment(
            data.departure_date,
          ).format('DD,MMM')}`,
          backTitle: `${data.flight_to}-${data.flight_from} ${moment(
            data.arrival_date,
          ).format('DD,MMM')}`,
        });
      }
    }
  };

  const adult = (value, index) => {
    setState({...state, adult: value});
  };
  const child = (value, index) => {
    setState({...state, child: value});
  };
  const infant = (value, index) => {
    setState({...state, infant: value});
  };
  const returnDate = () =>
    setState({...state, selectedIndex: 1, pick: '1', isShown: true});

  const pickdate = () => setState({...state, pick: '0', isShown: true});

  const updateAirportCode = object => {
    console.log(JSON.stringify(object, null, 2));
    const {selectedIndex, multicity, multicityId} = state;
    if (selectedIndex === 2) {
      console.log(multicityId);
      multicity.map(item => {
        if (item.id === multicityId) {
          if (object.airport) {
            item.from = object.airport;
          }
          if (object.toLocation) {
            item.to = object.toLocation;
          }
        }
      });
      setState({...state, multicity});
    } else {
      setState({...state, ...object});
    }
  };
  const _from = multicityId => {
    setState({...state, click: '0', multicityId});
    navigation.navigate('FromAirport', {
      key: 0,
      updateAirportCode: updateAirportCode,
    });
  };
  const _to = multicityId => {
    setState({...state, click: '1', multicityId});
    navigation.navigate('FromAirport', {
      key: 1,
      updateAirportCode: updateAirportCode,
    });
  };
  const addMultiCityHandler = () => {
    const {multicity} = state;
    multicity.push({
      id: multicity.length + 1,
      date: '',
      to: '',
      from: '',
    });
    setState({...state, multicity});
  };
  const removeMultiCityHandler = id => {
    let {multicity} = state;
    multicity = multicity.filter(item => item.id !== id);
    multicity.map((item, index) => (item.id = index + 1));
    console.log(JSON.stringify(multicity, null, 1));
    setState({...state, multicity});
  };
  const addMultiCityDate = multicityId => {
    let marks = {
      selected: true,
      selectedColor: '#09304B',
    };
    let markedDates = {};
    state.multicity.forEach(item => {
      if (item.id === multicityId && item.date !== '') {
        markedDates = {
          [item.date]: {...marks},
        };
      }
    });

    setState({...state, markedDates, multicityId, pick: '2', isShown: true});
  };
  var timeColor;
  if (state.time == '') {
    timeColor = 'grey';
  } else {
    timeColor = 'black';
  }

  var dateColor;
  if (state.date == '') {
    dateColor = 'grey';
  } else {
    dateColor = 'black';
  }

  var to;
  if (state.toLocation == '') {
    to = 'grey';
  } else {
    to = 'black';
  }

  var airports;
  if (state.airport == '') {
    airports = 'grey';
  } else {
    airports = 'black';
  }

  var sdd;
  if (state.detail == '') {
    sdd = 'grey';
  } else {
    sdd = 'black';
  }

  var dire;
  if (state.detail == '') {
    dire = 'grey';
  } else {
    dire = 'black';
  }

  let classe = [
    {
      value: 'All',
    },
    {
      value: 'Economy',
    },
    {
      value: 'PremiumEconomy',
    },
    {
      value: 'Business',
    },
    {
      value: 'Premium Business',
    },
    {
      value: 'First',
    },
  ];
  let data = [
    {
      value: '0',
    },
    {
      value: '1',
    },
    {
      value: '2',
    },
    {
      value: '3',
    },
    {
      value: '4',
    },
    {
      value: '5',
    },
    {
      value: '6',
    },
    {
      value: '7',
    },
    {
      value: '8',
    },
  ];
  let data_adult = [
    {
      value: '1',
    },
    {
      value: '2',
    },
    {
      value: '3',
    },
    {
      value: '4',
    },
    {
      value: '5',
    },
    {
      value: '6',
    },
    {
      value: '7',
    },
    {
      value: '8',
    },
  ];
  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" />
      {SimpleHeader('Flight', navigation.goBack)}
      {state.isLoading && <Loader />}
      <KeyboardAwareScrollView>
        <View
          style={{
            backgroundColor: 'white',
            borderRadius: 25,
            margin: 20,
            padding: 10,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowOpacity: 0.2,
            shadowRadius: 1.41,
            elevation: 2,
          }}>
          <View
            style={{
              alignSelf: 'center',
              width: '100%',
              padding: 10,
            }}>
            <SegmentedControl
              values={[
                'One Way',
                'Round Trip',
                // , 'Multicity'
              ]}
              selectedIndex={state.selectedIndex}
              onChange={event => {
                setState({
                  ...state,
                  selectedIndex: event.nativeEvent.selectedSegmentIndex,
                });
              }}
            />
          </View>
          <View
            style={{
              paddingHorizontal: 10,
            }}>
            {state.selectedIndex !== 2 && (
              <>
                <TouchableOpacity onPress={() => _from('')}>
                  <View>
                    <TextField
                      label={'From'}
                      defaultValue={state.airport}
                      tintColor="grey"
                      editable={false}
                      baseColor={airports}
                    />
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => _to('')}>
                  <View>
                    <TextField
                      label={'To'}
                      defaultValue={state.toLocation}
                      tintColor="grey"
                      editable={false}
                      baseColor={airports}
                    />
                  </View>
                </TouchableOpacity>

                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity
                    style={{flex: 0.5}}
                    onPress={() => pickdate()}>
                    <View>
                      <TextField
                        label={'Departure Date'}
                        defaultValue={state.date}
                        editable={false}
                        tintColor="grey"
                        baseColor={dateColor}
                      />
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    disabled={state.selectedIndex === 0}
                    style={{flex: 0.5}}
                    onPress={() => returnDate()}>
                    <View style={{marginLeft: 10}}>
                      <TextField
                        label={'Return Date'}
                        defaultValue={state.dropdate}
                        editable={false}
                        tintColor="grey"
                        baseColor={
                          state.selectedIndex === 0 ? '#0000004d' : dateColor
                        }
                      />
                    </View>
                  </TouchableOpacity>
                </View>
              </>
            )}
            {state.selectedIndex === 2 &&
              state.multicity.map(item => (
                <View style={styles.mlt_view} key={`mlt_${item.id}`}>
                  <TouchableOpacity
                    style={{width: '20%'}}
                    onPress={() => from(item.id)}>
                    <TextField
                      label={'From'}
                      defaultValue={item.from}
                      tintColor="grey"
                      editable={false}
                      baseColor={airports}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{width: '20%'}}
                    onPress={() => to(item.id)}>
                    <TextField
                      label={'To'}
                      defaultValue={item.to}
                      tintColor="grey"
                      editable={false}
                      baseColor={airports}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{width: '30%'}}
                    onPress={() => addMultiCityDate(item.id)}>
                    <View>
                      <TextField
                        label={'Departure'}
                        defaultValue={item.date}
                        editable={false}
                        tintColor="grey"
                        baseColor={dateColor}
                      />
                    </View>
                  </TouchableOpacity>
                  <View style={styles.mlt_view_1}>
                    {item.id > 2 && (
                      <TouchableOpacity
                        onPress={() => removeMultiCityHandler(item.id)}>
                        <Image
                          source={require('../assets/close.png')}
                          style={styles.mlt_image}
                        />
                      </TouchableOpacity>
                    )}
                  </View>
                </View>
              ))}

            {state.selectedIndex === 2 && state.multicity.length !== 5 && (
              <TouchableOpacity
                style={styles.mlt_addTouch}
                onPress={addMultiCityHandler}>
                <Image
                  source={require('../assets/add.png')}
                  style={styles.mlt_addImage}
                />
              </TouchableOpacity>
            )}
            <Dropdown
              rippleOpacity={0.54}
              dropdownPosition={-4.2}
              onChangeText={(value, index) => getIndex(index, value)}
              itemTextStyle={{
                fontFamily: GLOBAL.heavy,
                fontSize: 16,
                color: 'red',
              }}
              label="Travel Class"
              data={classe}
              style
            />
            <Text style={{color: 'grey', marginTop: 20}}>
              Number of Travellers
            </Text>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '100%',
              }}>
              <View style={{width: '30%'}}>
                <Dropdown
                  rippleOpacity={0.54}
                  dropdownPosition={-4.2}
                  onChangeText={(value, index) => adult(value, index)}
                  itemTextStyle={{
                    fontFamily: GLOBAL.heavy,
                    fontSize: 16,
                    color: 'red',
                  }}
                  label="Adult"
                  data={data_adult}
                />
              </View>

              <View style={{width: '30%'}}>
                <Dropdown
                  rippleOpacity={0.54}
                  dropdownPosition={-4.2}
                  onChangeText={(value, index) => child(value, index)}
                  itemTextStyle={{
                    fontFamily: GLOBAL.heavy,
                    fontSize: 16,
                    color: {dire},
                  }}
                  label="Child"
                  data={data}
                />
              </View>
              <View style={{width: '30%'}}>
                <Dropdown
                  rippleOpacity={0.54}
                  dropdownPosition={-4.2}
                  onChangeText={(value, index) => infant(value, index)}
                  itemTextStyle={{
                    fontFamily: GLOBAL.heavy,
                    fontSize: 16,
                    color: {dire},
                  }}
                  label="Infant"
                  data={data}
                />
              </View>
            </View>
          </View>

          <Button
            buttonStyle={{
              backgroundColor: '#09304B',
              width: 200,
              borderRadius: 20,
              alignSelf: 'center',
              marginVertical: 20,
            }}
            titleStyle={{fontFamily: GLOBAL.heavy, fontSize: 20}}
            onPress={handleClick}
            title="Search"
          />
        </View>
      </KeyboardAwareScrollView>

      {state.isShown == true && (
        <View
          style={{
            position: 'absolute',
            backgroundColor: 'rgba(0,0,0,0.7)',
            width: width,
            height: height,
          }}>
          <View
            style={{
              position: 'absolute',
              width: window.width,
              height: 620,
              bottom: 0,
              backgroundColor: 'white',
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
            }}>
            <TouchableOpacity
              onPress={() => setState({...state, isShown: false})}>
              <View style={{flexDirection: 'row', marginTop: 16}}>
                <Image
                  source={require('../assets/cross-black.png')}
                  style={{
                    width: 20,
                    height: 20,
                    marginLeft: 20,
                    marginTop: 13,
                    resizeMode: 'contain',
                  }}
                />

                <Text
                  style={{
                    fontFamily: GLOBAL.heavy,
                    fontWeight: 'bold',
                    fontSize: 18,
                    color: 'black',
                    margin: 20,
                    marginTop: 15,
                  }}>
                  Select date and time
                </Text>
              </View>
            </TouchableOpacity>

            <View
              style={{
                width: window.width,
                height: 1,
                backgroundColor: '#eaecef',
              }}></View>
            <Calendar
              minDate={
                state.pick === '1' && state.date !== ''
                  ? state.date
                  : moment().format('YYYY-MM-DD')
              }
              maxDate={state.pick === '0' && state.dropdate}
              markedDates={state.markedDates}
              onDayPress={({dateString}) => {
                console.log(dateString);
                const {
                  pick,
                  date,
                  dropdate,
                  selectedIndex,
                  multicity,
                  multicityId,
                } = state;
                console.log(selectedIndex);
                console.log(typeof selectedIndex);
                if (selectedIndex === 2) {
                  multicity.map(item => {
                    if (item.id === multicityId) {
                      console.log('done');
                      item.date = dateString;
                    }
                  });
                  // console.log(JSON.stringify(multicity, null, 2));
                  setState({
                    ...state,
                    markedDates: {},
                    isShown: false,
                    multicity,
                  });
                } else {
                  let marks = {
                    selected: true,
                    selectedColor: '#09304B',
                  };
                  let markedDates = {
                    [dateString]: {...marks},
                  };
                  if (pick === '0') {
                    if (dropdate !== '') markedDates[dropdate] = {...marks};
                  } else {
                    if (data !== '') markedDates[date] = {...marks};
                  }
                  setState({
                    ...state,
                    markedDates: {...markedDates},
                    isShown: false,
                    [pick === '0' ? 'date' : 'dropdate']: dateString,
                  });
                }
              }}
            />
          </View>
        </View>
      )}
    </View>
  );
};

export default Flight;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  button: {
    borderWidth: 1,
    borderColor: '#000000',
    margin: 5,
    padding: 5,
    width: '70%',
    backgroundColor: '#DDDDDD',
    borderRadius: 5,
  },
  textField: {
    borderWidth: 1,
    borderColor: '#AAAAAA',
    margin: 5,
    padding: 5,
    width: '70%',
  },
  spacer: {
    height: 10,
  },

  title: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
  mlt_view: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  mlt_view_1: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  mlt_image: {
    height: 12,
    width: 12,
  },
  mlt_addImage: {
    height: 25,
    width: 25,
    resizeMode: 'contain',
  },
  mlt_addTouch: {
    alignSelf: 'center',
    padding: 5,
    marginTop: 5,
  },
});

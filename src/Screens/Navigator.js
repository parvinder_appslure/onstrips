import React from 'react';
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Splash from './Splash.js';
// import Home from './Home.js';
// import Drawer from './Drawer.js';
import AddTravel from './AddTravel.js';
import Airport from './Airport.js';
// import MapViewController from './MapViewController.js';
// import Location from './Location.js';
import FlightBooking from './FlightBooking.js';
import FlightBookings from './FlightBookings.js';
import FlightDetail from './FlightDetail.js';
import FlightDetails from './FlightDetails.js';
// import MultiCityFlight from './MultiCityFlight.js';
import FromAirport from './FromAirport.js';
import Flight from './Flight.js';
import CarRental from './CarRental.js';
// import Package from './Package.js';
import Outstation from './Outstation.js';
import SearchCab from './SearchCab.js';
// import PackageDetail from './PackageDetail.js';
// import PaymentMode from './PaymentMode.js';
import CabDetail from './CabDetail.js';
import Register from './Register.js';
import Otp from './Otp.js';
import Login from './Login';
// import Password from './Password.js';
import Forgot from './Forgot.js';
// import Invite from './Invite.js';
import History from './History.js';
import MapLocation from './MapLocation.js';
import MapSearchLocation from './MapSearchLocation.js';
// import BlogDescription from './BlogDescription.js';
// import BookingOption from './BookingOption.js';
// import HistoryDetail from './HistoryDetail.js';
// import PaymentGateway from './PaymentGateway.js';
// import MyMap from './MyMap.js';
// import Issue from './Issue.js';
// import MyPackage from './MyPackage.js';
// import Offer from './Offer.js';
import EditProfile from './EditProfile.js';
// import Rating from './Rating.js';
// import Report from './Report.js';
// import Setting from './Setting.js';
// import About from './About.js';
// import City from './City.js';
import MainScreen from './MainScreen.js';
import CustomDrawerContent from './CustomDrawerContent.js';
import PackageDetail from './PackageDetail.js';
import Package from './Package.js';
import PackageBooking from './PackageBooking.js';
import Wallet from './Wallet.js';
import CabOption from './CabOption.js';
import PaymentSuccess from './PaymentSuccess.js';
import ResetPassword from './ResetPassword.js';
import AirportCabList from './AirportCabList.js';
import PackageSearch from './PackageSearch.js';
// import Notification from './Notification.js';
// import CompletedRide from './CompletedRide.js';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const DrawerNavigator = () => (
  <Drawer.Navigator
    initialRouteName="MainScreen"
    drawerContent={props => <CustomDrawerContent {...props} />}>
    <Drawer.Screen name="MainScreen" component={MainScreen} />
  </Drawer.Navigator>
);
const StackNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={'Splash'}
        screenOptions={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}>
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Forgot"
          component={Forgot}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ResetPassword"
          component={ResetPassword}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Otp"
          component={Otp}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="DrawerNavigator"
          component={DrawerNavigator}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="PackageSearch"
          component={PackageSearch}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Package"
          component={Package}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="PackageDetail"
          component={PackageDetail}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="PackageBooking"
          component={PackageBooking}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Wallet"
          component={Wallet}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Flight"
          component={Flight}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Airport"
          component={Airport}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="FromAirport"
          component={FromAirport}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="AirportCabList"
          component={AirportCabList}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="FlightDetail"
          component={FlightDetail}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="FlightDetails"
          component={FlightDetails}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="FlightBooking"
          component={FlightBooking}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="FlightBookings"
          component={FlightBookings}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="CabOption"
          component={CabOption}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="EditProfile"
          component={EditProfile}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Outstation"
          component={Outstation}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="CarRental"
          component={CarRental}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="MapLocation"
          component={MapLocation}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="MapSearchLocation"
          component={MapSearchLocation}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="AddTravel"
          component={AddTravel}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="PaymentSuccess"
          component={PaymentSuccess}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SearchCab"
          component={SearchCab}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="CabDetail"
          component={CabDetail}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="History"
          component={History}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default StackNavigator;

import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
  TextInput,
  Dimensions,
  StatusBar,
  SafeAreaView,
  ScrollView,
} from 'react-native';
var validator = require('email-validator');
const {width, height} = Dimensions.get('window');
import Loader from './Loader';
import {StatusBarDark} from '../utils/CustomStatusBar';
import {SignUpOtpApi} from '../service/Api';
const Register = ({navigation, route}) => {
  const [state, setState] = useState({
    name: '',
    email: '',
    mobile: '',
    address: '',
    city: '',
    states: '',
    password: '',
    confirmPassword: '',
    referal: '',
  });
  const [loading, setLoading] = useState(false);

  // const referCodeHandler = () => {
  //   if (state.referal == '') {
  //     alert('Please enter Referaal Code.');
  //   } else {
  //     const url = 'http://139.59.76.223/cab/webservices/checkReferral';

  //     fetch(url, {
  //       method: 'POST',
  //       headers: {
  //         'x-api-key':
  //           '$2y$12$MOOt6dmiClUmITafZDyR2edjeJzx.UiXzG/ArWY8fl.zhNSi6FUfy',
  //         'Content-Type': 'application/json',
  //       },
  //       body: JSON.stringify({
  //         referral_code: state.referal,
  //       }),
  //     })
  //       .then(response => response.json())
  //       .then(responseJson => {
  //         if (responseJson.status == true) {
  //           // GLOBAL.referal_user_id = responseJson.referral_user_id;

  //           alert('Successfully Applied');
  //         } else {
  //           alert('Invalid Referal Code');
  //         }
  //       })
  //       .catch(error => {
  //         console.error(error);
  //       });
  //   }
  // };

  const submitHandler = async () => {
    const {
      name,
      email,
      mobile,
      address,
      states,
      city,
      password,
      confirmPassword,
    } = state;
    if (name == '') {
      alert('Please enter Name.');
    } else if (email == '') {
      alert('Please enter Email.');
    } else if (validator.validate(email) == false) {
      alert('Please enter valid Email.');
    } else if (mobile == '') {
      alert('Please enter Mobile.');
    } else if (address == '') {
      alert('Please enter Address.');
    } else if (city == '') {
      alert('Please enter City.');
    } else if (states == '') {
      alert('Please enter State.');
    } else if (password == '') {
      alert('Please enter Password.');
    } else if (confirmPassword == '') {
      alert('Please enter Confirm Password.');
    } else if (confirmPassword != password) {
      alert('Password not match');
    } else {
      const otp = Math.floor(1000 + Math.random() * 9000);
      setLoading(true);
      const {status = false, msg = 'Something went wrong'} = await SignUpOtpApi(
        {email, mobile, otp},
      );
      setLoading(false);
      if (status) {
        navigation.navigate('Otp', {
          type: 'register',
          data: {
            name,
            email,
            mobile,
            address,
            states,
            city,
            password,
            otp,
          },
        });
      } else {
        alert(msg);
      }
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBarDark />
      {loading && <Loader />}
      <ScrollView>
        <TouchableOpacity
          style={styles.touchBack}
          onPress={() => navigation.goBack()}>
          <Image
            source={require('../assets/backicon.png')}
            style={styles.imageBack}
          />
        </TouchableOpacity>

        <Text style={styles.textTitle_1}>Create your account</Text>
        <Text style={styles.textTitle_2}>Enter your details</Text>

        <TextInput
          style={styles.textInput}
          onChangeText={name => setState({...state, name})}
          placeholder="Name"
          keyboardType={'default'}
          value={state.name}
          placeholderTextColor={'#09304B'}
        />
        <TextInput
          style={styles.textInput}
          onChangeText={email => setState({...state, email})}
          placeholder="Email"
          keyboardType={'email-address'}
          value={state.email}
          placeholderTextColor={'#09304B'}
        />
        <TextInput
          style={styles.textInput}
          onChangeText={mobile => setState({...state, mobile})}
          placeholder="Mobile"
          keyboardType={'number-pad'}
          value={state.mobile}
          placeholderTextColor={'#09304B'}
        />
        <TextInput
          style={styles.textInput}
          onChangeText={address => setState({...state, address})}
          placeholder="Address (Optional)"
          keyboardType={'default'}
          value={state.address}
          placeholderTextColor={'#09304B'}
        />
        <View style={styles.viewCityState}>
          <TextInput
            style={[styles.textInput, styles.textInput_2]}
            onChangeText={city => setState({...state, city})}
            placeholder="City"
            keyboardType={'default'}
            value={state.city}
            placeholderTextColor={'#09304B'}
          />
          <TextInput
            style={[styles.textInput, styles.textInput_2]}
            onChangeText={states => setState({...state, states})}
            placeholder="State"
            keyboardType={'default'}
            value={state.states}
            placeholderTextColor={'#09304B'}
          />
        </View>
        <TextInput
          style={styles.textInput}
          onChangeText={password => setState({...state, password})}
          placeholder="Password"
          keyboardType={'default'}
          secureTextEntry={true}
          value={state.password}
          placeholderTextColor={'#09304B'}
        />
        <TextInput
          style={styles.textInput}
          onChangeText={confirmPassword =>
            setState({...state, confirmPassword})
          }
          placeholder="Confirm Password"
          keyboardType={'default'}
          secureTextEntry={true}
          value={state.confirmPassword}
          placeholderTextColor={'#09304B'}
        />
        {/* <View style={styles.viewReferral}>
          <TextInput
            style={[styles.textInput, styles.textInput_3]}
            onChangeText={referal => setState({...state, referal})}
            placeholder="Referral Code"
            keyboardType={'default'}
            secureTextEntry={true}
            value={state.referal}
            placeholderTextColor={'#09304B'}
          />
          <TouchableOpacity
            style={styles.touchValid}
            onPress={referCodeHandler}>
            <Text style={styles.textValid}>Validate</Text>
          </TouchableOpacity>
        </View> */}

        <TouchableOpacity onPress={submitHandler} style={styles.touchNext}>
          <Text style={styles.touchText}>Register</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => navigation.navigate('Login')}
          style={styles.touchRegister}>
          <Text style={styles.textReg_1}>Already have an account</Text>
          <Text style={styles.textReg_2}>Login</Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FBD303',
  },

  touchBack: {
    marginTop: 40,
    marginLeft: 20,
    padding: 10,
    alignSelf: 'flex-start',
  },
  imageBack: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
  textTitle_1: {
    color: '#09304B',
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 34,
    marginHorizontal: 34,
    marginTop: 20,
  },
  textTitle_2: {
    color: '#242E42',
    fontFamily: 'Avenir-Roman',
    fontWeight: '400',
    fontSize: 14,
    marginVertical: 10,
    marginHorizontal: 34,
  },
  textInput: {
    paddingHorizontal: 20,
    marginHorizontal: 34,
    marginVertical: 10,
    color: '#242E42',
    fontSize: 17,
    fontFamily: 'Aviner-Heavy',
    fontWeight: '900',
    backgroundColor: '#F1F2F6',
    borderRadius: 8,
    borderColor: '#EAECEF',
    borderWidth: 1,
  },
  textInput_2: {
    marginHorizontal: 0,
    flex: 0.47,
  },
  textInput_3: {
    marginHorizontal: 0,
    marginVertical: 0,
    flex: 0.65,
  },

  touchValid: {
    flex: 0.35,
    marginVertical: 8,
    marginHorizontal: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#09304B',
  },
  textValid: {
    fontSize: 14,
    color: '#09304B',
    fontFamily: 'Aviner-Roman',
  },
  viewCityState: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 34,
  },
  viewReferral: {
    flexDirection: 'row',
    marginHorizontal: 34,
    marginVertical: 10,
    backgroundColor: '#F1F2F6',
  },
  touchNext: {
    backgroundColor: '#09304B',
    width: '60%',
    alignSelf: 'center',
    borderRadius: 6,
    padding: 8,
    marginTop: 20,
  },
  touchText: {
    fontFamily: 'Aviner-Heavy',
    fontSize: 17,
    fontWeight: '900',
    color: '#fff',
    textAlign: 'center',
  },

  touchRegister: {
    marginVertical: 20,
    alignSelf: 'center',
    padding: 5,
  },
  textReg_1: {
    alignSelf: 'center',
    color: '#767676',
    fontFamily: 'Aviner-Roman',
    fontSize: 14,
  },
  textReg_2: {
    alignSelf: 'center',
    color: '#09304B',
    fontFamily: 'Aviner-Medium',
    fontSize: 15,
  },
});

export default Register;

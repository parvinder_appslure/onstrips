import AsyncStorage from '@react-native-community/async-storage';
import Global from './Global';
export const _post = (path, json) => {
  return new Promise((resolve, reject) => {
    fetch(Global.BASE_URL + path, {
      method: 'POST',
      headers: {
        'x-api-key':
          '$2y$12$MOOt6dmiClUmITafZDyR2edjeJzx.UiXzG/ArWY8fl.zhNSi6FUfy',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(json),
    })
      .then(res => res.json())
      .then(res => resolve(res))
      .catch(error => reject(error));
  });
};

export const Api_updateToken = () => {
  return new Promise((resolve, reject) => {
    fetch(Global.BASE_URL + 'get_flight_tokenId', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(res => res.json())
      .then(async res => {
        if (res.status == true) {
          Global.flighttoken = res.TokenId;
          const tokenObject = {
            tokenId: res.TokenId,
            expire: new Date().getTime(),
          };
          try {
            await AsyncStorage.setItem(
              'tokenObject',
              JSON.stringify(tokenObject),
            );
            resolve({status: true, token: res.TokenId});
          } catch (error) {
            console.error('token set error');
            resolve({status: false, error: error});
          }
        } else {
          resolve({status: false, error: ''});
        }
      })
      .catch(error => {
        console.error(error);
        resolve({status: false, error: error});
      });
  });
};

export const Api_removeToken = async () => {
  try {
    await AsyncStorage.removeItem('tokenObject');
    return true;
  } catch (e) {
    console.log('api 64: ', e);
    return false;
  }
};
export const Api_getToken = async () => {
  try {
    const value = await AsyncStorage.getItem('tokenObject');
    if (value !== null) {
      const {tokenId, expire} = JSON.parse(value);
      let time = new Date().getTime() - expire;
      if (time < 86400000) {
        Global.flighttoken = tokenId;
        return {status: true, token: tokenId};
      } else {
        return {status: false, message: 'token expire'};
      }
    } else {
      return {status: false, message: 'token not found'};
    }
  } catch (error) {
    console.error('token error', error);
    return {status: false};
  }
};

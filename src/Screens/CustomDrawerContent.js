import React from 'react';
import {
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Alert,
  View,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import * as actions from '../redux/actions';
const CustomDrawerContent = ({navigation, route}) => {
  const {user, isLogin} = useSelector(store => store);
  const dispatch = useDispatch();

  const OptionView = (title, source, onPress) => (
    <View style={styles.menuItem}>
      <Image style={styles.drawericon} source={source} />
      <Text style={styles.drawerTexts} onPress={onPress}>
        {title}
      </Text>
    </View>
  );
  const onAuthHandler = () => {
    if (isLogin) {
      Alert.alert(
        'Logout!',
        'Are you sure you want to Logout?',
        [
          {text: 'Cancel'},
          {
            text: 'Yes',
            onPress: () => {
              dispatch(actions.Logout());
              navigation.toggleDrawer();
            },
          },
        ],
        {cancelable: false},
      );
    } else {
      navigation.toggleDrawer();
      navigation.navigate('Login');
    }
  };
  return (
    <View style={styles.container}>
      <ScrollView>
        <TouchableOpacity
          style={styles.touchProfile}
          disabled={!isLogin}
          onPress={() => navigation.navigate('EditProfile')}>
          <Image
            style={styles.imageProfile}
            source={
              isLogin ? {uri: user.profile_pic} : require('../assets/user.png')
            }
          />
          <Text style={styles.textName}>{user?.name || 'Guest'}</Text>
          {isLogin && <Text style={styles.textName}>{user?.email}</Text>}
        </TouchableOpacity>

        {OptionView(
          `Home`,
          require('../assets/home.png'),
          navigation.toggleDrawer,
        )}

        {/* <View style={styles.menuItem}>
          <Image
            style={styles.drawericon}
            source={require('../assets/wallet.png')}
          />
          <Text
            style={styles.drawerTexts}
            onPress={() => navigation.navigate('Wallet')}>
            Wallet
          </Text>
        </View> */}
        {OptionView(`Offers`, require('../assets/offer2.png'), () =>
          navigation.navigate('Offer'),
        )}
        {OptionView(`My Booking`, require('../assets/history.png'), () =>
          navigation.navigate('History'),
        )}
        {OptionView(
          `Notifications`,
          require('../assets/notification.png'),
          () => navigation.navigate('Notification'),
        )}
        {OptionView(`Invite Friends`, require('../assets/invite.png'), () =>
          navigation.navigate('Invite'),
        )}
        {OptionView(`Settings`, require('../assets/settings.png'), () =>
          navigation.navigate('Setting'),
        )}
        {OptionView(
          `${isLogin ? 'Logout' : 'Login'}`,
          require('../assets/logout.png'),
          onAuthHandler,
        )}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#09304B',
  },
  drawerText: {
    marginTop: 2,
    color: 'white',
    marginLeft: 40,
    fontSize: 15,

    fontFamily: GLOBAL.medium,
  },
  touchProfile: {
    paddingTop: 50,
    paddingHorizontal: 30,
    paddingBottom: 20,
  },
  imageProfile: {
    width: 70,
    height: 70,
    borderRadius: 35,
  },
  textName: {
    marginTop: 10,
    color: 'white',
    fontFamily: 'Aviner-Medium',
    fontSize: 14,
    fontWeight: '500',
    height: 'auto',
  },
  menuItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
    padding: 5,
    paddingHorizontal: 30,
  },
  drawericon: {
    width: 20,
    height: 20,
    marginRight: 15,
    resizeMode: 'contain',
  },

  drawerTexts: {
    color: 'white',
    fontSize: 17,
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
  },

  loading: {
    position: 'absolute',
    left: window.width / 2 - 30,

    top: window.height / 2,

    opacity: 0.5,

    justifyContent: 'center',
    alignItems: 'center',
  },
  slide1: {
    marginLeft: 50,

    width: window.width - 50,
    height: 300,
    resizeMode: 'contain',
    marginTop: window.height / 2 - 200,
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  account: {
    marginTop: 20,
    textAlign: 'center',
    fontSize: 17,
    justifyContent: 'center',
    color: '#262628',
  },
  createaccount: {
    marginLeft: 5,
    fontSize: 17,
    textAlign: 'center',
    marginTop: 30,
    color: '#0592CC',
  },

  createaccounts: {
    marginLeft: 5,
    fontSize: 17,
    textAlign: 'center',
    marginTop: 30,
    color: '#0592CC',
    textDecorationLine: 'underline',
  },
  transcript: {
    textAlign: 'center',
    color: 'red',
  },
});

export default CustomDrawerContent;

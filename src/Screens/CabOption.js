import React from 'react';
import {ImageBackground} from 'react-native';
import {ScrollView} from 'react-native';
import {SafeAreaView} from 'react-native';
import {Text} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {StyleSheet, View, Dimensions} from 'react-native';
import {SimpleHeader} from '../utils/Header';

const {width, height} = Dimensions.get('window');
const CabOption = ({navigation, route}) => {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        {SimpleHeader('Cab Book', () => navigation.goBack())}
        {[
          // {
          //   title: 'Airport Transport',
          //   source: require('../assets/airport.png'),
          //   key: 'Airport',
          // },
          {
            title: 'Outstation Cab',
            source: require('../assets/outstation.png'),
            key: 'Outstation',
          },
          {
            title: 'Car Rent Cab',
            source: require('../assets/car.png'),
            key: 'CarRental',
          },
        ].map(({title, source, key}) => (
          <TouchableOpacity
            style={styles.otp_container}
            onPress={() => navigation.navigate(key, route.params)}>
            <ImageBackground style={styles.otp_bg} source={source}>
              <Text style={styles.otp_title}>{title}</Text>
            </ImageBackground>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </SafeAreaView>
  );
};

export default CabOption;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  otp_container: {
    marginVertical: 10,
    alignItems: 'center',
    marginHorizontal: 20,
  },
  otp_bg: {
    width: width - 30,
    height: 150,
    borderRadius: 8,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
  },
  otp_title: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 26,
    color: '#FFFFFF',
  },
});

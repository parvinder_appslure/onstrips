import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  TextInput,
  Dimensions,
  SafeAreaView,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {AsyncStorageSetUserId, GetProfileApi, SignInApi} from '../service/Api';
import Loader from './Loader';
import {CustomAlert} from '../utils/Modal';
import * as actions from '../redux/actions';
import {StatusBarDark} from '../utils/CustomStatusBar';
const {width, height} = Dimensions.get('window');

const Login = ({navigation, route}) => {
  const {deviceInfo} = useSelector(state => state);
  const dispatch = useDispatch();
  const [state, setState] = useState({
    mobile: '',
    password: '',
    loading: false,
  });
  const [modalState, setModalState] = useState({
    title: '',
    type: '',
    visible: false,
    callback: '',
  });
  const onCloseModel = () => {
    const {callback} = modalState;
    setModalState({
      ...modalState,
      title: '',
      type: '',
      visible: false,
      callback: '',
    });
    if (typeof callback === 'function') {
      callback();
    }
  };
  const toggleLoading = loading => setState({...state, loading});
  const handleClick = async () => {
    if (state.mobile == '') {
      alert('Please enter Mobile.');
    } else if (state.password == '') {
      alert('Please enter Password');
    } else {
      toggleLoading(true);
      const result = await SignInApi({
        mobile: state.mobile,
        device_id: deviceInfo.id,
        device_type: deviceInfo.os,
        device_token: deviceInfo.token,
        password: state.password,
      });
      const {user_id = '', message} = result;
      if (user_id !== '') {
        AsyncStorageSetUserId(user_id.toString());
        const {status = false, data = {}} = await GetProfileApi({user_id});
        toggleLoading(false);
        if (status) {
          dispatch(actions.Login(data));
          setModalState({
            ...modalState,
            title: 'Login Successfully',
            visible: true,
            type: 'success',
            callback: navigation.goBack,
          });
        }
      } else {
        toggleLoading(false);
        let title = 'Something went wrong please try later';
        if (message === 'Invalid Credentials.') {
          title = 'Inavlid Mobile Number and Password';
        }
        setModalState({
          ...modalState,
          title,
          visible: true,
          type: 'fail',
          callback: '',
        });
      }
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBarDark />
      {state.loading && <Loader />}
      <CustomAlert modalState={modalState} onCloseModel={onCloseModel} />
      <Text style={styles.textTitle_1}>{'Verify phone\nNumber'}</Text>
      <Text style={styles.textTitle_2}>
        We'll send a code to verify your phone
      </Text>

      <TextInput
        style={styles.textInput}
        onChangeText={mobile => setState({...state, mobile})}
        placeholder="Mobile"
        keyboardType={'number-pad'}
        value={state.mobile}
        placeholderTextColor={'#09304B'}
      />
      <TextInput
        style={styles.textInput}
        onChangeText={text => setState({...state, password: text})}
        placeholder="Password"
        secureTextEntry={true}
        value={state.password}
        placeholderTextColor={'#09304B'}
      />
      <TouchableOpacity
        onPress={() => navigation.navigate('Forgot')}
        style={styles.touchForgot}>
        <Text style={styles.textForgot}>Forgot Password</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={handleClick} style={styles.touchNext}>
        <Text style={styles.touchText}>Login</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('Register');
        }}
        style={styles.touchRegister}>
        <Text style={styles.textReg_1}>Didn't have an account</Text>

        <Text style={styles.textReg_2}>Register</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};
export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#FBD303',
    paddingHorizontal: 34,
  },
  textTitle_1: {
    color: '#09304B',
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 34,
  },
  textTitle_2: {
    color: '#242E42',
    fontFamily: 'Avenir-Roman',
    fontWeight: '400',
    fontSize: 14,
    marginVertical: 20,
  },
  textInput: {
    paddingHorizontal: 20,
    color: '#242E42',
    fontSize: 17,
    fontFamily: 'Aviner-Heavy',
    fontWeight: '900',
    marginVertical: 10,
    backgroundColor: '#F1F2F6',
    borderRadius: 8,
    borderColor: '#EAECEF',
    borderWidth: 1,
  },
  touchForgot: {
    marginVertical: 10,
    alignSelf: 'flex-end',
    padding: 5,
  },
  textForgot: {
    color: '#242E42',
    fontFamily: 'Aviner-Medium',
    fontWeight: '500',
    fontSize: 15,
  },
  touchNext: {
    backgroundColor: '#09304B',
    width: '60%',
    alignSelf: 'center',
    borderRadius: 6,
    padding: 8,
  },
  touchText: {
    fontFamily: 'Aviner-Heavy',
    fontSize: 17,
    fontWeight: '900',
    color: '#fff',
    textAlign: 'center',
  },
  touchRegister: {
    marginTop: 15,
    alignSelf: 'center',
    padding: 5,
  },
  textReg_1: {
    alignSelf: 'center',
    color: '#767676',
    fontFamily: 'Aviner-Roman',
    fontSize: 14,
  },
  textReg_2: {
    alignSelf: 'center',
    color: '#09304B',
    fontFamily: 'Aviner-Medium',
    fontSize: 15,
  },
});

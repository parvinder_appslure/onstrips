import React, {useEffect, useRef, useState} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
  Dimensions,
  SafeAreaView,
  ScrollView,
  TextInput,
  Platform,
} from 'react-native';
const window = Dimensions.get('window');

import Paytm from '@philly25/react-native-paytm';
import {checkSumApi} from './paytm';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';

import {TextField} from 'react-native-material-textfield';
import {StatusBarDark} from '../utils/CustomStatusBar';
import {SimpleHeader} from '../utils/Header';
import HTMLView from 'react-native-htmlview';
import {useSelector} from 'react-redux';
import {CabBookingApi, CouponApi, getCouponModule} from '../service/Api';

import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import Loader from './Loader';

const CabDetail = ({navigation, route}) => {
  const {user, couponList, isLogin} = useSelector(state => state);
  const [state, setState] = useState({
    discount: 0,
    finalPrice: 0,
    price: true,
    extra: true,
    important: true,
    user_id: user?.id || '',
    name: user.name,
    email: user.email,
    mobile: user.mobile,
    remark: '',
    local_service: 0,
    gender: user.gender || 'male',
    radio_props: [
      {label: 'Male', value: 'male'},
      {label: 'Female', value: 'female'},
    ],
    item: route.params.item,
    directionDetails: route.params.directionDetails,
    showPicker: false,
    mode: '',
    date: '',
    time: '',
    localIndex: 0,
  });
  const [couponState, setCouponState] = useState({
    id: 0,
    code: '',
    input: '',
    discount_type: '',
    discount_amount: '',
  });

  const bookingJsonRef = useRef();
  const [isLoading, setIsLoading] = useState(false);
  const bookingHandler = () => {
    if (!isLogin) {
      navigation.navigate('Login');
      return;
    }
    const {
      user_id,
      name,
      email,
      mobile,
      gender,
      item,
      directionDetails,
      remark,
      local_service = 0,
    } = state;
    const {
      distance,
      condition,
      direction_type_id,
      booking_way,
      airport_id = 0,
      origin,
      destination,
      pickup_date,
      pickup_time,
      return_date,
      return_time,
    } = directionDetails;
    const {
      id,
      price_per_km,
      total_price,
      price_per_km_discount,
      total_price_discount,
      tax_percentage = 0,
      nightcharge = 0,
    } = item;
    const flag = +total_price_discount === 0;

    const {discount_amount = 0, discount_type} = couponState;

    let discount = discount_amount;
    if (discount_type === 'percentage') {
      discount = Math.round((+total_price * +discount_amount) / 100);
    }

    const tax_amount = Math.round(
      ((+total_price + +nightcharge) * +tax_percentage) / 100,
    );

    const total_amount =
      (flag ? total_price : total_price_discount) -
      discount +
      tax_amount +
      nightcharge;

    if (name === '') {
      alert('Please provide traveller name');
      return;
    }
    if (email === '') {
      alert('Please provide email');
      return;
    }
    if (mobile === '') {
      alert('Please provide number');
      return;
    }

    bookingJsonRef.current = {
      user_id,
      coupan_id: couponState.id,
      coupan_code: couponState.code,
      payment_type: 'cab_booking',
      booking_way: '1',
      total_km: distance,
      transaction_mode: 'paytm',
      direction_type_id: '',
      cab_category_id: id,
      booking_for: condition,
      airport_id,
      local_service,
      direction_type_id,
      booking_way,
      total_amount,
      tax_amount,
      nightcharge,
      wallet_amount: 0,
      discount_amount: discount,
      payment_status: '1',
      traval_date: pickup_date,
      traval_time: pickup_time,
      travel_to_date: return_date,
      travel_to_time: return_time,
      picup_address: origin.description,
      drop_address: destination.description,
      picup_lat: origin.latitude,
      picup_lon: origin.longitude,
      drop_lat: destination.latitude,
      drop_lon: destination.longitude,
      traveller_name: name,
      traveller_email: email,
      traveller_mobile: mobile,
      traveller_gender: gender,
      booking_note: remark,
    };
    const config = {
      orderId: `OID_${user_id}_${new Date().getTime()}`,
      mobile: mobile,
      email: email,
      customerId: user_id,
      taxAmt: total_amount,
    };
    setIsLoading(true);
    checkSumApi(config)
      .then(details => Paytm.startPayment(details))
      .catch(error => {
        setIsLoading(false);
        console.log(error);
      });
  };

  const couponHandler = async () => {
    const {input} = couponState;
    const {user_id} = state;
    const {condition, condition_outstation} = state.directionDetails;
    if (input) {
      const module = getCouponModule(condition, condition_outstation);
      const body = {
        coupan_code: input,
        user_id,
        module,
      };
      setIsLoading(true);
      const apiResponse = await CouponApi(body);
      const {
        status = false,
        msg,
        discount_type,
        value,
        coupan_id,
      } = apiResponse;

      setIsLoading(false);
      if (status) {
        setCouponState({
          ...couponState,
          id: coupan_id,
          code: input,
          input: '',
          discount_type: discount_type,
          discount_amount: value,
        });
        alert('Coupon Applied');
      } else {
        setCouponState({
          ...couponState,
          id: 0,
          code: '',
          input: '',
          discount_type: '',
          discount_amount: 0,
        });
        alert('Invalid Coupon');
      }
    }
  };
  const travellerInfo = () => (
    <View style={styles.fl_container}>
      <Text style={styles.title_option}>Traveller Information</Text>
      <TextField
        label={'Name'}
        defaultValue={state.name}
        keyboardType={'default'}
        tintColor="grey"
        onChangeText={name => setState({...state, name})}
        baseColor={state.name === '' ? 'grey' : 'black'}
        inputContainerStyle={styles.inputContainerStyle}
      />
      <TextField
        label={'Email'}
        keyboardType={'email-address'}
        defaultValue={state.email}
        tintColor="grey"
        onChangeText={email => setState({...state, email})}
        baseColor={state.email === '' ? 'grey' : 'black'}
        inputContainerStyle={styles.inputContainerStyle}
      />
      <TextField
        label={'Moble'}
        prefix={'+91'}
        defaultValue={state.mobile}
        keyboardType={'phone-pad'}
        maxLength={10}
        tintColor="grey"
        onChangeText={mobile => setState({...state, mobile})}
        baseColor={state.mobile === '' ? 'grey' : 'black'}
        inputContainerStyle={styles.inputContainerStyle}
        affixTextStyle={{top: -2.2}}
      />

      <View style={styles.genderView}>
        <Text style={styles.genderText}>Gender</Text>
        <RadioForm formHorizontal={true} animation={true}>
          {state.radio_props.map((obj, i) => (
            <RadioButton labelHorizontal={true} key={i}>
              <RadioButtonInput
                obj={obj}
                index={i}
                isSelected={state.gender === obj.value}
                onPress={() => setState({...state, gender: obj.value})}
                borderWidth={1}
                buttonInnerColor={'#F8A301'}
                buttonOuterColor={
                  state.gender === obj.value ? '#F8A301' : '#242E42'
                }
                buttonSize={10}
                buttonOuterSize={20}
                buttonStyle={{}}
                buttonWrapStyle={{}}
              />
              <RadioButtonLabel
                obj={obj}
                index={i}
                labelHorizontal={true}
                onPress={() => setState({...state, gender: obj.value})}
                labelStyle={{
                  paddingRight: 20,
                }}
                labelWrapStyle={{
                  fontSize: 18,
                  color: '#242E42',
                  fontFamily: 'Avenir-Medium',
                  fontWeight: '500',
                }}
              />
            </RadioButton>
          ))}
        </RadioForm>
      </View>
      {/* <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginHorizontal: 30,
        }}>
        <View style={{flex: 0.45}}>
          <TouchableOpacity
            onPress={() =>
              setState({...state, showPicker: true, mode: 'date'})
            }>
            <TextField
              label={'Pickup Date'}
              defaultValue={state.date}
              editable={false}
              tintColor="grey"
              baseColor={state.date === '' ? 'grey' : 'black'}
            />
          </TouchableOpacity>
        </View>

        <View style={{flex: 0.45}}>
          <TouchableOpacity
            onPress={() =>
              setState({...state, showPicker: true, mode: 'time'})
            }>
            <TextField
              label={'Pickup Time'}
              defaultValue={state.time}
              editable={false}
              tintColor="grey"
              baseColor={state.time === '' ? 'grey' : 'black'}
            />
          </TouchableOpacity>
        </View>
      </View>
     */}
    </View>
  );

  const showCoupon = () => (
    <View style={styles.fl_container}>
      <Text style={styles.title_option}>Apply Coupon</Text>
      <View style={styles.cp_view}>
        {couponState.code === '' && (
          <>
            <TextInput
              style={styles.cp_input}
              value={couponState.input}
              onChangeText={value =>
                setCouponState({...couponState, input: value})
              }
            />
            <TouchableOpacity style={styles.cp_touch} onPress={couponHandler}>
              <Text style={styles.cp_text}>APPLY</Text>
            </TouchableOpacity>
          </>
        )}
        {couponState.code !== '' && (
          <>
            <Text
              style={[
                styles.cp_input,
                {color: '#16b922'},
              ]}>{`${couponState.code} Applied`}</Text>
            <TouchableOpacity
              style={styles.cp_touch}
              onPress={() =>
                setCouponState({
                  ...couponState,
                  id: 0,
                  code: '',
                  input: '',
                  discount_type: '',
                  discount_amount: '',
                })
              }>
              <Image
                source={require('../assets/close.png')}
                style={styles.cp_closeImage}
              />
            </TouchableOpacity>
          </>
        )}
      </View>

      {couponState.code === '' && (
        <>
          {couponList.map(item => (
            <TouchableOpacity
              style={styles.cpl_touch}
              onPress={() =>
                setCouponState({...couponState, input: item.code})
              }>
              <View style={styles.cpl_view}>
                <Text style={styles.cpl_text_1}>Code</Text>
                <Text style={styles.cpl_text_2}>{item.code}</Text>
              </View>

              <View style={styles.cpl_view}>
                <Text style={styles.cpl_text_3}>{item.heading}</Text>
              </View>
            </TouchableOpacity>
          ))}
        </>
      )}
    </View>
  );
  // const showDateTimePicker = () => (
  //   <DateTimePicker
  //     value={new Date()}
  //     minimumDate={new Date()}
  //     mode={state.mode}
  //     display="default"
  //     is24Hour={true}
  //     onChange={({type, nativeEvent}) => {
  //       if (type === 'set') {
  //         if (state.mode === 'time') {
  //           setState({
  //             ...state,
  //             showPicker: false,
  //             time: moment(nativeEvent.timestamp).format('hh:mm'),
  //           });
  //         } else {
  //           setState({
  //             ...state,
  //             showPicker: false,
  //             date: moment(nativeEvent.timestamp).format('YYYY-MM-DD'),
  //           });
  //         }
  //       } else {
  //         setState({...state, showPicker: false});
  //       }
  //     }}
  //   />
  // );

  const fareField = (description, fare) => (
    <View style={styles.fare_titleView}>
      <View style={styles.fare_titleDes}>
        <Text style={styles.fare_subTitleText}>{description}</Text>
      </View>
      <View style={styles.fare_titleSize}>
        <Text style={styles.fare_subTitleText2}>{`\u20B9 ${fare}`}</Text>
      </View>
    </View>
  );
  const estimateFare = () => {
    const {
      price_per_km,
      total_price,
      price_per_km_discount,
      total_price_discount,
      tax_percentage = 0,
      nightcharge = 0,
    } = state.item;
    const flag = +total_price_discount === 0;
    const {discount_amount = 0, discount_type} = couponState;

    let discount = discount_amount;
    if (discount_type === 'percentage') {
      discount = Math.round((+total_price * +discount_amount) / 100);
    }
    const tax_price = Math.round(
      ((+total_price + +nightcharge) * +tax_percentage) / 100,
    );
    const totalFare =
      (flag ? total_price : total_price_discount) -
      discount +
      tax_price +
      nightcharge;
    return (
      <View style={styles.fl_container}>
        <Text style={styles.title_option}>Fare Estimate</Text>
        <View style={styles.fare_view}>
          <View style={styles.fare_titleView}>
            <View style={styles.fare_titleDes}>
              <Text style={styles.fare_titleText}>Description</Text>
            </View>
            <View style={styles.fare_titleSize}>
              <Text style={styles.fare_titleText}>Charges</Text>
            </View>
          </View>
        </View>
        {fareField('Base Fare', Math.round(total_price))}
        {fareField('Night Charge', Math.round(nightcharge))}
        {!flag &&
          fareField(
            'Base Fare After Discount',
            Math.round(total_price_discount),
          )}
        {fareField('Coupon Discount', Math.round(discount))}
        {+tax_percentage !== 0 &&
          fareField(`Tax @${tax_percentage}`, tax_price)}
        <View style={styles.fare_titleView_2}>
          <Text style={styles.paymentText}>Total Fare</Text>
          <Text style={styles.paymentText}>{`\u20B9 ${Math.round(
            totalFare,
          )}`}</Text>
        </View>
      </View>
    );
  };
  //local transport option select
  const localTransportCallback = index => {
    const {
      price,
      extra_km,
      extra_hr_charge,
      nightcharge,
      tax_percentage,
      title,
    } = state.item.prices_array_local[index];
    setState({
      ...state,
      item: {
        ...state.item,
        total_price: +price,
        price_per_km: +extra_km,
        tax_percentage,
        nightcharge,
      },
      local_service: title,
    });
  };

  useEffect(() => {
    Paytm.addListener(Paytm.Events.PAYTM_RESPONSE, onPayTmResponse);
    return () => {
      Paytm.removeListener(Paytm.Events.PAYTM_RESPONSE, onPayTmResponse);
    };
  }, []);
  const onPayTmResponse = async resp => {
    const {STATUS, status, response} = resp;

    if (Platform.OS === 'ios') {
      if (status === 'Success') {
        const jsonResponse = JSON.parse(response);
        const {STATUS} = jsonResponse;

        if (STATUS && STATUS === 'TXN_SUCCESS') {
          // Payment succeed!
        }
      }
    } else {
      if (STATUS && STATUS === 'TXN_SUCCESS') {
        console.log('booking');
        bookingRequest(resp);
      } else {
        alert('Something went wrong');
        setIsLoading(false);
      }
    }
  };
  const bookingRequest = async paymentdetail => {
    const body = {
      ...bookingJsonRef.current,
      ...paymentdetail,
    };
    const response = await CabBookingApi(body);
    const {status = false} = response;
    setIsLoading(false);
    if (status) {
      navigation.navigate('PaymentSuccess');
    } else {
      alert('Something Went Wrong');
    }
  };
  useEffect(() => {
    if (isLogin) {
      const {id, name = '', email = '', mobile = '', gender = 'male'} = user;
      setState({...state, name, email, mobile, gender, user_id: id});
    }
  }, [isLogin]);
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <StatusBarDark />
      {isLoading && <Loader />}
      {SimpleHeader(`Cab Booking`, navigation.goBack)}
      <ScrollView>
        <CabView
          item={state.item}
          flag={route.params.directionDetails.condition === 'local'}
          callback={localTransportCallback}
        />
        {travellerInfo()}
        {showCoupon()}
        {/* {state.showPicker && showDateTimePicker()} */}
        {estimateFare()}
        <TextField
          label={'Remark'}
          defaultValue={state.remark}
          keyboardType={'default'}
          maxLength={60}
          tintColor="grey"
          onChangeText={remark => setState({...state, remark})}
          baseColor={state.remark === '' ? 'grey' : 'black'}
          inputContainerStyle={styles.inputContainerStyle}
          affixTextStyle={{top: -2.2}}
          multiline
          rows={4}
        />
        <TouchableOpacity style={styles.btnTouch} onPress={bookingHandler}>
          <Text style={styles.btnTitle}>Book Now</Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
};
export default CabDetail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tt_view: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#D8D8D8',
  },
  tt_view_2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  tt_touch: {
    padding: 5,
  },
  tt_icon: {
    height: 16,
    width: 16,
    resizeMode: 'contain',
  },

  tabTextTitle: {
    color: '#242E42',
    fontFamily: 'Avenir-Roman',
    fontSize: 14,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  button: {
    borderWidth: 1,
    borderColor: '#000000',
    margin: 5,
    padding: 5,
    width: '70%',
    backgroundColor: '#DDDDDD',
    borderRadius: 5,
  },
  textField: {
    borderWidth: 1,
    borderColor: '#AAAAAA',
    margin: 5,
    padding: 5,
    width: '70%',
  },
  spacer: {
    height: 10,
  },
  scene: {
    flex: 1,
  },

  title: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
  },

  fl_container: {
    margin: 15,
    paddingVertical: 10,
    backgroundColor: 'white',
    borderColor: '#f7f7f7',
    borderRadius: 6,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.6,
    shadowRadius: 2,
    elevation: 5,
  },
  fl_container_2: {
    borderBottomWidth: 1,
    borderBottomColor: '#D8D8D8',
    paddingVertical: 10,
  },
  fl_view_1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
  },
  fl_cab_image: {
    width: 100,
    height: 70,
    resizeMode: 'contain',
  },
  fl_view_2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    alignItems: 'center',
    borderLeftColor: '#FBD303',
    borderLeftWidth: 2,
  },
  fl_view_3: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 17,
    alignItems: 'center',
    // borderLeftColor: '#FBD303',
    // borderLeftWidth: 2,
    paddingVertical: 5,
  },
  fl_view_3s: {
    backgroundColor: '#FBD303',
  },
  fl_text_cat: {
    color: '#242E42',
    fontFamily: 'Avenir-Roman',
    fontSize: 18,
    fontWeight: '900',
  },
  fl_text_price: {
    color: '#242E42',
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
  },
  fl_text_price_2: {
    color: '#767676',
    fontFamily: 'Avenir-Roman',
    fontSize: 16,
    fontWeight: '500',
    textDecorationLine: 'line-through',
  },
  fl_text_price_3: {
    color: '#767676',
    fontFamily: 'Avenir-Roman',
    fontSize: 16,
    fontWeight: '500',
  },
  fl_text_feature: {
    color: '#767676',
    fontFamily: 'Avenir-Roman',
    fontSize: 12,
    fontWeight: '500',
  },
  opt_text_1: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#242E42',
  },
  opt_main_view: {
    flexDirection: 'row',
    marginHorizontal: 10,
    marginVertical: 5,
  },
  opt_left_view: {
    flex: 0.8,
  },
  opt_right_view: {
    flex: 0.2,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  title_option: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    color: '#242E42',
    fontWeight: '500',
    margin: 20,
    marginTop: 5,
    alignSelf: 'center',
  },
  inputContainerStyle: {
    marginHorizontal: 30,
  },

  genderView: {
    marginVertical: 10,
    marginHorizontal: 30,
  },
  genderText: {
    color: 'black',
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    marginBottom: 20,
  },

  cp_view: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#E5E5E5',
    borderRadius: 8,
    margin: 20,
    marginTop: 0,
    alignItems: 'center',
  },
  cp_input: {
    flex: 1,
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 16,
    color: 'black',
    paddingHorizontal: 20,
  },
  cp_touch: {
    padding: 10,
  },
  cp_closeImage: {
    height: 15,
    width: 15,
    resizeMode: 'contain',
    marginRight: 15,
  },
  cp_text: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 14,
    color: '#09304B',
  },
  cpl_touch: {
    marginHorizontal: 30,
    marginVertical: 5,
    paddingHorizontal: 5,
  },
  cpl_view: {flexDirection: 'row'},
  cpl_text_1: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    color: '#242E42',
  },
  cpl_text_2: {
    marginLeft: 20,
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 18,
    color: '#F8A301',
  },
  cpl_text_3: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 12,
    color: '#767676',
  },

  btnTouch: {
    marginTop: 15,
    marginBottom: 25,
    backgroundColor: '#09304B',
    width: '80%',
    paddingVertical: 8,
    borderRadius: 15,
    alignSelf: 'center',
    alignItems: 'center',
  },
  btnTitle: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#fff',
  },

  fare_view: {},
  fare_titleView: {
    flexDirection: 'row',
    padding: 5,
    paddingHorizontal: 25,
  },
  fare_titleView_2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 5,
    marginVertical: 15,
    paddingVertical: 12,
    paddingHorizontal: 25,
    backgroundColor: '#F7F7F7',
  },
  fare_titleDes: {
    flex: 0.5,
  },
  fare_titleSize: {
    flex: 0.5,
    alignItems: 'flex-end',
  },
  fare_titleText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 16,
    color: '#0A1F44',
  },
  fare_subTitleText: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 15,
    color: '#282727',
  },
  fare_subTitleText2: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 15,
    color: '#EF4236',
  },
});

const CabView = ({item, flag, callback}) => {
  const [state, setState] = useState({
    index: 0,
  });
  const totalFareCalculate = item => {
    const {total_price_discount, total_price} = item;
    return +total_price_discount === 0 ? total_price : total_price_discount;
  };
  const discountRateCalculate = item => {
    const {price_per_km_discount, price_per_km} = item;
    const flag = +price_per_km_discount === 0;
    const finalPrice = Math.round(flag ? price_per_km : price_per_km_discount);
    return (
      <Text style={styles.fl_text_price_3}>
        {`\u20B9`}
        {!flag && (
          <Text style={styles.fl_text_price_2}>{`  ${Math.round(
            price_per_km,
          )}`}</Text>
        )}
        {` ${finalPrice}/Km`}
      </Text>
    );
  };
  useEffect(() => {
    flag && callback(state.index);
  }, [state.index]);

  return (
    <View style={styles.fl_container}>
      <Text style={styles.title_option}>Cab Details</Text>
      <View style={styles.fl_container_2}>
        <View style={styles.fl_view_1}>
          <Image style={styles.fl_cab_image} source={{uri: item.image}} />
          {!flag && (
            <View style={{alignItems: 'flex-end'}}>
              {+item.total_price_discount !== 0 && (
                <Text style={styles.fl_text_price_2}>
                  {`\u20B9 ${Math.round(item.total_price)}`}
                </Text>
              )}
              <Text style={styles.fl_text_price}>
                {`\u20B9 ${Math.round(totalFareCalculate(item))}`}
              </Text>
            </View>
          )}
        </View>
        <View style={styles.fl_view_2}>
          <Text style={styles.fl_text_cat}>{item.cab_category}</Text>
          {!flag && discountRateCalculate(item)}
        </View>
        <View style={[styles.fl_view_2, {marginBottom: 10}]}>
          <Text style={styles.fl_text_feature}>{item.feature}</Text>
          <Text style={styles.fl_text_feature}>
            excl. of all Toll and Taxes
          </Text>
        </View>
        {flag &&
          item.prices_array_local.map((obj, index) => (
            <TouchableOpacity
              style={[
                styles.fl_view_3,
                index === state.index && styles.fl_view_3s,
              ]}
              onPress={() => setState({...state, index})}>
              <Text style={styles.fl_text_feature}>{obj.title}</Text>
              <Text
                style={styles.fl_text_feature}>{`\u20B9 ${obj.price}`}</Text>
            </TouchableOpacity>
          ))}
      </View>
      <OptionView
        title={'INCLUDED IN THE PRICE'}
        item={item.include_features}
        flag={false}
      />
      <OptionView
        title={'EXTRA CHARGES'}
        item={item.extra_features}
        flag={false}
      />
      <OptionView
        title={'Important Info'}
        item={item.importentinfo}
        flag={true}
      />
    </View>
  );
};

const OptionView = ({title, item, flag}) => {
  const [state, setState] = useState(false);
  return (
    <View style={styles.tt_view}>
      <View style={styles.tt_view_2}>
        <Text style={styles.tabTextTitle}>{title}</Text>
        <TouchableOpacity
          style={styles.tt_touch}
          onPress={() => setState(!state)}>
          <Image
            source={
              state
                ? require('../assets/down-1.png')
                : require('../assets/left-1.png')
            }
            style={styles.tt_icon}
          />
        </TouchableOpacity>
      </View>
      {state &&
        !flag &&
        item.map(obj => (
          <View style={styles.opt_main_view}>
            <View style={styles.opt_left_view}>
              <Text style={styles.opt_text_1}>{obj.title}</Text>
            </View>
            <View style={styles.opt_right_view}>
              <Text style={styles.opt_text_1}>{`\u20B9${obj.price}`}</Text>
            </View>
          </View>
        ))}
      {state && flag && (
        <View style={styles.opt_main_view}>
          <HTMLView value={item} stylesheet={htmlStyles} />
        </View>
      )}
    </View>
  );
};

const htmlStyles = StyleSheet.create({
  p: {
    fontWeight: '300',
    color: 'black', // make links coloured pink
    paddingHorizontal: 20,
  },
});

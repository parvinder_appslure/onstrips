import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
  Dimensions,
  ImageBackground,
  StatusBar,
  SafeAreaView,
} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import Airport from './Airport';
import {Marker} from 'react-native-maps';
const GLOBAL = require('./Global');
import MapView from 'react-native-maps';
const {width, height} = Dimensions.get('window');
const SCREEN_HEIGHT = height;
const SCREEN_WIDTH = width;
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

import {TabBar} from 'react-native-tab-view';
import {globalStyle} from './style';
export default class MapViewController extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      location: '',
      lat: 0,
      long: 0,
    };
  }
  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
      animations: {
        setRoot: {
          waitForRender: false,
        },
      },
    };
  };

  _renderScene = ({route}) => {
    switch (route.key) {
      case 'first':
        return <Airport />;
      case 'second':
        return <View />;
      case 'third':
        return <View />;
      default:
        return null;
    }
  };

  _handleStateChange = (state) => {
    this.setState({location: GLOBAL.searchLocation});

    GLOBAL.mylong = parseFloat(GLOBAL.searchLongitude);
    GLOBAL.mylat = parseFloat(GLOBAL.searchLatiude);

    if (GLOBAL.searchLocation != '') {
      this.setState({lat: parseFloat(GLOBAL.searchLatiude)});
      this.setState({long: parseFloat(GLOBAL.searchLongitude)});

      this.mapView.animateToRegion(
        {
          latitude: parseFloat(GLOBAL.searchLatiude),
          longitude: parseFloat(GLOBAL.searchLongitude),
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        },
        1000,
      );
    }
  };
  change = () => {
    // alert(GLOBAL.airportLocation)

    GLOBAL.toLocation = this.state.location;
    this.props.navigation.goBack();
  };
  componentDidMount() {
    this.setState({lat: parseFloat(GLOBAL.lat)});
    this.setState({long: parseFloat(GLOBAL.long)});

    this.props.navigation.addListener('willFocus', this._handleStateChange);
  }

  renderTabBar(props) {
    return (
      <TabBar
        style={{
          backgroundColor: '#FFFFFF',
          elevation: 0,
          borderColor: 'white',
          borderBottomWidth: 2.5,
          height: 50,
        }}
        labelStyle={{color: 'grey', fontSize: 10, fontFamily: GLOBAL.roman}}
        {...props}
        indicatorStyle={{backgroundColor: Colors.blue, height: 2.5}}
      />
    );
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
        }}>
        <StatusBar
          barStyle="dark-content"
          backgroundColor={'transparent'}
          translucent={true}
        />
        <SafeAreaView style={{flex: 0, backgroundColor: Colors.blue}} />

        <View style={[globalStyle.header_1]}>
          <TouchableOpacity
            onPress={() => this.props.navigation.goBack()}
            style={{
              padding: 8,
            }}>
            <Image
              source={require('./back.png')}
              style={globalStyle.backImage}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flex: 0.95,
            }}
            onPress={() => this.props.navigation.navigate('Location')}>
            <View
              style={{
                borderRadius: 4,
                backgroundColor: 'white',
                flexDirection: 'row',
                alignItems: 'center',
                padding: 5,
                paddingHorizontal: 10,
              }}>
              <Image
                source={require('./location.png')}
                style={{
                  width: 20,
                  height: 20,
                  resizeMode: 'contain',
                }}
              />
              <Text
                style={{
                  fontFamily: GLOBAL.heavy,
                  fontSize: 16,
                  color: 'black',
                  flex: 1,
                  marginHorizontal: 10,
                }}>
                {this.state.location}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <MapView
          style={{flex: 1}}
          showsUserLocation={true}
          showsMyLocationButton={true}
          showsTraffic={true}
          ref={(ref) => (this.mapView = ref)}
          region={{
            latitude: this.state.lat,
            longitude: this.state.long,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }}>
          <Marker
            coordinate={{
              latitude: this.state.lat,
              longitude: this.state.long,
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA,
            }}>
            <Image
              source={require('./location_pin.png')}
              style={{height: 60, width: 60, resizeMode: 'contain'}}
            />
          </Marker>
        </MapView>
        <TouchableOpacity
          onPress={this.change}
          style={{
            backgroundColor: '#09304B',
            position: 'absolute',
            alignSelf: 'center',
            bottom: 10,
            width: '60%',
            borderRadius: 6,
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: GLOBAL.heavy,
              fontWeight: '900',
              fontSize: 17,
              color: '#FFFFFF',
              paddingVertical: 8,
            }}>
            Apply
          </Text>
        </TouchableOpacity>

        {/* <Button
          buttonStyle={{
            backgroundColor: Colors.blue,
            // width: window.width - 20,
            borderRadius: 5,
            alignSelf: 'center',
            // position: 'absolute',
            // bottom: 40,
          }}
          titleStyle={{
            padding: 9,
            fontFamily: GLOBAL.roman,
            fontSize: 17,
            marginTop: 2,
          }}
          title="Apply"
          onPress={this.change}
        /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  button: {
    borderWidth: 1,
    borderColor: '#000000',
    margin: 5,
    padding: 5,
    width: '70%',
    backgroundColor: '#DDDDDD',
    borderRadius: 5,
  },
  textField: {
    borderWidth: 1,
    borderColor: '#AAAAAA',
    margin: 5,
    padding: 5,
    width: '70%',
  },
  spacer: {
    height: 10,
  },
  scene: {
    flex: 1,
  },

  title: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
  },
});

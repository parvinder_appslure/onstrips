import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
  TextInput,
  Dimensions,
  SafeAreaView,
} from 'react-native';
import {ForgotOtpApi, OtpApi} from '../service/Api';
import {StatusBarDark} from '../utils/CustomStatusBar';
import Loader from './Loader';
const window = Dimensions.get('window');
const {width, height} = Dimensions.get('window');
const Forgot = ({navigation, route}) => {
  const [state, setState] = useState({
    mobile: '',
    loading: false,
  });
  const handleClick = async () => {
    console.log('otp');
    const {mobile} = state;
    if (mobile.length === 10) {
      setState({...state, loading: true});
      const {status = false, otp = ''} = await ForgotOtpApi({mobile});
      setState({...state, loading: false});
      if (status && otp !== '') {
        navigation.navigate('Otp', {
          type: 'forgotPassword',
          data: {
            mobile,
            otp,
          },
        });
      } else {
        alert('Please enter registered number');
      }
    } else {
      alert('Please enter valid number');
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBarDark />
      {state.loading && <Loader />}
      <TouchableOpacity
        style={styles.touchBack}
        onPress={() => navigation.goBack()}>
        <Image
          source={require('../assets/backicon.png')}
          style={styles.imageBack}
        />
      </TouchableOpacity>

      <Text style={styles.textTitle_1}>Forgot your Password</Text>
      <Text style={styles.textTitle_2}>
        Enter your email/mobile no below to receive your password reset
        instruction{' '}
      </Text>

      <TextInput
        style={styles.textInput}
        onChangeText={mobile => setState({...state, mobile})}
        placeholder="Mobile"
        keyboardType={'default'}
        value={state.mobile}
        placeholderTextColor={'#09304B'}
      />

      <TouchableOpacity onPress={handleClick} style={styles.touchNext}>
        <Text style={styles.touchText}>Send</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FBD303',
  },
  touchBack: {
    marginVertical: 40,
    marginLeft: 20,
    padding: 10,
    alignSelf: 'flex-start',
  },
  imageBack: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
  textTitle_1: {
    color: '#09304B',
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 34,
    marginHorizontal: 34,
  },
  textTitle_2: {
    color: '#242E42',
    fontFamily: 'Avenir-Roman',
    fontWeight: '400',
    fontSize: 14,
    marginVertical: 20,
    marginHorizontal: 34,
  },
  textInput: {
    paddingHorizontal: 20,
    marginHorizontal: 34,
    marginBottom: 30,
    marginTop: '20%',
    color: '#242E42',
    fontSize: 17,
    fontFamily: 'Aviner-Heavy',
    fontWeight: '900',
    backgroundColor: '#F1F2F6',
    borderRadius: 8,
    borderColor: '#EAECEF',
    borderWidth: 1,
  },
  touchNext: {
    backgroundColor: '#09304B',
    width: '60%',
    alignSelf: 'center',
    borderRadius: 6,
    padding: 8,
    marginTop: 20,
  },
  touchText: {
    fontFamily: 'Aviner-Heavy',
    fontSize: 17,
    fontWeight: '900',
    color: '#fff',
    textAlign: 'center',
  },
});

export default Forgot;

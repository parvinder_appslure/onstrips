import moment from 'moment';
import React, {useEffect, useState} from 'react';
import {TouchableOpacity} from 'react-native';
import {
  FlatList,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {useSelector} from 'react-redux';
import {HistoryApi} from '../service/Api';
import {StatusBarDark} from '../utils/CustomStatusBar';
import {SimpleHeader} from '../utils/Header';
import Loader from './Loader';
import {CancelBookingApi} from '../service/Api';
const statusCode = ['', 'Pending', 'On Trip', 'Completed', 'Cancel'];
const History = ({navigation, route}) => {
  const {user} = useSelector(state => state);
  const [state, setState] = useState({
    lists: [],
    init: true,
    isLoading: true,
  });

  useEffect(() => {
    fetchHistory();
  }, []);

  const fetchHistory = async () => {
    const response = await HistoryApi({user_id: user.id});
    const {status = false, lists} = response;
    // console.log(Object.keys(response));
    setState({...state, lists, init: false, isLoading: false});
  };

  const cancelBookingHandler = async booking_id => {
    consolejson({booking_id});
    setState({...state, isLoading: true});
    const response = await CancelBookingApi({booking_id});
    const {status = false} = response;
    consolejson(response);
    if (status) {
      fetchHistory();
    } else {
      setState({...state, isLoading: false});
      alert('Something went wrong');
    }
  };

  const cancelView = booking_id => (
    <TouchableOpacity
      style={styles.hs_touch_cancel}
      onPress={() => cancelBookingHandler(booking_id)}>
      <Text style={styles.hs_text_cancel}>Cancel</Text>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={styles.container}>
      <StatusBarDark />
      {state.isLoading && <Loader />}
      {SimpleHeader('History', navigation.goBack)}
      <FlatList
        data={state.lists}
        style={{flex: 1}}
        contentContainerStyle={{
          paddingTop: 20,
        }}
        keyExtractor={item => item.booking_id.toString()}
        renderItem={({item, index}) => {
          if (index === 0) {
            // consolejson(Object.keys(item));
            // consolejson(item);
            // console.log();
          }
          const {
            booking_id,
            status,
            traval_date,
            traval_time,
            picup_address,
            drop_address,
            transaction_id,
            pay_amount,
            package_detail,
            cab_category_name,
            cancel_power,
          } = item;
          // consolejson({booking_id, cancel_power});
          const bookingType =
            Object.keys(package_detail).length === 0 ? 'cab' : 'package';
          return (
            <View key={`#b${booking_id}`} style={styles.hs_view}>
              <View style={styles.hs_view_1}>
                <Text style={styles.hs_text_date}>
                  {`${moment(traval_date).format('DD-MMM')}, ${traval_time}`}
                </Text>
                <Text style={styles.hs_text_status}>{statusCode[status]}</Text>
              </View>
              <Text
                style={
                  styles.hs_text_bookinId
                }>{`Booking Id #${booking_id}`}</Text>
              <Text
                style={
                  styles.hs_text_transactionId
                }>{`Transaction Id #${transaction_id}`}</Text>

              {bookingType === 'package' && (
                <View
                  style={{
                    padding: 15,
                  }}>
                  <Text style={styles.hs_text_address}>Package</Text>
                  <Text style={styles.hs_text_address}>
                    {package_detail.name}
                  </Text>
                </View>
              )}
              {bookingType === 'cab' && (
                <View style={styles.hs_view_2}>
                  <View style={styles.hs_view_21}>
                    <Image
                      source={require('../assets/origin2.png')}
                      style={styles.hs_image_point}
                    />
                    <Image
                      source={require('../assets/coordsLine.png')}
                      style={styles.hs_image_line}
                    />
                    <Image
                      source={require('../assets/destination2.png')}
                      style={styles.hs_image_point}
                    />
                  </View>
                  <View style={{flex: 1}}>
                    <Text style={styles.hs_text_address}>{picup_address}</Text>
                    <Text style={styles.hs_text_address}>{drop_address}</Text>
                  </View>
                </View>
              )}
              {cancel_power === 1 && cancelView(booking_id)}
              {bookingType === 'cab' && (
                <View style={styles.hs_view_3}>
                  <Image
                    source={require('../assets/sedan.png')}
                    style={styles.hs_image_cab}
                  />
                  <View style={styles.hs_view_title}>
                    <Text style={styles.hs_text_cabCat_1}>
                      {cab_category_name}
                    </Text>
                    {/* <Text
                      style={
                        styles.hs_text_totalPrice_2
                      }>{`\u20B9${pay_amount}`}</Text> */}
                  </View>
                  <View style={styles.hs_view_amount}>
                    <Text style={styles.hs_text_totalPrice_1}>Total Price</Text>
                    <Text
                      style={
                        styles.hs_text_totalPrice_2
                      }>{`\u20B9${pay_amount}`}</Text>
                  </View>
                </View>
              )}
            </View>
          );
        }}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  hs_view: {
    borderRadius: 6,
    marginHorizontal: 16,
    backgroundColor: 'white',
    marginBottom: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  hs_view_1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15,
    borderBottomColor: '#EFEFF4',
    borderBottomWidth: 1,
  },
  hs_text_date: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 15,
    color: '#BEC2CE',
  },
  hs_text_status: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 15,
    color: '#FF8900',
  },
  hs_text_bookinId: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 18,
    color: '#242E42',
    padding: 15,
    paddingBottom: 5,
  },
  hs_text_transactionId: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 15,
    color: '#BEC2CE',
    paddingHorizontal: 15,
  },
  hs_text_address: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#242E42',
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  hs_view_title: {
    flex: 1,
    marginHorizontal: 15,
  },
  hs_view_amount: {
    alignItems: 'flex-end',
    marginLeft: 'auto',
  },
  hs_text_totalPrice_1: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 15,
    color: '#BEC2CE',
  },
  hs_text_totalPrice_2: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 15,
    color: '#242E42',
  },
  hs_text_cabCat_1: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 15,
    color: '#242E42',
  },
  hs_view_2: {
    flexDirection: 'row',
    // justifyContent: 'space-between',
    padding: 15,
    borderBottomColor: '#EFEFF4',
    borderBottomWidth: 1,
  },
  hs_view_21: {
    alignItems: 'center',
    justifyContent: 'space-evenly',
    paddingRight: 15,
  },
  hs_view_3: {
    flexDirection: 'row',
    padding: 15,
    alignItems: 'center',
  },
  hs_image_cab: {
    width: 60,
    height: 20,
    resizeMode: 'contain',
  },
  hs_image_point: {
    width: 10,
    height: 10,
    resizeMode: 'contain',
  },
  hs_image_line: {
    height: 50,
    resizeMode: 'contain',
  },
  hs_text_cancel: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 15,
    color: 'white',
  },
  hs_touch_cancel: {
    backgroundColor: '#FF8900',
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 15,
    alignSelf: 'flex-end',
    marginRight: 15,
    marginTop: 15,
  },
});
export default History;

import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
  Dimensions,
  StatusBar,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import moment from 'moment';

const {width, height} = Dimensions.get('window');
import {decode} from 'html-entities';
import Paytm from '@philly25/react-native-paytm';
import Global from './Global';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import {checkSumApi} from './paytm';
import airlineicon from './airlineicon';
// import {_post} from './Api';
import Loader from './Loader';
import {Modal} from 'react-native';
import {useStore} from 'react-redux';
import {
  GetMemberApi,
  DeleteMemberApi,
  ToggleMemberApi,
  GetSsrDetailApi,
  GetFlightFareApi,
} from '../service/Api';

const FlightBooking = ({navigation, route}) => {
  const store = useStore();
  const {user} = store.getState();
  const [loadingState, setLoadingState] = useState(true);
  const [state, setState] = useState({
    index: 0,
    location: '',
    eventLists: [],
    lat: '',
    long: '',
    selectedTab: 0,
    offer: [],
    discount: 0,
    finalPrice: 0,
    price: true,
    extra: true,
    important: true,
    name: '',
    email: '',
    mobile: '',
    detail: [],
    value: 0,
    focus: true,
    // fare: {...route.params.Fare},
    params: route.params,
    nonLccFlight: {
      id: '',
    },
    Fare: {},
    FareBreakdown: [],
    Segments: [],
    TravellersCount: 0,
    initialize: true,
    mealModal: false,
    mealFare: 0,
  });
  const [memberState, setMemberState] = useState({
    adult: [],
    infant: [],
    child: [],
    adultcount: 0,
    childcount: 0,
    infantcount: 0,
  });
  const [mealState, setMealState] = useState({
    _list: [],
    count: 0,
  });
  const flightTitle = () => {
    const {Segments} = state;
    return (
      <View style={styles.ft_container}>
        <Image
          source={airlineicon[Segments[0][0].Airline.AirlineCode]}
          style={styles.ft_image}
        />
        <View style={styles.ft_view_1}>
          <Text style={styles.ft_airlineName}>
            {Segments[0][0].Airline.AirlineName}
          </Text>
          <Text style={styles.ft_airlineCode}>
            {`${Segments[0][0].Airline.AirlineCode} - ${Segments[0][0].Airline.FlightNumber}`}
          </Text>
        </View>
      </View>
    );
  };
  const flightDetail = () => {
    const segments = state.Segments[0];
    return (
      <>
        {segments.map(segment => {
          return (
            <View style={styles.fd_container}>
              <View style={styles.fd_view}>
                <Text style={styles.fd_airportCodeText}>
                  {segment.Origin.Airport.AirportCode}
                </Text>
                <Text style={styles.fd_depTime}>
                  {moment(segment.Origin.DepTime).format('h:mm:a')}
                </Text>
                <Text style={styles.fd_terminalText}>
                  {`Terminal ${
                    segment.Origin.Airport.Terminal
                      ? segment.Origin.Airport.Terminal
                      : '-'
                  }`}
                </Text>
                <Text style={styles.fd_airportNameText}>
                  {segment.Origin.Airport.AirportName}
                </Text>
              </View>
              <View style={styles.fd_centerView}>
                <Text style={styles.fd_duration}>
                  {convertMinsToTime(segment.Duration)}
                </Text>
                <Image
                  source={require('../assets/cad.png')}
                  style={styles.fd_cadImage}
                />
              </View>
              <View style={styles.fd_view_2}>
                <Text style={styles.fd_airportCodeText}>
                  {segment.Destination.Airport.AirportCode}
                </Text>
                <Text style={styles.fd_depTime}>
                  {moment(segment.Destination.ArrTime).format('h:mm:a')}
                </Text>
                <Text style={styles.fd_terminalText}>
                  {`Terminal ${
                    segment.Destination.Airport.Terminal
                      ? segment.Destination.Airport.Terminal
                      : '-'
                  }`}
                </Text>
                <Text style={styles.fd_airportNameText}>
                  {segment.Destination.Airport.AirportName}
                </Text>
              </View>
            </View>
          );
        })}
      </>
    );
  };

  const titleView = (subTitle, selectedCount) => {
    return (
      <View style={styles.ttl_view}>
        <Text style={styles.ttl_title}>{subTitle}</Text>
        <Text style={styles.ttl_title}>{selectedCount}</Text>
      </View>
    );
  };
  const addTravelCallback = () => {
    console.log('addTravelCallback');
    fetchMembersDetail(false);
  };
  const deleteMember = async (_list, key, id) => {
    console.log('delete');
    let mList = _list.filter(item => item.id !== id);
    setLoadingState(true);
    const {status = false} = await DeleteMemberApi({id});
    setLoadingState(false);
    if (status) toggleMember(mList, key, id);
    else alert('Something went wrong');
  };
  const editMember = (_list, key, data) => {
    // TODO:
    navigation.navigate('AddTravel', {
      key,
      type: 'edit',
      data,
      addTravelCallback,
    });
  };

  const toggleMember = (_list, key, id) => {
    let mList = _list;
    const allowedcount = state.params[key + 'count'];
    let count = 0;
    mList.map(item => {
      if (item.id === id) {
        item.is_selected = item.is_selected === '1' ? '' : '1';
      }
      if (item.is_selected === '1') {
        count++;
      }
    });
    // console.log(key + 'count', allowedcount);
    // console.log('count', count);
    if (allowedcount >= count) {
      // console.log(JSON.stringify(mList, null, 2));
      setMemberState({...memberState, [key]: mList, [key + 'count']: count});
    } else {
      alert(`You can select ${allowedcount} ${key} only`);
    }
  };

  const radioView = key => {
    // console.log('LIST ', key);
    return (
      <RadioForm formHorizontal={false} animation={true}>
        {memberState[key].map((itemData, i) => (
          <RadioButton labelHorizontal={true} key={i}>
            <RadioButtonInput
              obj={{
                label: `${itemData.firstname} - ${itemData.lastname}`,
              }}
              index={i}
              isSelected={itemData.is_selected === '1'}
              onPress={() =>
                toggleMember([...memberState[key]], key, itemData.id)
              }
              borderWidth={1}
              buttonInnerColor={'#09304B'}
              buttonOuterColor={'#09304B'}
              buttonSize={10}
              buttonOuterSize={20}
              buttonStyle={{}}
              buttonWrapStyle={radioStyle.buttonWrapStyle}
            />
            <RadioButtonLabel
              obj={{
                label: `${itemData.firstname} - ${typeof itemData.is_selected}`,
              }}
              index={i}
              labelHorizontal={true}
              onPress={() =>
                toggleMember([...memberState[key]], key, itemData.id)
              }
              labelStyle={radioStyle.labelStyle}
              labelWrapStyle={radioStyle.labelWrapStyle}
            />
            <TouchableOpacity
              style={radioStyle.touchEdit}
              onPress={() => editMember([...memberState[key]], key, itemData)}>
              <Image
                source={require('../assets/edit.png')}
                style={radioStyle.closeImage}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={radioStyle.touchClose}
              onPress={() =>
                deleteMember([...memberState[key]], key, itemData.id)
              }>
              <Image
                source={require('../assets/close.png')}
                style={radioStyle.closeImage}
              />
            </TouchableOpacity>
          </RadioButton>
        ))}
      </RadioForm>
    );
  };
  const buttonView = (title, key, type = 'add') => {
    // TODO:
    return (
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('AddTravel', {
            key,
            type,
            data: {},
            addTravelCallback,
          })
        }>
        <View style={styles.meal_addTouch}>
          <Text style={styles.meal_addText}>{title}</Text>
        </View>
      </TouchableOpacity>
    );
  };
  const adultViewRender = count => {
    // console.log('count');
    return (
      <>
        {titleView('Adult (12 yrs +)', `${memberState.adultcount}/${count}`)}
        {radioView('adult')}
        {buttonView('+ ADD ADULT TRAVELLER', 'adult')}
      </>
    );
  };
  const childViewRender = count => {
    return (
      <>
        {titleView('CHILD (2 - 12 Yrs)', `${memberState.childcount}/${count}`)}
        {radioView('child')}
        {buttonView('+ ADD CHILD TRAVELLER', 'child')}
      </>
    );
  };
  const inflantViewRender = count => {
    return (
      <>
        {titleView('INFANT (2 Yrs)', `${memberState.infantcount}/${count}`)}
        {radioView('infant')}
        {buttonView('+ ADD INFANT TRAVELLER', 'infant')}
      </>
    );
  };
  const mealAdd = meal_id => {
    let {count, _list} = mealState;
    let {TravellersCount} = state;
    if (TravellersCount === count) return;
    _list.map(item => {
      if (item.meal_id === meal_id) {
        item.meal_quantity++;
        count++;
      }
    });
    setMealState({...mealState, _list, count});
  };
  const mealSub = meal_id => {
    let {count, _list} = mealState;
    if (count === 0) return;
    _list.map(item => {
      if (item.meal_id === meal_id && item.meal_quantity > 0) {
        item.meal_quantity--;
        count--;
      }
    });
    setMealState({...mealState, _list, count});
  };
  const mealView = () => {
    const {_list} = mealState;
    return (
      <View style={styles.tv_container}>
        <Text style={styles.tv_title}>Meals</Text>
        <TouchableOpacity
          style={styles.meal_addTouch}
          onPress={() => setState({...state, mealModal: true})}>
          <Text style={styles.meal_addText}>+ Add Meals</Text>
        </TouchableOpacity>
        {_list.map(item => {
          const {meal_quantity, meal_id, AirlineDescription, Price} = item;
          if (meal_quantity === 0) return;
          return (
            <View key={`key_${meal_id}`} style={modalStyle.m2_view}>
              <Text style={modalStyle.m2_qty}>{meal_quantity}</Text>
              <Text style={modalStyle.m2_disc}>
                {decode(AirlineDescription)}
              </Text>
              <Text style={modalStyle.m2_price}>{`\u20B9 ${
                meal_quantity * Price
              }`}</Text>
            </View>
          );
        })}
      </View>
    );
  };
  const mealModalView = () => {
    const {TravellersCount} = state;
    const {_list, count} = mealState;
    return (
      <Modal animationType="slide" transparent={true} visible={state.mealModal}>
        <View style={modalStyle.container}>
          <View style={modalStyle.view}>
            <View style={modalStyle.headerView}>
              <Text
                style={
                  styles.tv_title
                }>{`Select meals  ${count}/${TravellersCount}`}</Text>
              <TouchableOpacity
                onPress={() => setState({...state, mealModal: false})}
                style={modalStyle.closeTouch}>
                <Image
                  source={require('../assets/close.png')}
                  style={modalStyle.closeImage}
                />
              </TouchableOpacity>
            </View>
            <ScrollView>
              {_list.map((item, index) => {
                // console.log('item', index);
                // console.log(item);
                const {meal_id, meal_quantity, AirlineDescription, Price} =
                  item;
                return (
                  <View key={`${meal_id}`} style={modalStyle.meal_view}>
                    <View style={modalStyle.meal_view_1}>
                      <Text style={modalStyle.description}>
                        {decode(AirlineDescription)}
                      </Text>
                      <Text style={modalStyle.price}>{`\u20B9 ${Price}`}</Text>
                    </View>
                    <View style={modalStyle.meal_view_2}>
                      {meal_quantity !== 0 && (
                        <View style={modalStyle.count_view}>
                          <TouchableOpacity
                            style={modalStyle.countTouch}
                            onPress={() => mealSub(meal_id)}>
                            <Text style={modalStyle.text_1}>-</Text>
                          </TouchableOpacity>
                          <View style={modalStyle.count_view_1}>
                            <Text style={modalStyle.text_2}>
                              {meal_quantity}
                            </Text>
                          </View>
                          <TouchableOpacity
                            style={modalStyle.countTouch}
                            onPress={() => mealAdd(meal_id)}
                            disabled={count === TravellersCount}>
                            <Text style={modalStyle.text_1}>+</Text>
                          </TouchableOpacity>
                        </View>
                      )}
                      {meal_quantity === 0 && (
                        <TouchableOpacity
                          style={modalStyle.addTouch}
                          onPress={() => mealAdd(meal_id)}
                          disabled={count === TravellersCount}>
                          <Text style={modalStyle.addText}>
                            {`ADD `}
                            <Text style={modalStyle.text_1}>+</Text>
                          </Text>
                        </TouchableOpacity>
                      )}
                    </View>
                  </View>
                );
              })}
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  };
  const fareView = () => {
    const {BaseFare, Tax, YQTax, OtherCharges, Discount, PublishedFare} =
      state.Fare;
    const {mealFare} = state;
    const title = (a, b) => {
      return (
        <View style={fareStyles.fieldContainer}>
          <Text style={fareStyles.text}>{a}</Text>
          <Text style={fareStyles.text}>{`\u20B9 ${b}`}</Text>
        </View>
      );
    };
    return (
      <View style={fareStyles.container}>
        {title('BaseFare', BaseFare)}
        {title('Tax', Tax)}
        {title('YQTax', YQTax)}
        {title('Other Charges', OtherCharges)}
        {title('Discount', Discount)}
        {mealFare !== 0 && title('Meal', mealFare)}
        <View style={fareStyles.fieldContainer2} />
        {title('Total Price', PublishedFare + mealFare)}
      </View>
    );
  };
  const bottomView = () => {
    const {mealFare} = state;
    const {PublishedFare} = state.Fare;
    const fare = mealFare + PublishedFare;
    return (
      <View style={bottomStyles.container}>
        <View>
          <View style={bottomStyles.view_1}>
            <Text style={bottomStyles.fareText}>{`\u20B9 ${fare}`}</Text>
            <Image
              source={require('../assets/info.png')}
              style={bottomStyles.image}
            />
          </View>
          <Text style={bottomStyles.fareText_2}>All inclusive price</Text>
        </View>
        <TouchableOpacity
          style={bottomStyles.payButton}
          onPress={payNowHandler}>
          <Text style={bottomStyles.payText}>Pay Now</Text>
        </TouchableOpacity>
      </View>
    );
  };

  const fetchMembersDetail = async (flag = true) => {
    // TODO: user_id removing global depandency
    console.log('fetching', flag);
    const body = {
      user_id: user.id,
      flag,
    };
    // console.log('body', body);
    const {
      status = false,
      adult = [],
      infant = [],
      child = [],
    } = await GetMemberApi(body);
    // console.log('memberstate', adult.length);
    // console.log(JSON.stringify(adult, null, 2));
    if (status) setMemberState({...memberState, adult, infant, child});
  };
  const payNowHandler = async () => {
    const {params, mealFare, Fare} = state;
    if (
      memberState.adultcount !== params.adultcount ||
      memberState.childcount !== params.childcount ||
      memberState.infantcount !== params.infantcount
    ) {
      alert('Please complete Traveller Details');
      return;
    }
    console.log('done');
    const {IsLCC} = state.params;
    // const taxAmt = parseInt(Fare.PublishedFare) - parseInt(state.discount);
    const taxAmt = mealFare + Fare.PublishedFare;
    console.log('taxAmt =>', taxAmt);

    // const userDetails = JSON.parse(await AsyncStorage.getItem('userDetails'));
    // if (Object.keys(userDetails).length === 0) {
    //   alert('Something went wrong please start app again');
    //   return;
    // }

    // if (!IsLCC) {
    //   showLoading();
    //   const response = await postApi(
    //     Global.BASE_URL + 'ticket_for_nonlcc_hold',
    //     {
    //       ResultIndex: route.params.ResultIndex,
    //       TokenId: Global.flighttoken,
    //       TraceId: Global.TraceId,
    //       user_id: Global.user_id,
    //     },
    //   );
    //   console.log(JSON.stringify(response.id));
    //   hideLoading();
    //   if (response.status) {
    //     setState({...state,
    //       nonLccFlight: {
    //         id: response.id,
    //       },
    //     });
    //   } else {
    //     alert('Something went wrong please try later');
    //     return;
    //   }
    // }
    const config = {
      orderId: `OID_${user.id}_${new Date().getTime()}`,
      mobile: user.mobile,
      email: user.email,
      customerId: user.id,
      taxAmt: taxAmt,
    };
    console.log(JSON.stringify(config, null, 2));
    checkSumApi(config)
      .then(details => Paytm.startPayment(details))
      .catch(error => console.log(error));
  };
  const mealFetch = async () => {
    const {TokenId, TraceId, ResultIndex} = state.params;
    const {status = false, MealDynamic = []} = await GetSsrDetailApi({
      TokenId,
      TraceId,
      ResultIndex,
    });
    console.log('meal');
    // console.log(JSON.stringify(MealDynamic, null, 2));
    console.log(MealDynamic.length);
    if (status && MealDynamic.length !== 0) {
      const _list = MealDynamic[0].filter(item => item.Quantity !== 0);
      if (_list.length !== 0) {
        _list.map((item, index) => {
          (item['meal_id'] = `meal_id_${index}`), (item['meal_quantity'] = 0);
        });
        // setState({...state, MealDynamic: _list, initialize: false});
        setMealState({...mealState, _list});
        // console.log('MealDynamic', _list.length);
        // console.log('done');
      }
    } else {
      setState({...state, initialize: false});
    }
  };
  const fetchFlightDetails = async () => {
    const {
      TokenId,
      TraceId,
      ResultIndex,
      adultcount = 0,
      infantcount = 0,
      childcount = 0,
    } = state.params;
    const TravellersCount = adultcount + infantcount + childcount;
    consolejson({
      TokenId,
      TraceId,
      ResultIndex,
    });
    const {status = false, Results = {}} = await GetFlightFareApi({
      TokenId,
      TraceId,
      ResultIndex,
    });
    if (status) {
      if (Object.keys(Results).length) {
        const {Fare, FareBreakdown, Segments} = Results;
        (Fare.OtherCharges += 200),
          (Fare.PublishedFare += 200),
          setState({
            ...state,
            Fare,
            FareBreakdown,
            Segments,
            TravellersCount,
            initialize: false,
          });
        setLoadingState(false);
      }
    }
  };
  useEffect(() => {
    fetchFlightDetails();
    fetchMembersDetail(true);
    mealFetch();
    Paytm.addListener(Paytm.Events.PAYTM_RESPONSE, onPayTmResponse);
    return () => {
      Paytm.removeListener(Paytm.Events.PAYTM_RESPONSE, onPayTmResponse);
    };
  }, []);

  useEffect(() => {
    const {_list} = mealState;
    let mealFare = 0;
    _list.forEach(item => {
      const {meal_quantity, Price} = item;
      if (meal_quantity !== 0) {
        mealFare += meal_quantity * Price;
      }
    });
    setState({...state, mealFare});
  }, [mealState._list]);
  useEffect(() => {
    console.log('memberstate change');
    // return;
    const {adult, child, infant} = memberState;
    const members_id = [];
    const selectItemById = item => {
      if (item.is_selected === '1') {
        members_id.push(item.id);
      }
    };
    adult.forEach(item => selectItemById(item));
    child.forEach(item => selectItemById(item));
    infant.forEach(item => selectItemById(item));

    const Api = async body => {
      console.log(body);
      const {status = false} = await ToggleMemberApi(body);
      console.log('toggle api status', status);
    };
    Api({
      members_id,
      user_id: user.id,
      is_selected: '1',
    });
  }, [memberState]);
  const onPayTmResponse = async resp => {
    const {STATUS, status, response} = resp;

    if (Platform.OS === 'ios') {
      if (status === 'Success') {
        const jsonResponse = JSON.parse(response);
        const {STATUS} = jsonResponse;

        if (STATUS && STATUS === 'TXN_SUCCESS') {
          // Payment succeed!
          const body = {
            ResultIndex: route.params.ResultIndex,
            TokenId: Global.flighttoken,
            TraceId: Global.TraceId,
            user_id: Global.user_id,
          };
          console.log(body);
        }
      }
    } else {
      if (STATUS && STATUS === 'TXN_SUCCESS') {
        // Payment succeed!
        const body = {
          ResultIndex: route.params.ResultIndex,
          TokenId: Global.flighttoken,
          TraceId: Global.TraceId,
          user_id: Global.user_id,
          id: state.nonLccFlight.id,
        };
        const url = Global.BASE_URL + 'ticket_for_lcc';
        console.log(JSON.stringify(body));
        showLoading();
        //  const response = await postApi(url, body)
      }
    }
  };

  const changeIndex = index => {
    var discount = state.offer[index].type;
    if (discount == 'Fixed') {
      setState({...state, discount: state.offer[index].amount});
    } else {
      var fp = state.finalPrice;
      var amount = parseInt(state.offer[index].amount);
      var s = (fp * amount) / 100;

      if (s > parseInt(state.offer[index].upto_amount)) {
        setState({...state, discount: state.offer[index].upto_amount});
      } else {
        setState({...state, discount: s});
      }
    }

    //   Global.language = item.code;
    let {offer} = state;
    for (let i = 0; i < offer.length; i++) {
      offer[i].is_selected = '';
    }

    let targetPost = offer[index];

    // Flip the 'liked' property of the targetPost
    targetPost.is_selected = 'Y';

    offer[index] = targetPost;

    (Global.couponCode = targetPost.promocode),
      (Global.couponid = targetPost.id),
      // Then update targetPost in 'posts'
      // You probably don't need the following line.
      // posts[index] = targetPost;

      // Then reset the 'state.posts' property

      setState({...state, offer: offer});
  };
  const _setCount = (_list, key) => {
    let count = 0;
    _list.forEach(mAdult => {
      if (mAdult.is_selected === 1) {
        count++;
      }
    });
    setState({...state, [key]: count});
  };

  const convertMinsToTime = mins => {
    let hours = Math.floor(mins / 60);
    let minutes = mins % 60;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    return `${hours}hrs:${minutes}mins`;
  };

  const subTitleText = `${moment(Global.flide.departure_date).format(
    'DD-MMM',
  )} ${Global.flide.totalCount} Travellers | ${Global.flide.classflight}`;
  const {adultcount, childcount, infantcount} = state.params;
  const {Fare, Segments, initialize} = state;
  const {_list} = mealState;
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      {loadingState && <Loader />}
      {!initialize && (
        <>
          <View style={styles.header_container}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Image
                source={require('../assets/back2.png')}
                style={styles.header_backImage}
              />
            </TouchableOpacity>
            <View style={styles.header_titleView}>
              {Segments.length !== 0 && (
                <Text style={styles.header_titleText_1}>
                  {Segments[0][0].Origin.Airport.AirportCode} -{' '}
                  {Segments[0][0].Destination.Airport.AirportCode}
                </Text>
              )}
              <Text style={styles.header_titleText_2}>{subTitleText}</Text>
            </View>
          </View>
          <ScrollView>
            {Segments.length !== 0 && (
              <>
                <View style={styles.main_view}>
                  <View style={{marginHorizontal: 10}}>
                    {flightTitle()}
                    {flightDetail()}
                  </View>
                  <Text style={styles.fare_type_title}>
                    Fare Type | Ecnomy Basic
                  </Text>
                  <View style={styles.fare_type_view}>
                    <Text style={styles.fare_type_text}>
                      .Baggage - {Segments[0][0].Baggage}
                    </Text>
                    <Text style={styles.fare_type_text}>
                      .Cabin Baggage - {Segments[0][0].CabinBaggage}
                    </Text>
                    <Text style={styles.fare_type_text}>
                      .Base Fare - {Fare.BaseFare}
                    </Text>
                    <Text style={styles.fare_type_text}>.Tax - {Fare.Tax}</Text>
                  </View>
                </View>
                <View style={styles.tv_container}>
                  <Text style={styles.tv_title}>Traveller Details</Text>
                  {+adultcount !== 0 && adultViewRender(adultcount)}
                  {+childcount !== 0 && childViewRender(childcount)}
                  {+infantcount !== 0 && inflantViewRender(infantcount)}
                </View>
                {_list.length !== 0 && mealView()}
                {fareView()}
                {bottomView()}
              </>
            )}
            {_list.length !== 0 && mealModalView()}
          </ScrollView>
        </>
      )}
    </SafeAreaView>
  );
};
export default FlightBooking;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header_container: {
    flexDirection: 'row',
    backgroundColor: '#FBD303',
    paddingTop: 25,
    paddingBottom: 10,
    paddingHorizontal: 20,
    alignItems: 'center',
  },
  header_backImage: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
  header_titleView: {
    paddingHorizontal: 10,
  },
  header_titleText_1: {
    fontFamily: Global.heavy,
    fontSize: 16,
    color: '#09304B',
  },
  header_titleText_2: {
    fontFamily: Global.roman,
    fontSize: 12,
    color: '#09304B',
  },
  main_view: {
    backgroundColor: 'white',
    color: 'white',
    flexDirection: 'column',
    borderColor: '#f7f7f7',
    borderRadius: 6,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.6,
    shadowRadius: 2,
    elevation: 5,
    marginHorizontal: 20,
    marginTop: 20,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  button: {
    borderWidth: 1,
    borderColor: '#000000',
    margin: 5,
    padding: 5,
    width: '70%',
    backgroundColor: '#DDDDDD',
    borderRadius: 5,
  },
  textField: {
    borderWidth: 1,
    borderColor: '#AAAAAA',
    margin: 5,
    padding: 5,
    width: '70%',
  },
  spacer: {
    height: 10,
  },
  scene: {
    flex: 1,
  },

  title: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
  },

  ft_container: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    borderBottomColor: '#E0E0E0',
    borderBottomWidth: 0.5,
  },
  ft_image: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
  ft_view_1: {
    marginHorizontal: 10,
  },
  ft_airlineName: {
    color: '#09304B',
    fontFamily: Global.roman,
    fontSize: 14,
    fontWeight: '900',
  },
  ft_airlineCode: {
    color: '#09304B80',
    fontFamily: Global.roman,
    fontSize: 12,
    fontWeight: '400',
  },
  fd_container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 5,
    borderBottomColor: '#E0E0E0',
    borderBottomWidth: 0.5,
    paddingBottom: 10,
  },
  fd_view: {
    paddingTop: 10,
    width: 80,
  },
  fd_airportCodeText: {
    color: '#09304B80',
    fontFamily: Global.roman,
    fontWeight: '500',
    fontSize: 17,
  },
  fd_depTime: {
    color: '#F8A301',
    fontFamily: Global.roman,
    fontWeight: '500',
    fontSize: 15,
  },
  fd_terminalText: {
    color: '#09304B80',
    fontFamily: Global.roman,
    fontWeight: '500',
    fontSize: 12,
  },
  fd_airportNameText: {
    color: '#09304B80',
    fontFamily: Global.roman,
    fontWeight: '500',
    fontSize: 12,
  },
  fd_centerView: {
    justifyContent: 'center',
  },
  fd_duration: {
    color: '#09304B80',
    fontFamily: Global.roman,
    fontSize: 14,
    textAlign: 'center',
  },
  fd_cadImage: {
    width: 140,
    height: 20,
    marginVertical: 2,
    resizeMode: 'contain',
  },
  fd_view_2: {
    paddingTop: 10,
    width: 80,
    alignItems: 'flex-end',
  },
  fare_type_title: {
    color: '#09304B',
    fontFamily: Global.heavy,
    fontSize: 14,
    fontWeight: '500',
    padding: 10,
  },
  fare_type_view: {
    paddingHorizontal: 10,
    paddingBottom: 10,
  },
  fare_type_text: {
    color: '#8396A4',
    fontFamily: Global.heavy,
    fontSize: 12,
  },
  tv_container: {
    backgroundColor: 'white',
    color: 'white',
    flexDirection: 'column',
    borderColor: '#f7f7f7',
    borderRadius: 6,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.6,
    shadowRadius: 2,
    elevation: 5,
    marginHorizontal: 20,
    marginTop: 20,
    padding: 10,
  },
  tv_title: {
    fontFamily: Global.roman,
    fontSize: 18,
    fontWeight: '500',
    color: '#09304B',
  },
  meal_addTouch: {
    marginVertical: 10,
    alignSelf: 'center',
  },
  meal_addText: {
    fontFamily: Global.heavy,
    fontSize: 14,
    color: '#F8A301',
    textAlign: 'center',
  },
  ttl_view: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
  },
  ttl_title: {
    fontFamily: Global.heavy,
    fontSize: 15,
    color: '#09304B',
  },
});
const radioStyle = StyleSheet.create({
  buttonWrapStyle: {
    margin: 10,
  },
  labelStyle: {
    fontSize: 14,
    color: '#09304B',
    fontFamily: Global.heavy,
    fontWeight: '500',
  },
  labelWrapStyle: {
    marginHorizontal: 0,
  },
  touchClose: {
    justifyContent: 'center',
    marginHorizontal: 10,
  },
  touchEdit: {
    justifyContent: 'center',
    marginLeft: 'auto',
    marginRight: 10,
  },
  closeImage: {
    height: 10,
    width: 10,
    resizeMode: 'contain',
  },
});

const fareStyles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    color: 'white',
    borderColor: '#f7f7f7',
    borderRadius: 6,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.6,
    shadowRadius: 2,
    elevation: 5,
    margin: 20,
    padding: 10,
  },
  fieldContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  fieldContainer2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderBottomColor: '#efefe4',
    borderBottomWidth: 1,
  },
  text: {
    fontFamily: Global.roman,
    fontSize: 15,
    color: '#09304B',
  },
});

const bottomStyles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#FBD303',
    padding: 20,
    marginTop: 10,
  },

  view_1: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  image: {
    height: 18,
    width: 18,
    resizeMode: 'contain',
    marginHorizontal: 10,
  },
  fareText: {
    fontFamily: Global.heavy,
    fontSize: 25,
    color: '#09304B',
  },
  fareText_2: {
    fontFamily: Global.roman,
    fontSize: 14,
    color: '#09304B',
  },
  payButton: {
    backgroundColor: '#09304B',
    borderRadius: 25,
    alignSelf: 'center',
  },
  payText: {
    fontFamily: Global.heavy,
    fontWeight: '900',
    fontSize: 19,
    color: '#FFFFFF',
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
});

const modalStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#00000099',
    justifyContent: 'center',
  },
  view: {
    backgroundColor: 'white',
    marginHorizontal: 30,
    borderRadius: 20,
    paddingVertical: 20,
    height: '50%',
  },
  headerView: {
    marginBottom: 10,
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  closeTouch: {
    padding: 10,
  },
  closeImage: {
    height: 12,
    width: 12,
    resizeMode: 'contain',
  },
  meal_view: {
    flexDirection: 'row',
    marginBottom: 5,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  meal_view_1: {
    flex: 0.7,
    paddingRight: 5,
  },
  meal_view_2: {
    flex: 0.3,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  m2_view: {
    flexDirection: 'row',
    margin: 10,
    alignItems: 'center',
  },
  m2_qty: {
    marginRight: 10,
  },
  m2_disc: {
    flex: 0.7,
  },
  m2_price: {
    marginLeft: 'auto',
  },
  count_view: {
    borderRadius: 15,
    borderColor: '#09304B',
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 5,
  },
  countTouch: {
    paddingHorizontal: 5,
  },

  text_1: {
    fontFamily: Global.roman,
    fontSize: 16,
    color: '#ed5a6b',
  },
  count_view_1: {
    paddingHorizontal: 5,
    backgroundColor: '#f7ebec',
  },
  text_2: {
    color: '#09304B',
    fontFamily: Global.roman,
    fontSize: 16,
  },
  addText: {
    fontFamily: Global.roman,
    fontSize: 12,
    color: '#09304B',
  },
  addTouch: {
    borderRadius: 15,
    borderColor: '#09304B',
    borderWidth: 1,
    paddingHorizontal: 15,
    alignItems: 'center',
  },
  quantitiy: {},
  meal_text_count: {},
  meal_view_count: {},
});

import React, {useEffect} from 'react';
import {LogBox} from 'react-native';
import store from './src/redux/store';
import * as actions from './src/redux/actions';
import {Provider} from 'react-redux';
import StackNavigator from './src/Screens/Navigator';
import NetInfo from '@react-native-community/netinfo';
import DeviceInfo from 'react-native-device-info';
import PushNotification from 'react-native-push-notification';

global.consolejson = json => console.log(JSON.stringify(json, null, 2));

const App = () => {
  LogBox.ignoreLogs([
    'Warning:',
    'VirtualizedList:',
    'Animated: `useNativeDriver` was not specified',
    'Non-serializable values were found in the navigation state',
  ]);
  LogBox.ignoreAllLogs(true);

  const pushNotificationMessage = ({channelId, title, message}) =>
    PushNotification.localNotification({
      channelId,
      title,
      message,
    });

  useEffect(() => {
    const netInfoSubscribe = NetInfo.addEventListener(state =>
      store.dispatch(actions.SetNetInfo(state)),
    );
    PushNotification.configure({
      onRegister: ({token}) => {
        console.log(token.toString());
        store.dispatch(
          actions.SetDeviceInfo({
            id: DeviceInfo.getDeviceId(),
            token: token.toString(),
            model: DeviceInfo.getModel(),
            os: Platform.OS,
          }),
        );
      },
      onNotification: notification => {
        // console.log('notification');
        console.log(JSON.stringify(notification, null, 2));
        pushNotificationMessage(notification);
      },
      onAction: function (notification) {
        console.log('ACTION:', notification.action);
        console.log('NOTIFICATION:', notification);

        // process the action
      },
      onRegistrationError: function (err) {
        console.error(err.message, err);
      },
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      senderID: '659247106062',
      popInitialNotification: true,
      requestPermissions: true,
    });

    PushNotification.localNotificationSchedule({
      //... You can use all the options from localNotifications
      message: 'My Notification Message', // (required)
      date: new Date(Date.now() + 60 * 1000), // in 60 secs
      allowWhileIdle: false, // (optional) set notification to work while on doze, default: false
    });
    return () => {
      netInfoSubscribe();
    };
  }, []);
  return (
    <Provider store={store}>
      <StackNavigator />
    </Provider>
  );
};

export default App;
